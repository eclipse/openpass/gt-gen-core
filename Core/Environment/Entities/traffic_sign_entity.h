/*******************************************************************************
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_ENTITIES_TRAFFICSIGNENTITY_H
#define GTGEN_CORE_ENVIRONMENT_ENTITIES_TRAFFICSIGNENTITY_H

#include "Core/Environment/Entities/static_object_entity.h"

#include <unordered_map>

namespace gtgen::core::environment::entities
{

class TrafficSignEntity : public StaticObject
{
  public:
    TrafficSignEntity(mantle_api::UniqueId id, const std::string& name);

    void AddSupplementarySign(const mantle_api::UniqueId& key, mantle_api::IEntity& supplementary_sign);
    mantle_api::IEntity* GetSupplementarySign(const mantle_api::UniqueId& key) const;
    std::unordered_map<mantle_api::UniqueId, std::reference_wrapper<mantle_api::IEntity>> GetSupplementarySigns() const;
    bool DeleteSupplementarySign(const mantle_api::UniqueId& key);

    /// @brief Overrides SetPosition to simplify supplementary sign positioning.
    /// If no teleport action is defined for a supplementary sign, it calculates its position
    /// based on the main sign’s position, orientation, and vertical shift.
    void SetPosition(const mantle_api::Vec3<units::length::meter_t>& position) override;

    /// @brief Overrides SetOrientation to simplify supplementary sign orientation.
    /// If no teleport action is defined for a supplementary sign, it inherits the main sign's orientation.
    void SetOrientation(const mantle_api::Orientation3<units::angle::radian_t>& orientation) override;

  private:
    void SetSupplementarySignPositions(const mantle_api::StaticObjectProperties* properties);
    void SetSupplementarySignOrientations(const mantle_api::Orientation3<units::angle::radian_t>& orientation);
    std::unordered_map<mantle_api::UniqueId, std::reference_wrapper<mantle_api::IEntity>> supplementary_signs;
};

}  // namespace gtgen::core::environment::entities

#endif  // GTGEN_CORE_ENVIRONMENT_ENTITIES_TRAFFICSIGNENTITY_H
