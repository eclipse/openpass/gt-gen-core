/*******************************************************************************
 * Copyright (c) 2021-2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Entities/vehicle_entity.h"

#include <MantleAPI/Common/vector.h>
#include <gtest/gtest.h>
#include <units.h>

namespace gtgen::core::environment::entities
{

using units::literals::operator""_m;

TEST(VehicleEntityTest, GivenVehicleEntity_WhenSettingProperties_ThenPropertiesAreReadable)
{
    mantle_api::Vec3<units::length::meter_t> expected_bb_front{90_m, 60_m, 90_m};
    mantle_api::Vec3<units::length::meter_t> expected_bb_rear{42_m, 21_m, 10.5_m};
    units::length::meter_t expected_front_wheel_diameter{10.0};
    units::length::meter_t expected_rear_wheel_diameter{12.0};
    mantle_api::VehicleClass expected_classification = mantle_api::VehicleClass::kLuxury_car;

    auto properties = std::make_unique<mantle_api::VehicleProperties>();
    properties->classification = expected_classification;

    properties->rear_axle.bb_center_to_axle_center = expected_bb_rear;
    properties->front_axle.bb_center_to_axle_center = expected_bb_front;

    properties->front_axle.wheel_diameter = expected_front_wheel_diameter;
    properties->rear_axle.wheel_diameter = expected_rear_wheel_diameter;

    properties->bounding_box.dimension.length = 4_m;
    properties->bounding_box.dimension.width = 2_m;
    properties->bounding_box.dimension.height = 2_m;
    properties->is_host = true;

    VehicleEntity vehicle(0, "name");
    vehicle.SetProperties(std::move(properties));

    mantle_api::Dimension3 expected_dimensions{4_m, 2_m, 2_m};

    EXPECT_EQ(expected_classification, vehicle.GetProperties()->classification);
    EXPECT_EQ(expected_bb_rear, vehicle.GetProperties()->rear_axle.bb_center_to_axle_center);
    EXPECT_EQ(expected_bb_front, vehicle.GetProperties()->front_axle.bb_center_to_axle_center);

    EXPECT_EQ(expected_front_wheel_diameter, vehicle.GetProperties()->front_axle.wheel_diameter);
    EXPECT_EQ(expected_rear_wheel_diameter, vehicle.GetProperties()->rear_axle.wheel_diameter);

    auto dimensions = vehicle.GetProperties()->bounding_box.dimension;
    EXPECT_EQ(dimensions.length, expected_dimensions.length);
    EXPECT_EQ(dimensions.width, expected_dimensions.width);
    EXPECT_EQ(dimensions.height, expected_dimensions.height);

    EXPECT_TRUE(vehicle.GetProperties()->is_host);
}

TEST(VehicleEntityTest, GivenVehicleEntity_WhenSettingIndicatorState_ThenIndicatorStateCanBeRead)
{
    auto expected_indicator{mantle_api::IndicatorState::kWarning};
    VehicleEntity vehicle(0, "name");

    vehicle.SetIndicatorState(expected_indicator);

    EXPECT_EQ(expected_indicator, vehicle.GetIndicatorState());
}

TEST(VehicleEntityTest, GivenVehicleEntity_WhenSettingWheelStates_ThenWheelStatesCanBeRead)
{
    WheelStates expected_wheel_states{0.5, 0.4, 0.3, 0.2};
    VehicleEntity vehicle(0, "name");

    vehicle.SetWheelStates(expected_wheel_states);

    const auto& actual_wheel_states = vehicle.GetWheelStates();
    EXPECT_EQ(expected_wheel_states.front_left_mue, actual_wheel_states.front_left_mue);
    EXPECT_EQ(expected_wheel_states.front_right_mue, actual_wheel_states.front_right_mue);
    EXPECT_EQ(expected_wheel_states.rear_left_mue, actual_wheel_states.rear_left_mue);
    EXPECT_EQ(expected_wheel_states.rear_right_mue, actual_wheel_states.rear_right_mue);
}

TEST(VehicleEntityTest, GivenVehicleEntity_WhenSettingSteeringWheelAngle_ThenSteeringWheelAngleCanBeRead)
{
    const units::angle::radian_t expected_steering_wheel_angle{units::angle::degree_t{42}};
    VehicleEntity vehicle(0, "name");

    vehicle.SetSteeringWheelAngle(expected_steering_wheel_angle);

    EXPECT_EQ(expected_steering_wheel_angle, vehicle.GetSteeringWheelAngle());
}

}  // namespace gtgen::core::environment::entities
