/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Entities/entity_container.h"

#include "Core/Environment/Entities/vehicle_entity.h"
#include "Core/Tests/TestUtils/expect_extensions.h"

#include <MantleAPI/Common/vector.h>
#include <gtest/gtest.h>

namespace gtgen::core::environment::entities
{

using units::literals::operator""_m;
using units::literals::operator""_rad;

TEST(EntityContainerTest, GivenEntityContainer_WhenConstructing_ThenIdAndNameAreCorrect)
{
    VehicleEntity entity(169, "vehicle_name");
    EntityContainer container(&entity);

    EXPECT_EQ(169, container.GetUniqueId());
    EXPECT_EQ("vehicle_name", container.GetName());
}

TEST(EntityContainerTest, GivenEntityContainer_WhenConstructing_ThenPropertiesAreReadable)
{
    mantle_api::Vec3<units::length::meter_t> expected_bb_front{90_m, 60_m, 90_m};
    mantle_api::Vec3<units::length::meter_t> expected_bb_rear{42_m, 21_m, 10.5_m};
    units::length::meter_t expected_front_wheel_diameter{10.0};
    units::length::meter_t expected_rear_wheel_diameter{12.0};
    mantle_api::VehicleClass expected_classification = mantle_api::VehicleClass::kLuxury_car;

    auto properties = std::make_unique<mantle_api::VehicleProperties>();
    properties->classification = expected_classification;

    properties->rear_axle.bb_center_to_axle_center = expected_bb_rear;
    properties->front_axle.bb_center_to_axle_center = expected_bb_front;

    properties->front_axle.wheel_diameter = expected_front_wheel_diameter;
    properties->rear_axle.wheel_diameter = expected_rear_wheel_diameter;

    properties->bounding_box.dimension.length = 4_m;
    properties->bounding_box.dimension.width = 2_m;
    properties->bounding_box.dimension.height = 2_m;
    properties->is_host = true;

    VehicleEntity entity(0, "name");
    entity.SetProperties(std::move(properties));
    EntityContainer container(&entity);

    auto contaier_properties = dynamic_cast<mantle_api::VehicleProperties*>(container.GetProperties());

    mantle_api::Dimension3 expected_dimensions{4_m, 2_m, 2_m};

    EXPECT_EQ(expected_classification, contaier_properties->classification);
    EXPECT_EQ(expected_bb_rear, contaier_properties->rear_axle.bb_center_to_axle_center);
    EXPECT_EQ(expected_bb_front, contaier_properties->front_axle.bb_center_to_axle_center);

    EXPECT_EQ(expected_front_wheel_diameter, contaier_properties->front_axle.wheel_diameter);
    EXPECT_EQ(expected_rear_wheel_diameter, contaier_properties->rear_axle.wheel_diameter);

    auto dimensions = contaier_properties->bounding_box.dimension;
    EXPECT_EQ(dimensions.length, expected_dimensions.length);
    EXPECT_EQ(dimensions.width, expected_dimensions.width);
    EXPECT_EQ(dimensions.height, expected_dimensions.height);

    EXPECT_TRUE(contaier_properties->is_host);
}

TEST(EntityContainerTest, GivenEntityContainer_WhenConstructing_ThenContainerStateIsEqualToEntityState)
{
    VehicleEntity entity(169, "vehicle_name");

    units::velocity::meters_per_second_t base_velocity{1};
    units::acceleration::meters_per_second_squared_t base_acceleration{1};
    units::angular_velocity::radians_per_second_t base_orientation_rate{1};
    units::angular_acceleration::radians_per_second_squared_t base_orientation_acceleration{1};

    mantle_api::Vec3<units::length::meter_t> position{1_m, 1_m, 1_m};
    mantle_api::Vec3<units::velocity::meters_per_second_t> velocity{base_velocity, base_velocity, base_velocity};
    mantle_api::Vec3<units::acceleration::meters_per_second_squared_t> acceleration{
        base_acceleration, base_acceleration, base_acceleration};
    mantle_api::Orientation3<units::angle::radian_t> orientation{1_rad, 1_rad, 1_rad};
    mantle_api::Orientation3<units::angular_velocity::radians_per_second_t> orientation_rate{
        base_orientation_rate, base_orientation_rate, base_orientation_rate};
    mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t> orientation_acceleration{
        base_orientation_acceleration, base_orientation_acceleration, base_orientation_acceleration};
    std::vector<std::uint64_t> assigned_lane_ids{1, 2, 3};
    mantle_api::EntityVisibilityConfig visibility{true, false, false, {"sensor_1", "sensor_2"}};

    entity.SetPosition(position);
    entity.SetVelocity(velocity);
    entity.SetAcceleration(acceleration);
    entity.SetOrientation(orientation);
    entity.SetOrientationRate(orientation_rate);
    entity.SetOrientationAcceleration(orientation_acceleration);
    entity.SetAssignedLaneIds(assigned_lane_ids);
    entity.SetVisibility(visibility);

    EntityContainer container(&entity);

    EXPECT_TRIPLE(position, container.GetPosition());
    EXPECT_TRIPLE(velocity, container.GetVelocity());
    EXPECT_TRIPLE(acceleration, container.GetAcceleration());
    EXPECT_TRIPLE(orientation, container.GetOrientation());
    EXPECT_TRIPLE(orientation_rate, container.GetOrientationRate());
    EXPECT_TRIPLE(orientation_acceleration, container.GetOrientationAcceleration());
    EXPECT_EQ(assigned_lane_ids, container.GetAssignedLaneIds());
    auto container_visibility = container.GetVisibility();
    EXPECT_EQ(visibility.graphics, container_visibility.graphics);
    EXPECT_EQ(visibility.traffic, container_visibility.traffic);
    EXPECT_EQ(visibility.sensors, container_visibility.sensors);
    EXPECT_EQ(visibility.sensor_names, container_visibility.sensor_names);
}

TEST(EntityContainerTest, GivenEntityContainer_WhenUpdateEntity_ThenEntityStateIsUpdated)
{
    VehicleEntity entity(169, "vehicle_name");

    mantle_api::Vec3<units::length::meter_t> position{1_m, 1_m, 1_m};
    mantle_api::Orientation3<units::angle::radian_t> orientation{1_rad, 1_rad, 1_rad};
    entity.SetPosition(position);
    entity.SetOrientation(orientation);

    EntityContainer container(&entity);

    mantle_api::Vec3<units::length::meter_t> new_position{4_m, 4_m, 4_m};
    mantle_api::Orientation3<units::angle::radian_t> new_orientation{4_rad, 4_rad, 4_rad};
    container.SetPosition(new_position);
    container.SetOrientation(new_orientation);

    EXPECT_TRIPLE(position, container.GetPosition());
    EXPECT_TRIPLE(orientation, container.GetOrientation());
    EXPECT_TRIPLE(position, entity.GetPosition());
    EXPECT_TRIPLE(orientation, entity.GetOrientation());

    container.UpdateEntity();

    EXPECT_TRIPLE(new_position, container.GetPosition());
    EXPECT_TRIPLE(new_orientation, container.GetOrientation());
    EXPECT_TRIPLE(new_position, entity.GetPosition());
    EXPECT_TRIPLE(new_orientation, entity.GetOrientation());
}

}  // namespace gtgen::core::environment::entities
