cc_library(
    name = "basic_data_buffer_implementation",
    srcs = ["basic_data_buffer_implementation.cpp"],
    hdrs = ["basic_data_buffer_implementation.h"],
    visibility = [
        "//:__pkg__",
        "//Core/Environment/Collision:__subpackages__",
        "//Core/Environment/GtGenEnvironment:__subpackages__",
    ],
    deps = [
        ":data_buffer_interface",
    ],
)

cc_test(
    name = "basic_data_buffer_implementation_test",
    timeout = "short",
    srcs = ["basic_data_buffer_implementation_test.cpp"],
    deps = [
        ":basic_data_buffer_implementation",
        ":utils",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "cyclics",
    srcs = ["cyclics.cpp"],
    hdrs = ["cyclics.h"],
    visibility = [
        "//:__pkg__",
    ],
    deps = [
    ],
)

cc_test(
    name = "cyclics_test",
    timeout = "short",
    srcs = ["cyclics_test.cpp"],
    deps = [
        ":cyclics",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "data_buffer_interface",
    srcs = ["data_buffer_interface.cpp"],
    hdrs = ["data_buffer_interface.h"],
    visibility = [
        "//:__pkg__",
        "//Core/Environment/Collision:__subpackages__",
        "//Core/Environment/GtGenEnvironment:__subpackages__",
    ],
    deps = [
        "@mantle_api",
    ],
)

cc_library(
    name = "mock_data_buffer_interface",
    testonly = True,
    srcs = ["mock_data_buffer_interface.cpp"],
    hdrs = ["mock_data_buffer_interface.h"],
    visibility = [
        "//:__pkg__",
    ],
    deps = [
        ":data_buffer_interface",
        "@googletest//:gtest",
    ],
)

cc_library(
    name = "output_generator",
    srcs = ["output_generator.cpp"],
    hdrs = ["output_generator.h"],
    visibility = [
        "//:__pkg__",
        "//Core/Environment/Collision:__subpackages__",
        "//Core/Environment/Controller:__subpackages__",
        "//Core/Environment/GtGenEnvironment:__subpackages__",
    ],
    deps = [
        ":basic_data_buffer_implementation",
        ":cyclics",
        ":data_buffer_interface",
        ":utils",
        "//Core/Environment/Exception:exception",
        "//Core/Service/FileSystem:file_system",
        "//Core/Service/FileSystem:file_system_utils",
        "//Core/Service/Logging:logging",
        "@fmt",
    ],
)

cc_library(
    name = "test_utils",
    srcs = ["test_utils.cpp"],
    hdrs = ["test_utils.h"],
    visibility = [
        "//visibility:public",
    ],
    deps = [
        "//Core/Service/Exception:exception",
        "//Core/Service/FileSystem:file_system",
    ],
)

cc_test(
    name = "output_generator_test",
    timeout = "short",
    srcs = ["output_generator_test.cpp"],
    deps = [
        ":mock_data_buffer_interface",
        ":output_generator",
        ":test_utils",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "utils",
    srcs = ["utils.cpp"],
    hdrs = ["utils.h"],
    visibility = [
        "//:__pkg__",
        "//Core/Environment/GtGenEnvironment:__subpackages__",
    ],
    deps = [
        "@//third_party/nlohmann_json",
    ],
)

cc_test(
    name = "utils_test",
    timeout = "short",
    srcs = ["utils_test.cpp"],
    deps = [
        ":data_buffer_interface",
        ":utils",
        "//Core/Service/FileSystem:file_system",
        "@//third_party/nlohmann_json",
        "@googletest//:gtest_main",
    ],
)
