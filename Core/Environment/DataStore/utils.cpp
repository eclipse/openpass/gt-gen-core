/********************************************************************************
 * Copyright (c) 2024-2025 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Core/Environment/DataStore/utils.h"

#include <fstream>

namespace gtgen::core::environment::datastore::utils
{

void WriteJson(const nlohmann::ordered_json& json_object, const std::string& filename)
{
    if (!json_object.empty())
    {
        std::ofstream json_output_file;
        json_output_file.open(filename);
        json_output_file << json_object.dump(4) << std::endl;
        json_output_file.close();
    }
}

nlohmann::ordered_json ParseJson(const std::string& filename, const bool allow_exceptions)
{
    nlohmann::ordered_json json_object{};
    if (!filename.empty())
    {
        std::ifstream json_input_stream{filename};
        if (json_input_stream.is_open())
        {
            const nlohmann::ordered_json::parser_callback_t callback_filter{nullptr};
            json_object = nlohmann::ordered_json::parse(json_input_stream, callback_filter, allow_exceptions);
        }
    }
    return json_object;
}

}  // namespace gtgen::core::environment::datastore::utils
