/********************************************************************************
 * Copyright (c) 2024-2025 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#ifndef GTGEN_CORE_ENVIRONMENT_DATASTORE_OUTPUTGENERATOR_H
#define GTGEN_CORE_ENVIRONMENT_DATASTORE_OUTPUTGENERATOR_H

#include "Core/Environment/DataStore/basic_data_buffer_implementation.h"
#include "Core/Environment/DataStore/cyclics.h"
#include "Core/Service/FileSystem/filesystem.h"
#include "MantleAPI/Traffic/i_entity.h"

#include <MantleAPI/Common/time_utils.h>
#include <nlohmann/json.hpp>

#include <cstdint>

namespace gtgen::core::environment::datastore
{

class DataBufferInterface;

class OutputGenerator
{
  public:
    class Settings
    {
      public:
        /// @param[in] output_path Path to the directory where the output will be created.
        /// @param[in] current_run_number Data buffer from where the cyclics are retrieved each simulation step.
        /// @param[in] total_runs The total planned number of runs with the same input data but different seeds.
        Settings(const fs::path& output_path, std::int32_t current_run_number = 0, std::int32_t total_runs = 1);

        /// @brief Return global output folder.
        fs::path GetGlobalOutputFolder() const { return output_path_; }

        /// @brief Return current run number.
        std::uint32_t GetCurrentRunNumber() const { return current_run_number_; }

        /// @brief Return filename for csv file.
        std::string GetCyclicsFilename() const;

        /// @brief Return filename for acyclic data file.
        std::string GetAcyclicsFilename() const;

        /// @brief Return filename for core information json file.
        std::string GetCoreInformationFilename() const;

        /// @brief Return output folder for specific entity.
        fs::path GetEntityOutputFolder(const mantle_api::UniqueId entity_id) const;

      private:
        /// @brief Return string of run_number with leading zeros.
        std::string GetRunIdWithLeadingZeros() const;

        /// @brief The outputh directory path.
        fs::path output_path_;
        /// @brief Current run number.
        std::int32_t current_run_number_;
        /// @brief Total planned amount of runs
        std::int32_t total_runs_;
    };

    /// @param[in] settings Config information.
    OutputGenerator(const Settings& settings);

    OutputGenerator(const OutputGenerator&) = delete;
    OutputGenerator(OutputGenerator&&) = delete;
    OutputGenerator& operator=(const OutputGenerator&) & = delete;
    OutputGenerator& operator=(OutputGenerator&&) & = delete;

    ~OutputGenerator() = default;

    /// @brief Create the output directory, if does not exist. Load CoreInformation.json from previous runs.
    void Init();

    /// @brief Return config settings.
    const Settings& GetSettings() const { return settings_; }

    /// @brief Writes cyclic information
    /// @param[in]   entity_id   Id of the associated agent or object
    /// @param[in]   key        Unique topic identification
    /// @param[in]   value      Value to be written
    void PutCyclic(const mantle_api::UniqueId entity_id, const Key& key, const Value& value);

    /// @brief Set acyclic information related to the @c entity_id.
    ///
    /// @param[in] entity_id Id of the entity
    /// @param[in] key       Parameter key of the cyclic
    /// @param[in] acyclic   ayclic data
    void PutAcyclic(const mantle_api::UniqueId entity_id, const Key& key, const Acyclic& acyclic);

    /// @brief Update the stored cyclics with the latest values from the buffer. Clear the buffer.
    /// @param[in] time Timestamp in milliseconds to which the cyclics refer.
    void Step(const mantle_api::Time& time);

    /// @brief Get current nlohmann::json struct for CoreInformation.json
    const nlohmann::ordered_json& GetCoreInformation() const;

    /// @brief Overwrite current nlohmann::json struct for CoreInformation.json
    /// @param[in] core_information nlohmann::json struct which will overwrite current core_information
    void SetCoreInformation(const nlohmann::ordered_json& core_information);

    /// @brief Create the output files, if data is present. Clear the data
    void FinishRun(const bool write_cyclics = true);

  private:
    /// @brief Update the stored cyclics with the latest values.
    /// @param[in] time Timestamp in milliseconds to which the cyclics refer.
    void StoreCyclics(std::int32_t time);

    /// @brief Output the .csv file with the cyclics.
    void WriteCyclics();

    /// @brief Update the stored acyclics with the latest values.
    /// @param[in] time Timestamp in milliseconds to which the cyclics refer.
    void StoreAcyclics(std::int32_t time);

    /// @brief Output acyclic data to json file.
    void WriteAcyclics();

    /// @brief Append parameters acyclic data to properties field of the events' json object
    /// @param json_object object to append parameters
    /// @param parameters  map of parameters
    void WriteProperties(nlohmann::ordered_json& json_object, const Parameter& parameters);

    /// @brief Read CoreInformation.json
    nlohmann::ordered_json ReadCoreInformationJson() const;

    /// @brief Write CoreInformation.json
    void WriteCoreInformationJson();

    /// @brief Config information.
    Settings settings_;

    /// @brief Data buffer with the cyclics information for each step.
    gtgen::core::environment::datastore::BasicDataBufferImplementation data_buffer_;

    /// @brief Container for the cyclics.
    Cyclics cyclics_;

    /// @brief Container for events
    Events events_;

    /// @brief Container with data for CoreInfomration.json
    nlohmann::ordered_json core_information_;
};

}  // namespace gtgen::core::environment::datastore

#endif  // GTGEN_CORE_ENVIRONMENT_DATASTORE_OUTPUTGENERATOR_H
