/********************************************************************************
 * Copyright (c) 2020-2022 in-tech GmbH
 * Copyright (c) 2024-2025 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#ifndef GTGEN_CORE_ENVIRONMENT_DATASTORE_MOCKDATABUFFERINTERFACE_H
#define GTGEN_CORE_ENVIRONMENT_DATASTORE_MOCKDATABUFFERINTERFACE_H

#include "Core/Environment/DataStore/data_buffer_interface.h"

#include <gmock/gmock.h>

namespace gtgen::core::environment::datastore
{

class FakeDataBuffer : public DataBufferInterface
{
  public:
    MOCK_CONST_METHOD2(GetCyclic,
                       std::unique_ptr<CyclicResultInterface>(const std::optional<mantle_api::UniqueId> entity_id,
                                                              const Key& key));
    MOCK_METHOD3(PutCyclic, void(const mantle_api::UniqueId entity_id, const Key& key, const Value& value));
    MOCK_METHOD3(PutAcyclic, void(const mantle_api::UniqueId entity_id, const Key& key, const Acyclic& value));

    MOCK_CONST_METHOD2(GetAcyclic,
                       std::unique_ptr<AcyclicResultInterface>(const std::optional<mantle_api::UniqueId> entity_id,
                                                               const Key& key));

    MOCK_METHOD0(Clear, void());
};

class FakeCyclicResult : public CyclicResultInterface
{
  public:
    MOCK_CONST_METHOD0(size, std::size_t());
    MOCK_CONST_METHOD1(at, const CyclicRow&(const std::size_t));
    MOCK_CONST_METHOD0(begin, CyclicRowRefs::const_iterator());
    MOCK_CONST_METHOD0(end, CyclicRowRefs::const_iterator());
};

}  // namespace gtgen::core::environment::datastore

#endif  // GTGEN_CORE_ENVIRONMENT_DATASTORE_MOCKDATABUFFERINTERFACE_H
