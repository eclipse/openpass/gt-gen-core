/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include "Core/Environment/DataStore/utils.h"

#include "Core/Environment/DataStore/data_buffer_interface.h"
#include "Core/Service/FileSystem/filesystem.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <nlohmann/json.hpp>

#include <fstream>

namespace gtgen::core::environment::datastore
{

using std::literals::string_literals::operator""s;
class UtilsTestFixture : public testing::TestWithParam<std::tuple<Value, std::string>>
{
};

INSTANTIATE_TEST_SUITE_P(Utils,
                         UtilsTestFixture,
                         testing::ValuesIn(std::vector<std::tuple<Value, std::string>>{
                             std::make_tuple(true, "1"s),
                             std::make_tuple('#', std::to_string('#')),
                             std::make_tuple(123, "123"s),
                             std::make_tuple(std::size_t{123}, "123"s),
                             std::make_tuple(1.1f, "1.100000"s),
                             std::make_tuple(1.1, "1.100000"s),
                             std::make_tuple("abc"s, "abc"s),
                             std::make_tuple(std::vector<bool>{true, false}, "1,0"s),
                             std::make_tuple(std::vector<char>{'#', 'k'}, "#,k"s),
                             std::make_tuple(std::vector<int>{123, 456}, "123,456"s),
                             std::make_tuple(std::vector<std::size_t>{123, 456}, "123,456"s),
                             std::make_tuple(std::vector<float>{1.1, 2.2}, "1.1,2.2"s),
                             std::make_tuple(std::vector<double>{1.1, 2.2}, "1.1,2.2"s),
                             std::make_tuple(std::vector<std::string>{"abc", "def"}, "abc,def"s)}));

TEST_P(UtilsTestFixture, GivenVariantInput_WhenToString_ThenExpectedString)
{
    // Given
    const Value input = std::get<0>(GetParam());
    std::string expected_result = std::get<1>(GetParam());

    // When
    std::string result;
    std::visit(utils::to_string([&result](const std::string& value_str) { result = value_str; }), input);

    // Then
    EXPECT_EQ(result, expected_result);
}

TEST(WriteJsonTest, GivenJsonString_WhenWriteJson_ExpectCorrectJson)
{
    // Given
    const fs::path filename = fs::temp_directory_path() / "test_json_001.json";
    const std::string expected_string{
        R"({
    "simulatorVersion": "0.1.2",
    "schemaVersion": "3.4.5",
    "simulationRuns": [
        {
            "id": 6,
            "randomSeed": 7,
            "entities": [
                {
                    "id": 8,
                    "name": "host"
                },
                {
                    "id": 9,
                    "name": "traffic_vehicle1"
                }
            ]
        }
    ]
}
)"};
    const nlohmann::ordered_json expected_json = nlohmann::ordered_json::parse(expected_string);

    // When
    utils::WriteJson(expected_json, filename);

    // Then
    ASSERT_TRUE(fs::exists(filename)) << "File does not exist: " << (filename).string();
    std::ifstream actual_json_stream(filename);
    const nlohmann::ordered_json actual_json = nlohmann::ordered_json::parse(actual_json_stream);
    EXPECT_EQ(expected_json, actual_json);  // check for valid json
    actual_json_stream.clear();
    actual_json_stream.seekg(0, std::ios::beg);
    std::stringstream actual_string_buffer;
    actual_string_buffer << actual_json_stream.rdbuf();
    const std::string actual_string{actual_string_buffer.str()};
    EXPECT_EQ(expected_string, actual_string);  // check for expected format
    fs::remove(filename);
}

}  // namespace gtgen::core::environment::datastore
