/********************************************************************************
 * Copyright (c) 2020-2021 in-tech GmbH
 * Copyright (c) 2023-2025 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Core/Environment/DataStore/basic_data_buffer_implementation.h"

#include "Core/Environment/DataStore/data_buffer_interface.h"
#include "Core/Environment/DataStore/utils.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <cstddef>
#include <functional>
#include <memory>
#include <optional>
#include <ostream>
#include <string>
#include <utility>
#include <variant>
#include <vector>

namespace gtgen::core::environment::datastore
{

using ::testing::Eq;
using ::testing::NotNull;
using ::testing::SizeIs;

std::ostream& operator<<(std::ostream& stream, CyclicRow const& row)
{
    stream << ", entity_id = " << row.entity_id << ", key = " << row.key << " , value = ";

    std::visit(utils::to_string([&stream](const std::string& value_str) { stream << value_str; }, "|"), row.value);

    return stream << "}";
}

class TestBasicDataBuffer : public BasicDataBufferImplementation
{
  public:
    TestBasicDataBuffer() : BasicDataBufferImplementation() {}

    CyclicStore& GetStore() { return cyclic_store_; }
    AcyclicStore& GetAcyclicStore() { return acyclic_store_; }
    void SetStore(CyclicStore& new_store) { cyclic_store_ = new_store; }
};

class BasicDataBuffer_GetCyclic_Test : public ::testing::Test
{
  public:
    BasicDataBuffer_GetCyclic_Test()
    {
        implementation = std::make_unique<BasicDataBufferImplementation>();

        // fill some test data

        // value types according to openpass::databuffer::Value
        // bool, char, int, size_t, float, double, std::string and std::vector of each type

        implementation->PutCyclic(0, "bool_val", true);
        implementation->PutCyclic(0, "char_val", 'a');
        implementation->PutCyclic(0, "int_val", 1);
        implementation->PutCyclic(0, "size_val", static_cast<std::size_t>(2));
        implementation->PutCyclic(0, "float_val", 3.3f);
        implementation->PutCyclic(0, "double_val", 4.4);
        implementation->PutCyclic(0, "string_val", "str");

        implementation->PutCyclic(0, "bool_vec", std::vector<bool>{true, false});
        implementation->PutCyclic(0, "char_vec", std::vector<char>{'a'});
        implementation->PutCyclic(0, "int_vec", std::vector<char>{1, 2});
        implementation->PutCyclic(0, "size_vec", std::vector<std::size_t>{3, 4});
        implementation->PutCyclic(0, "float_vec", std::vector<float>{5.5f, 6.6f});
        implementation->PutCyclic(0, "double_vec", std::vector<double>{7.7, 8.8});
        implementation->PutCyclic(0, "string_vec", std::vector<std::string>{"str1", "str2"});

        // data with additional entity ids

        implementation->PutCyclic(1, "int_val", 2);
        implementation->PutCyclic(2, "int_val", 3);

        implementation->PutCyclic(1, "int_val_2", 22);
        implementation->PutCyclic(2, "int_val_2", 23);
    }

    std::unique_ptr<DataBufferInterface> implementation;
};

class BasicDataBuffer_Persistence_Test : public ::testing::Test
{
  public:
    BasicDataBuffer_Persistence_Test()
    {
        implementation = std::make_unique<BasicDataBufferImplementation>();

        implementation->PutCyclic(0, "cyclic_key", 2);

        Acyclic acyclic_data;
        acyclic_data.name = "AcyclicData";
        acyclic_data.triggering_entities = {1};
        acyclic_data.parameter.insert({"CollisionWithAgent", true});

        implementation->PutAcyclic(1, "acyclic_key", acyclic_data);
    }

    std::unique_ptr<DataBufferInterface> implementation;
};

TEST(BasicDataBuffer, PutCyclicData_StoresData)
{
    const mantle_api::UniqueId agent_id = 1;
    const int value = 2;
    const std::string key{"key"};
    const CyclicRow expected_row{agent_id, key, value};

    auto ds = std::make_unique<TestBasicDataBuffer>();

    ASSERT_THAT(ds, NotNull());

    ds->PutCyclic(agent_id, key, value);

    EXPECT_THAT(ds->GetStore().back(), Eq(expected_row));
}

TEST(BasicDataBuffer, PutAyclicData_StoresData)
{
    const mantle_api::UniqueId entity_id = 1;
    Acyclic acyclic_data;
    Key key = "acyclic_key";
    acyclic_data.name = "AcyclicData";
    acyclic_data.triggering_entities = {entity_id};
    acyclic_data.parameter.insert({"CollisionWithAgent", true});

    AcyclicRow expected_row{entity_id, key, acyclic_data};

    auto ds = std::make_unique<TestBasicDataBuffer>();

    ASSERT_THAT(ds, NotNull());

    ds->PutAcyclic(entity_id, key, acyclic_data);

    EXPECT_THAT(ds->GetAcyclicStore().back(), Eq(expected_row));
}

TEST(BasicAcyclicDataBufferTest, GivenAcyclicData_CheckAcyclicParameter)
{

    BasicDataBufferImplementation implementation{};

    // fill some test data
    // value types according to gtgen::core::environment::datastore::Value
    // bool, char, int, size_t, float, double, std::string and std::vector of each type
    Acyclic acyclic_data;
    mantle_api::UniqueId entity_id{1};
    const Key key = "acyclic_key";
    acyclic_data.name = "AcyclicData";
    acyclic_data.triggering_entities = {entity_id};

    acyclic_data.parameter.insert({"bool", false});
    acyclic_data.parameter.insert({"bool_vector", std::vector<bool>{false, true}});
    acyclic_data.parameter.insert({"char", 'c'});
    acyclic_data.parameter.insert({"char_vector", std::vector<char>{'c', 'a'}});
    acyclic_data.parameter.insert({"int", 11});
    acyclic_data.parameter.insert({"int_vector", std::vector<int>{1, 2, 3}});
    acyclic_data.parameter.insert({"size_t", static_cast<std::size_t>(2)});
    acyclic_data.parameter.insert({"size_t_vector", std::vector<std::size_t>{4, 5, 7}});
    acyclic_data.parameter.insert({"float", 3.3f});
    acyclic_data.parameter.insert({"float_vector", std::vector<float>{3.3f, 4.3f, 5.1f}});
    acyclic_data.parameter.insert({"double", 3.3});
    acyclic_data.parameter.insert({"double_vector", std::vector<float>{3.3, 4.3, 5.1}});
    acyclic_data.parameter.insert({"string", "random_string"});
    acyclic_data.parameter.insert({"string_vector", std::vector<std::string>{"s1", "s2", "s3"}});

    implementation.PutAcyclic(entity_id, key, acyclic_data);

    const auto result = implementation.GetAcyclic(std::optional<mantle_api::UniqueId>{entity_id}, key);

    ASSERT_EQ(result->size(), 1);
    ASSERT_EQ(result->at(0).entity_id, entity_id);
    ASSERT_EQ(result->at(0).key, key);
    ASSERT_EQ(result->at(0).data.parameter.size(), acyclic_data.parameter.size());
    ASSERT_EQ(result->at(0).data.parameter, acyclic_data.parameter);
}

TEST_F(BasicDataBuffer_GetCyclic_Test, GivenEntityIdAndKey_ReturnsCorrectData)
{
    const auto result1 = implementation->GetCyclic(1, "int_val");
    const auto result2 = implementation->GetCyclic(0, "int_val");

    ASSERT_THAT(result1->size(), Eq(1));
    ASSERT_THAT(result2->size(), Eq(1));
    EXPECT_THAT(result1->at(0), Eq(CyclicRow{1, "int_val", 2}));
    EXPECT_THAT(result2->at(0), Eq(CyclicRow{0, "int_val", 1}));
}

TEST_F(BasicDataBuffer_GetCyclic_Test, GivenKey_ReturnsCorrectResult)
{
    const auto result = implementation->GetCyclic(std::nullopt, "int_val");

    ASSERT_THAT(result->size(), Eq(3));
    EXPECT_THAT(result->at(0), Eq(CyclicRow{0, "int_val", 1}));
    EXPECT_THAT(result->at(1), Eq(CyclicRow{1, "int_val", 2}));
    EXPECT_THAT(result->at(2), Eq(CyclicRow{2, "int_val", 3}));
}

TEST_F(BasicDataBuffer_GetCyclic_Test, GivenEntityIdAndWildcard_ReturnsCorrectResult)
{
    const auto result = implementation->GetCyclic(2, kWildcard);

    ASSERT_THAT(result->size(), Eq(2));
    EXPECT_THAT(result->at(0), Eq(CyclicRow{2, "int_val", 3}));
    EXPECT_THAT(result->at(1), Eq(CyclicRow{2, "int_val_2", 23}));
}

TEST_F(BasicDataBuffer_GetCyclic_Test, GivenWildcard_ReturnsCorrectResult)
{
    const auto result = implementation->GetCyclic(std::nullopt, kWildcard);

    ASSERT_THAT(result->size(), Eq(18));

    EXPECT_THAT(result->at(0), Eq(CyclicRow{0, "bool_val", true}));
    EXPECT_THAT(result->at(1), Eq(CyclicRow{0, "char_val", 'a'}));
    EXPECT_THAT(result->at(2), Eq(CyclicRow{0, "int_val", 1}));
    EXPECT_THAT(result->at(3), Eq(CyclicRow{0, "size_val", static_cast<std::size_t>(2)}));
    EXPECT_THAT(result->at(4), Eq(CyclicRow{0, "float_val", 3.3f}));
    EXPECT_THAT(result->at(5), Eq(CyclicRow{0, "double_val", 4.4}));
    EXPECT_THAT(result->at(6), Eq(CyclicRow{0, "string_val", "str"}));

    EXPECT_THAT(result->at(7), Eq(CyclicRow{0, "bool_vec", std::vector<bool>{true, false}}));
    EXPECT_THAT(result->at(8), Eq(CyclicRow{0, "char_vec", std::vector<char>{'a'}}));
    EXPECT_THAT(result->at(9), Eq(CyclicRow{0, "int_vec", std::vector<char>{1, 2}}));
    EXPECT_THAT(result->at(10), Eq(CyclicRow{0, "size_vec", std::vector<std::size_t>{3, 4}}));
    EXPECT_THAT(result->at(11), Eq(CyclicRow{0, "float_vec", std::vector<float>{5.5f, 6.6f}}));
    EXPECT_THAT(result->at(12), Eq(CyclicRow{0, "double_vec", std::vector<double>{7.7, 8.8}}));
    EXPECT_THAT(result->at(13), Eq(CyclicRow{0, "string_vec", std::vector<std::string>{"str1", "str2"}}));

    EXPECT_THAT(result->at(14), Eq(CyclicRow{1, "int_val", 2}));
    EXPECT_THAT(result->at(15), Eq(CyclicRow{2, "int_val", 3}));

    EXPECT_THAT(result->at(16), Eq(CyclicRow{1, "int_val_2", 22}));
    EXPECT_THAT(result->at(17), Eq(CyclicRow{2, "int_val_2", 23}));
}

TEST_F(BasicDataBuffer_GetCyclic_Test, Result_IsIteratable)
{
    const auto result = implementation->GetCyclic(std::nullopt, "int_val_2");

    ASSERT_THAT(result->size(), Eq(2));

    int expected_value = 22;
    mantle_api::UniqueId id = 1;
    for (const auto& entry_ref : *result)
    {
        const auto& entry = entry_ref.get();
        EXPECT_THAT(entry, Eq(CyclicRow{id, "int_val_2", expected_value}));
        expected_value += 1;
        id += 1;
    }
}

TEST_F(BasicDataBuffer_Persistence_Test, FixtureSetup_StoresData)
{
    const auto cyclic_result = implementation->GetCyclic(std::nullopt, "cyclic_key");
    const auto acyclic_result = implementation->GetAcyclic(std::nullopt, "acyclic_key");

    ASSERT_THAT(cyclic_result->size(), Eq(1));
    ASSERT_THAT(acyclic_result->size(), Eq(1));
}

TEST_F(BasicDataBuffer_Persistence_Test, Clear_DeletesNonPersisentData)
{
    implementation->Clear();

    const auto cyclic_result = implementation->GetCyclic(std::nullopt, "cyclic_key");
    const auto acyclic_result = implementation->GetAcyclic(std::nullopt, "acyclic_key");

    ASSERT_THAT(cyclic_result->size(), Eq(0));
    ASSERT_THAT(acyclic_result->size(), Eq(0));
}

TEST(BasicDataBuffer_Constants, Wildcard_IsSize1)
{
    ASSERT_THAT(kWildcard, SizeIs(1));
}

}  // namespace gtgen::core::environment::datastore
