/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Chunking/Internal/formatting.h"
#include "Core/Environment/Chunking/Internal/polyline_chunking.h"
#include "Core/Tests/TestUtils/exception_testing.h"

#include <gtest/gtest.h>

namespace gtgen::core::environment::chunking
{
using units::literals::operator""_m;
using units::literals::operator""_rad;

using environment::map::GtGenMap;
using environment::map::Lane;
using environment::map::LaneBoundary;
using environment::map::LaneGroup;
using map_api::ReferenceLine;
using IdToPolyline = std::map<mantle_api::UniqueId, std::vector<mantle_api::Vec3<units::length::meter_t>>>;
using LaneGroupToId = std::map<mantle_api::UniqueId, std::vector<mantle_api::UniqueId>>;
using ExpectedChunkIds =
    std::vector<std::tuple<ChunkKey, std::vector<mantle_api::UniqueId>, std::vector<mantle_api::UniqueId>>>;

class PolylineChunkingMapTest : public testing::Test
{
  public:
    void AddLanesToGtGenMap(IdToPolyline& id_to_poly_line_association,
                            LaneGroupToId& lane_group_id_to_lane_id_association)
    {
        for (const auto& [lane_goup_id, lane_ids] : lane_group_id_to_lane_id_association)
        {
            LaneGroup lane_group{lane_goup_id, LaneGroup::Type::kUnknown};
            map_.AddLaneGroup(lane_group);
            for (const auto lane_id : lane_ids)
            {
                Lane lane{lane_id};
                lane.center_line = id_to_poly_line_association[lane_id];
                map_.AddLane(lane_goup_id, lane);
            }
        }
    }

    void AddReferenceLinesToGtGenMap(IdToPolyline& id_to_poly_line_association)
    {
        for (const auto& [reference_line_id, poly_line] : id_to_poly_line_association)
        {
            ReferenceLine reference_line;
            reference_line.id = reference_line_id;
            for (auto point : poly_line)
            {
                reference_line.poly_line.push_back({point, 0_m, 0_rad});
            }
            map_.Add(reference_line);
        }
    }

    template <class InnerDataVector>
    bool ContainElement(const InnerDataVector& data_vector, mantle_api::UniqueId id)
    {
        return std::count_if(
            data_vector.begin(), data_vector.end(), [&id](const auto* element) { return element->id == id; });
    }

    void CheckChunkContent(StaticChunkList& chunks, ExpectedChunkIds& expected_chunk_ids)
    {
        // number of chunks should be equal to number of elements in expected_chunk_ids
        ASSERT_EQ(expected_chunk_ids.size(), chunks.size());
        for (auto& [chunk_key, lane_group_ids, reference_line_ids] : expected_chunk_ids)
        {
            // check existence of chunk
            EXPECT_EQ(1, chunks.count(chunk_key));
            // check number of lane groups in this chunk
            const auto& current_chunk{chunks.at(chunk_key)};
            EXPECT_EQ(lane_group_ids.size(), current_chunk.lane_groups.size());
            EXPECT_EQ(reference_line_ids.size(), current_chunk.reference_lines.size());
            for (auto lane_group_id : lane_group_ids)
            {
                // check if every lane group is in this chunk
                EXPECT_TRUE(ContainElement(current_chunk.lane_groups, lane_group_id))
                    << fmt::format("Error for chunk key {} - lane group id {}", chunk_key, lane_group_id);
            }
            for (auto reference_line_id : reference_line_ids)
            {
                // check if every reference line is in this chunk
                EXPECT_TRUE(ContainElement(current_chunk.reference_lines, reference_line_id))
                    << fmt::format("Error for chunk key {} - reference line id {}", chunk_key, reference_line_id);
            }
        }
    }

  protected:
    PolylineChunkingMapTest() { chunking_.SetChunkSize(50.0); }

    GtGenMap map_;
    PolylineChunking chunking_;
};

TEST_F(PolylineChunkingMapTest,
       GivenLaneWithTwoPointsSpreadAcrossFourDiagonalChunksNorthWest_WhenInitializeChunks_ThenNumberOfChunksCorrect)
{
    IdToPolyline id_to_poly_line_association = {{1, {{-25.0_m, 25.0_m, 0.0_m}, {-175.0_m, 175.0_m, 0.0_m}}}};
    LaneGroupToId lane_goup_id_to_lane_id_association = {{1001, {1}}};

    AddLanesToGtGenMap(id_to_poly_line_association, lane_goup_id_to_lane_id_association);

    StaticChunkList chunks = chunking_.InitializeChunks(map_);
    EXPECT_EQ(chunks.size(), 10);
}

TEST_F(PolylineChunkingMapTest,
       GivenLaneWithTwoPointsSpreadAcrossFourDiagonalChunksSouthEast_WhenInitializeChunks_ThenNumberOfChunksCorrect)
{
    IdToPolyline id_to_poly_line_association = {{1, {{25.0_m, -25.0_m, 0.0_m}, {175.0_m, -175.0_m, 0.0_m}}}};
    LaneGroupToId lane_goup_id_to_lane_id_association = {{1001, {1}}};

    AddLanesToGtGenMap(id_to_poly_line_association, lane_goup_id_to_lane_id_association);

    StaticChunkList chunks = chunking_.InitializeChunks(map_);
    EXPECT_EQ(chunks.size(), 10);
}

TEST_F(PolylineChunkingMapTest, GivenLanesInSingleChunk_WhenInitializeChunks_ThenNumberOfChunksCorrect)
{
    // clang-format off
    IdToPolyline id_to_poly_line_association = {
        // Chunk {0, 0}
        {1,
         {{0.0_m, 0.0_m, 0.0_m},
         {10.0_m, 10.0_m, 0.0_m},
         {20.0_m, 20.0_m, 20.0_m}}},
        // Chunk {1, 1}
        {2,
         {{60.0_m, 50.0_m, 0.0_m},
         {55.0_m, 55.0_m, 0.0_m}}},
        // Chunk {0, 0}
        {3,
         {{0.0_m, 0.0_m, 0.0_m},
         {10.0_m, 0.0_m, 0.0_m},
         {20.0_m, 0.0_m, 0.0_m}}}};
    LaneGroupToId lane_goup_id_to_lane_id_association = {
        {1001, {1, 2}},
        {1002, {3}}};

    ExpectedChunkIds expected_chunk_ids = {
        {{0, 0}, {1001, 1002}, {1, 3}},
        {{1, 1}, {1001}, {2}}};
    // clang-format on
    AddLanesToGtGenMap(id_to_poly_line_association, lane_goup_id_to_lane_id_association);
    AddReferenceLinesToGtGenMap(id_to_poly_line_association);

    StaticChunkList chunks = chunking_.InitializeChunks(map_);
    CheckChunkContent(chunks, expected_chunk_ids);
}

TEST_F(PolylineChunkingMapTest,
       GivenLanesInMultipleChunkButPointsInAdjacentChunks_WhenInitializeChunks_ThenNumberOfChunksCorrect)
{
    // clang-format off
    IdToPolyline id_to_poly_line_association = {
        // Chunk {0, 0} / {1, 0}
        {1,
         {{0.0_m, 0.0_m, 0.0_m},
          {10.0_m, 10.0_m, 0.0_m},
          {20.0_m, 20.0_m, 20.0_m},
          {60.0_m, 20.0_m, 0.0_m},
          {80.0_m, 25.0_m, 0.0_m}}},
        // Chunk {1, 1} / {1, 2}
        {2,
         {{60.0_m, 50.0_m, 0.0_m},
          {55.0_m, 55.0_m, 0.0_m},
          {60.0_m, 102.0_m, 0.0_m}}},
        // Chunk {0, 0} / {-1, 0}
        {3,
         {{0.0_m, 0.0_m, 0.0_m},
         {10.0_m, 0.0_m, 0.0_m},
         {20.0_m, 0.0_m, 0.0_m},
         {-10.0_m, 0.0_m, 0.0_m}}}};
    LaneGroupToId lane_goup_id_to_lane_id_association = {
        {1001, {1, 2}},
        {1002, {3}}};

    ExpectedChunkIds expected_chunk_ids = {
        {{0, 0}, {1001, 1002}, {1, 3}},
        {{1, 0}, {1001}, {1}},
        {{1, 1}, {1001}, {2}},
        {{1, 2}, {1001}, {2}},
        {{-1, 0}, {1002}, {3}}};
    // clang-format on
    AddLanesToGtGenMap(id_to_poly_line_association, lane_goup_id_to_lane_id_association);
    AddReferenceLinesToGtGenMap(id_to_poly_line_association);

    StaticChunkList chunks = chunking_.InitializeChunks(map_);
    CheckChunkContent(chunks, expected_chunk_ids);
}  // namespace gtgen::core::environment::chunking

TEST_F(PolylineChunkingMapTest,
       GivenLanesInMultipleChunkAndPointsNotOnlyInAdjacentChunks_WhenInitializeChunks_ThenNumberOfChunksCorrect)
{
    // clang-format off
    IdToPolyline id_to_poly_line_association = {
        // Chunk {0, 0} / {1, 0} / {2, 1} -- intersecting {2, 0}
        {1,
         {{0.0_m, 0.0_m, 0.0_m},
          {10.0_m, 10.0_m, 0.0_m},
          {20.0_m, 20.0_m, 20.0_m},
          {60.0_m, 20.0_m, 0.0_m},
          {80.0_m, 25.0_m, 0.0_m},
          {120.0_m, 55.0_m, 0.0_m}}},
        // Chunk {1, 1} / {1, 2}
        {2,
         {{60.0_m, 50.0_m, 0.0_m},
          {55.0_m, 55.0_m, 0.0_m},
          {60.0_m, 102.0_m, 0.0_m}}},
        // Chunk {0, 0} / {-1, 0}
        {3,
         {{0.0_m, 0.0_m, 0.0_m},
          {10.0_m, 0.0_m, 0.0_m},
          {20.0_m, 0.0_m, 0.0_m},
          {-10.0_m, 0.0_m, 0.0_m}}},
        // Chunk {2, 1} / {3, 1} / {4, 1} / {4, 2} / {5, 2} / {5, 3} / {6, 2} / {6, 3}
        // Last line segment goes "directly" from {5, 2} to {6, 3} and there also touches {5, 3} and {6, 2}
        {4,
         {{111.0_m, 60.0_m, 0.0_m},
          {220.0_m, 90.0_m, 0.0_m},
          {275.0_m, 125.0_m, 0.0_m},
          {325.0_m, 175.0_m, 0.0_m}}},
        // Chunk {2, 1} / {2, 2} / {3, 2} / {3, 3} / {4, 3} / {4, 4} / {5, 4} / {5, 5} / {6, 5} / {6, 6}
        {5,
         {{131.0_m, 88.0_m, 0.0_m},
          {333.0_m, 333.0_m, 0.0_m}}}};

    LaneGroupToId lane_goup_id_to_lane_id_association = {
        {1001, {1, 2}},
        {1002 ,{3, 4}},
        {1003 ,{5}}};

    ExpectedChunkIds expected_chunk_ids = {
        {{0, 0}, {1001, 1002}, {1, 3}},
        {{2, 1}, {1001, 1002, 1003}, {1, 4, 5}},
        {{1, 0}, {1001}, {1}},
        {{1, 1}, {1001}, {2}},
        {{1, 2}, {1001}, {2}},
        {{2, 0}, {1001}, {1}},
        {{-1, 0}, {1002}, {3}},
        {{3, 1}, {1002}, {4}},
        {{4, 1}, {1002}, {4}},
        {{4, 2}, {1002}, {4}},
        {{5, 2}, {1002}, {4}},
        {{5, 3}, {1002}, {4}},
        {{6, 2}, {1002}, {4}},
        {{6, 3}, {1002}, {4}},
        {{2, 2}, {1003}, {5}},
        {{3, 2}, {1003}, {5}},
        {{3, 3}, {1003}, {5}},
        {{4, 3}, {1003}, {5}},
        {{4, 4}, {1003}, {5}},
        {{5, 4}, {1003}, {5}},
        {{5, 5}, {1003}, {5}},
        {{6, 5}, {1003}, {5}},
        {{6, 6}, {1003}, {5}}};
    // clang-format on
    AddLanesToGtGenMap(id_to_poly_line_association, lane_goup_id_to_lane_id_association);
    AddReferenceLinesToGtGenMap(id_to_poly_line_association);

    StaticChunkList chunks = chunking_.InitializeChunks(map_);
    CheckChunkContent(chunks, expected_chunk_ids);
}

TEST_F(
    PolylineChunkingMapTest,
    GivenLanesInMultipleChunkWithJumpingChunksViaBoundariesAndOrdering_WhenInitializeChunks_ThenNumberOfChunksCorrect)
{
    Lane lane1{0};
    // Chunk {2, 1} / {2, 2} / {3, 2} / {3, 3} / {4, 3} / {4, 4} / {5, 4} / {5, 5} / {6, 5} / {6, 6}
    // also tests the re-ordering of the chunk box that is looped over
    LaneBoundary left_lane_boundary_1{2001,
                                      LaneBoundary::Type::kSolidLine,
                                      LaneBoundary::Color::kWhite,
                                      {{{333.0_m, 333.0_m, 0.0_m}, 0.5, 0.1}, {{131.0_m, 88.0_m, 0.0_m}, 0.5, 0.1}}};

    // Adds Chunk {3, 4}
    LaneBoundary left_lane_boundary_2{2003,
                                      LaneBoundary::Type::kSolidLine,
                                      LaneBoundary::Color::kWhite,
                                      {{{165.0_m, 220.0_m, 0.0_m}, 0.5, 0.1}, {{170.0_m, 225.0_m, 0.0_m}, 0.5, 0.1}}};

    // Adds Chunk{6, 5}
    LaneBoundary right_lane_boundary_1{2002,
                                       LaneBoundary::Type::kSolidLine,
                                       LaneBoundary::Color::kWhite,
                                       {{{125.0_m, 95.0_m, 0.0_m}, 0.5, 0.1}, {{320.0_m, 330.0_m, 0.0_m}, 0.5, 0.1}}};

    mantle_api::UniqueId lane_group_1_id = 1001;
    lane1.left_lane_boundaries.emplace_back(2001);
    lane1.left_lane_boundaries.emplace_back(2003);
    lane1.right_lane_boundaries.emplace_back(2002);

    Lane lane2{1};
    // Chunk {2, 4} / {3, 4}
    LaneBoundary lane2_left_boundary_1{2005,
                                       LaneBoundary::Type::kSolidLine,
                                       LaneBoundary::Color::kWhite,
                                       {{{125.0_m, 220.0_m, 0.0_m}, 0.5, 0.1}, {{155.0_m, 220.0_m, 0.0_m}, 0.5, 0.1}}};
    // Chunk {2, 3} / {3, 3}
    LaneBoundary lane2_left_boundary_2{2006,
                                       LaneBoundary::Type::kSolidLine,
                                       LaneBoundary::Color::kWhite,
                                       {{{125.0_m, 180.0_m, 0.0_m}, 0.5, 0.1}, {{155.0_m, 180.0_m, 0.0_m}, 0.5, 0.1}}};
    // Chunk {2, 5} / {3, 5}
    LaneBoundary lane2_right_boundary_1{2007,
                                        LaneBoundary::Type::kSolidLine,
                                        LaneBoundary::Color::kWhite,
                                        {{{125.0_m, 260.0_m, 0.0_m}, 0.5, 0.1}, {{155.0_m, 270.0_m, 0.0_m}, 0.5, 0.1}}};
    // Chunk {2, 6} / {3, 6}
    LaneBoundary lane2_right_boundary_2{2008,
                                        LaneBoundary::Type::kSolidLine,
                                        LaneBoundary::Color::kWhite,
                                        {{{125.0_m, 310.0_m, 0.0_m}, 0.5, 0.1}, {{155.0_m, 310.0_m, 0.0_m}, 0.5, 0.1}}};

    mantle_api::UniqueId lane_group_2_id = 1002;
    lane2.left_lane_boundaries = {2005, 2006};
    lane2.right_lane_boundaries = {2007, 2008};

    Lane lane3{2};
    // Chunk {8, 4} / {9, 4}
    LaneBoundary lane3_left_boundary_1{2010,
                                       LaneBoundary::Type::kSolidLine,
                                       LaneBoundary::Color::kWhite,
                                       {{{405.0_m, 220.0_m, 0.0_m}, 0.5, 0.1}, {{455.0_m, 220.0_m, 0.0_m}, 0.5, 0.1}}};
    // Chunk {8, 3} / {9, 3}
    LaneBoundary lane3_left_boundary_2{2011,
                                       LaneBoundary::Type::kSolidLine,
                                       LaneBoundary::Color::kWhite,
                                       {{{405.0_m, 180.0_m, 0.0_m}, 0.5, 0.1}, {{455.0_m, 180.0_m, 0.0_m}, 0.5, 0.1}}};
    // Chunk {8, 5} / {9, 5}
    LaneBoundary lane3_right_boundary_1{2012,
                                        LaneBoundary::Type::kSolidLine,
                                        LaneBoundary::Color::kWhite,
                                        {{{405.0_m, 260.0_m, 0.0_m}, 0.5, 0.1}, {{455.0_m, 270.0_m, 0.0_m}, 0.5, 0.1}}};
    // Chunk {8, 6} / {9, 6}
    LaneBoundary lane3_right_boundary_2{2013,
                                        LaneBoundary::Type::kSolidLine,
                                        LaneBoundary::Color::kWhite,
                                        {{{405.0_m, 310.0_m, 0.0_m}, 0.5, 0.1}, {{455.0_m, 310.0_m, 0.0_m}, 0.5, 0.1}}};
    lane3.left_lane_boundaries = {2010, 2011};
    lane3.right_lane_boundaries = {2012, 2013};

    GtGenMap map;

    map.AddLaneGroup({lane_group_1_id, LaneGroup::Type::kUnknown});
    map.AddLane(lane_group_1_id, lane1);
    map.AddLaneBoundary(lane_group_1_id, left_lane_boundary_1);
    map.AddLaneBoundary(lane_group_1_id, right_lane_boundary_1);
    map.AddLaneBoundary(lane_group_1_id, left_lane_boundary_2);

    map.AddLaneGroup({lane_group_2_id, LaneGroup::Type::kUnknown});
    map.AddLane(lane_group_2_id, lane2);
    map.AddLane(lane_group_2_id, lane3);
    map.AddLaneBoundary(lane_group_2_id, lane2_left_boundary_1);
    map.AddLaneBoundary(lane_group_2_id, lane2_left_boundary_2);
    map.AddLaneBoundary(lane_group_2_id, lane2_right_boundary_1);
    map.AddLaneBoundary(lane_group_2_id, lane2_right_boundary_2);
    map.AddLaneBoundary(lane_group_2_id, lane3_left_boundary_1);
    map.AddLaneBoundary(lane_group_2_id, lane3_left_boundary_2);
    map.AddLaneBoundary(lane_group_2_id, lane3_right_boundary_1);
    map.AddLaneBoundary(lane_group_2_id, lane3_right_boundary_2);

    const environment::map::LaneGroups lane_groups = map.GetLaneGroups();
    StaticChunkList chunks = chunking_.InitializeChunks(map);

    auto lane_group_count = [](const MapChunk& chunk, mantle_api::UniqueId id) {
        return std::count_if(chunk.lane_groups.begin(), chunk.lane_groups.end(), [&id](const auto* lane_group) {
            return lane_group->id == id;
        });
    };

    auto check_chunk_for_lane_groups = [&chunks, &lane_group_count](const ChunkKey& key,
                                                                    std::vector<mantle_api::UniqueId> ids) {
        const auto& chunk{chunks.at(key)};
        EXPECT_EQ(ids.size(), chunk.lane_groups.size()) << fmt::format("Error in key {}", key);
        std::for_each(ids.begin(), ids.end(), [&chunk, &lane_group_count, &key](const auto id) {
            EXPECT_EQ(1, lane_group_count(chunk, id)) << fmt::format("Error for key {} - id {}", key, id);
        });
    };

    ASSERT_EQ(26, chunks.size());

    EXPECT_EQ(1, chunks.count({2, 1}));
    EXPECT_EQ(1, chunks.count({2, 2}));
    EXPECT_EQ(1, chunks.count({2, 3}));
    EXPECT_EQ(1, chunks.count({2, 4}));
    EXPECT_EQ(1, chunks.count({2, 5}));
    EXPECT_EQ(1, chunks.count({2, 6}));
    EXPECT_EQ(1, chunks.count({3, 2}));
    EXPECT_EQ(1, chunks.count({3, 3}));
    EXPECT_EQ(1, chunks.count({3, 4}));
    EXPECT_EQ(1, chunks.count({3, 5}));
    EXPECT_EQ(1, chunks.count({3, 6}));
    EXPECT_EQ(1, chunks.count({4, 3}));
    EXPECT_EQ(1, chunks.count({4, 4}));
    EXPECT_EQ(1, chunks.count({5, 4}));
    EXPECT_EQ(1, chunks.count({5, 5}));
    EXPECT_EQ(1, chunks.count({5, 6}));
    EXPECT_EQ(1, chunks.count({6, 5}));
    EXPECT_EQ(1, chunks.count({6, 6}));

    EXPECT_EQ(1, chunks.count({8, 3}));
    EXPECT_EQ(1, chunks.count({8, 4}));
    EXPECT_EQ(1, chunks.count({8, 5}));
    EXPECT_EQ(1, chunks.count({8, 6}));
    EXPECT_EQ(1, chunks.count({9, 3}));
    EXPECT_EQ(1, chunks.count({9, 4}));
    EXPECT_EQ(1, chunks.count({9, 5}));
    EXPECT_EQ(1, chunks.count({9, 6}));

    check_chunk_for_lane_groups({2, 1}, {lane_group_1_id});
    check_chunk_for_lane_groups({2, 2}, {lane_group_1_id});
    check_chunk_for_lane_groups({2, 3}, {lane_group_2_id});
    check_chunk_for_lane_groups({2, 4}, {lane_group_2_id});
    check_chunk_for_lane_groups({2, 5}, {lane_group_2_id});
    check_chunk_for_lane_groups({2, 6}, {lane_group_2_id});
    check_chunk_for_lane_groups({3, 2}, {lane_group_1_id});
    check_chunk_for_lane_groups({3, 3}, {lane_group_1_id, lane_group_2_id});
    check_chunk_for_lane_groups({3, 4}, {lane_group_1_id, lane_group_2_id});
    check_chunk_for_lane_groups({3, 5}, {lane_group_2_id});
    check_chunk_for_lane_groups({3, 6}, {lane_group_2_id});
    check_chunk_for_lane_groups({4, 3}, {lane_group_1_id});
    check_chunk_for_lane_groups({4, 4}, {lane_group_1_id});
    check_chunk_for_lane_groups({5, 4}, {lane_group_1_id});
    check_chunk_for_lane_groups({5, 5}, {lane_group_1_id});
    check_chunk_for_lane_groups({5, 6}, {lane_group_1_id});
    check_chunk_for_lane_groups({6, 5}, {lane_group_1_id});
    check_chunk_for_lane_groups({6, 6}, {lane_group_1_id});

    check_chunk_for_lane_groups({8, 3}, {lane_group_2_id});
    check_chunk_for_lane_groups({8, 4}, {lane_group_2_id});
    check_chunk_for_lane_groups({8, 5}, {lane_group_2_id});
    check_chunk_for_lane_groups({8, 6}, {lane_group_2_id});
    check_chunk_for_lane_groups({9, 3}, {lane_group_2_id});
    check_chunk_for_lane_groups({9, 4}, {lane_group_2_id});
    check_chunk_for_lane_groups({9, 5}, {lane_group_2_id});
    check_chunk_for_lane_groups({9, 6}, {lane_group_2_id});
}

TEST_F(
    PolylineChunkingMapTest,
    GivenLogicalLaneBoundariesInMultipleChunksWithJumpingChunksViaBoundariesAndOrdering_WhenInitializeChunks_ThenNumberOfChunksCorrect)
{
    const std::vector<map_api::ReferenceLinePoint> poly_line_1{{{333.0_m, 335.0_m, 0.0_m}, 0_m, 0_rad},
                                                               {{131.0_m, 90.0_m, 0.0_m}, 0_m, 0_rad}};
    map_api::ReferenceLine reference_line_1{map_api::Identifier{1U}, poly_line_1};
    const std::vector<map_api::LogicalBoundaryPoint> left_boundary_line_1{{{333.0_m, 333.0_m, 0.0_m}, 0_m, 0_m},
                                                                          {{131.0_m, 88.0_m, 0.0_m}, 0_m, 0_m}};
    const std::vector<map_api::LogicalBoundaryPoint> right_boundary_line_1{{{333.0_m, 337.0_m, 0.0_m}, 0_m, 0_m},
                                                                           {{131.0_m, 92.0_m, 0.0_m}, 0_m, 0_m}};

    const std::vector<map_api::ReferenceLinePoint> poly_line_2{{{165.0_m, 222.0_m, 0.0_m}, 0_m, 0_rad},
                                                               {{170.0_m, 227.0_m, 0.0_m}, 0_m, 0_rad}};
    map_api::ReferenceLine reference_line_2{map_api::Identifier{2U}, poly_line_2};
    const std::vector<map_api::LogicalBoundaryPoint> left_boundary_line_2{{{165.0_m, 224.0_m, 0.0_m}, 0_m, 0_m},
                                                                          {{170.0_m, 229.0_m, 0.0_m}, 0_m, 0_m}};
    const std::vector<map_api::LogicalBoundaryPoint> right_boundary_line_2{{{165.0_m, 220.0_m, 0.0_m}, 0_m, 0_m},
                                                                           {{170.0_m, 225.0_m, 0.0_m}, 0_m, 0_m}};

    const std::vector<map_api::ReferenceLinePoint> poly_line_3{{{125.0_m, 250.0_m, 0.0_m}, 0_m, 0_rad},
                                                               {{155.0_m, 250.0_m, 0.0_m}, 0_m, 0_rad}};
    map_api::ReferenceLine reference_line_3{map_api::Identifier{3U}, poly_line_3};
    const std::vector<map_api::LogicalBoundaryPoint> left_boundary_line_3{{{125.0_m, 252.0_m, 0.0_m}, 0_m, 0_m},
                                                                          {{155.0_m, 252.0_m, 0.0_m}, 0_m, 0_m}};
    const std::vector<map_api::LogicalBoundaryPoint> right_boundary_line_3{{{125.0_m, 248.0_m, 0.0_m}, 0_m, 0_m},
                                                                           {{155.0_m, 248.0_m, 0.0_m}, 0_m, 0_m}};

    // Chunk {2, 1} / {2, 2} / {3, 2} / {3, 3} / {4, 3} / {4, 4} / {5, 4} / {5, 5} / {6, 5} / {6, 6}
    // also tests the re-ordering of the chunk box that is looped over
    const map_api::LogicalLaneBoundary left_logical_lane_boundary_1{
        2001, left_boundary_line_1, map_api::LogicalLaneBoundary::PassingRule::kUnknown, &reference_line_1, {}, {}};
    // Chunk {2, 1} / {2, 2} / {3, 2} / {3, 3} / {4, 3} / {4, 4} / {5, 4} / {5, 5} / {6, 5} / {6, 6}
    // also tests the re-ordering of the chunk box that is looped over
    const map_api::LogicalLaneBoundary right_logical_lane_boundary_1{
        2002, right_boundary_line_1, map_api::LogicalLaneBoundary::PassingRule::kUnknown, &reference_line_1, {}, {}};
    // Chunk {3, 4}
    const map_api::LogicalLaneBoundary left_logical_lane_boundary_2{
        2003, left_boundary_line_2, map_api::LogicalLaneBoundary::PassingRule::kUnknown, &reference_line_2, {}, {}};
    // Chunk {3, 4}
    const map_api::LogicalLaneBoundary right_logical_lane_boundary_2{
        2004, right_boundary_line_2, map_api::LogicalLaneBoundary::PassingRule::kUnknown, &reference_line_2, {}, {}};
    // Chunk {2, 5} / {3, 5}
    const map_api::LogicalLaneBoundary left_logical_lane_boundary_3{
        2005, left_boundary_line_3, map_api::LogicalLaneBoundary::PassingRule::kUnknown, &reference_line_3, {}, {}};
    // Chunk {2, 4} / {3, 4}
    const map_api::LogicalLaneBoundary right_logical_lane_boundary_3{
        2006, right_boundary_line_3, map_api::LogicalLaneBoundary::PassingRule::kUnknown, &reference_line_3, {}, {}};

    gtgen::core::environment::map::GtGenMap map;

    map.Add(left_logical_lane_boundary_1);
    map.Add(right_logical_lane_boundary_1);
    map.Add(left_logical_lane_boundary_2);
    map.Add(right_logical_lane_boundary_2);
    map.Add(left_logical_lane_boundary_3);
    map.Add(right_logical_lane_boundary_3);

    const std::vector<std::pair<ChunkKey, std::vector<mantle_api::UniqueId>>> expected_chunk_ids = {
        {{2, 1}, {2001, 2002}},
        {{2, 2}, {2001, 2002}},
        {{2, 4}, {2006}},
        {{2, 5}, {2005}},
        {{3, 2}, {2001, 2002}},
        {{3, 3}, {2001, 2002}},
        {{3, 4}, {2003, 2004, 2006}},
        {{3, 5}, {2005}},
        {{4, 3}, {2001, 2002}},
        {{4, 4}, {2001, 2002}},
        {{5, 4}, {2001, 2002}},
        {{5, 5}, {2001, 2002}},
        {{6, 5}, {2001, 2002}},
        {{6, 6}, {2001, 2002}}};

    const StaticChunkList chunks = chunking_.InitializeChunks(map);

    // number of chunks should be equal to number of elements in expected_chunk_ids
    ASSERT_EQ(expected_chunk_ids.size(), chunks.size());
    for (auto& [chunk_key, logical_lane_boundary_ids] : expected_chunk_ids)
    {
        // check existence of chunk
        EXPECT_EQ(1, chunks.count(chunk_key));
        // check number of logical lane boundaries in this chunk
        const auto& current_chunk{chunks.at(chunk_key)};
        EXPECT_EQ(logical_lane_boundary_ids.size(), current_chunk.logical_lane_boundaries.size());
        for (auto logical_lane_boundary_id : logical_lane_boundary_ids)
        {
            // check if every logical lane boundary is in this chunk
            EXPECT_TRUE(ContainElement(current_chunk.logical_lane_boundaries, logical_lane_boundary_id)) << fmt::format(
                "Error for chunk key {} - logical lane boundary id {}", chunk_key, logical_lane_boundary_id);
        }
    }
}

TEST_F(
    PolylineChunkingMapTest,
    GivenLogicalLanesInMultipleChunksWithJumpingChunksViaBoundariesAndOrdering_WhenInitializeChunks_ThenNumberOfChunksIsCorrect)
{
    map_api::ReferenceLine reference_line{
        map_api::Identifier{1U},
        std::vector<map_api::ReferenceLinePoint>{{{0_m, 100_m, 0_m}, 0_m, 0_rad},
                                                 {{250.0_m, 400.0_m, 0_m}, 390_m, 0_rad}}};

    map_api::LogicalLaneBoundary logical_lane_boundary_1{
        map_api::Identifier{11U},
        std::vector<map_api::LogicalBoundaryPoint>{{{99_m, 67_m, 0_m}, 38_m, -101_m},
                                                   {{306_m, 315_m, 0_m}, 361_m, -98_m}},
        map_api::LogicalLaneBoundary::PassingRule::kUnknown,
        &reference_line,
        {},
        {}};

    map_api::LogicalLaneBoundary logical_lane_boundary_2{
        map_api::Identifier{12U},
        std::vector<map_api::LogicalBoundaryPoint>{{{102_m, 65_m, 0_m}, 38_m, -101_m},
                                                   {{309_m, 313_m, 0_m}, 361_m, -101_m}},
        map_api::LogicalLaneBoundary::PassingRule::kUnknown,
        &reference_line,
        {},
        {}};

    map_api::LogicalLaneBoundary logical_lane_boundary_3{
        map_api::Identifier{13U},
        std::vector<map_api::LogicalBoundaryPoint>{{{105_m, 62_m, 0_m}, 38_m, -104_m},
                                                   {{312_m, 310_m, 0.0_m}, 361_m, -104_m}},
        map_api::LogicalLaneBoundary::PassingRule::kUnknown,
        &reference_line,
        {},
        {}};

    // Chunk {1, 1} / {2, 1} / {2, 2} / {3, 2} / {3, 3} / {4, 3} / {4, 4} / {5, 4} / {5, 5} / {5, 6} / {6, 6}
    // also tests the re-ordering of the chunk box that is looped over
    map_api::LogicalLane logical_lane_1{map_api::Identifier{101U},
                                        38_m,
                                        361_m,
                                        &reference_line,
                                        map_api::LogicalLane::Type::kUnknown,
                                        {},
                                        {},
                                        map_api::LogicalLane::MoveDirection::kDecreasingS,
                                        {},
                                        {},
                                        {},
                                        {logical_lane_boundary_1},
                                        {logical_lane_boundary_2},
                                        {},
                                        {},
                                        {}};

    // Chunk {2, 1} / {2, 2} / {3, 2} / {3, 3} / {4, 3} / {4, 4} / {5, 4} / {5, 5} / {5, 6} / {6, 5} / {6, 6}
    // also tests the re-ordering of the chunk box that is looped over
    map_api::LogicalLane logical_lane_2{map_api::Identifier{102U},
                                        38_m,
                                        361_m,
                                        &reference_line,
                                        map_api::LogicalLane::Type::kUnknown,
                                        {},
                                        {},
                                        map_api::LogicalLane::MoveDirection::kIncreasingS,
                                        {},
                                        {},
                                        {},
                                        {logical_lane_boundary_2},
                                        {logical_lane_boundary_3},
                                        {},
                                        {},
                                        {}};

    logical_lane_1.left_adjacent_lanes.push_back(map_api::LogicalLaneRelation{
        logical_lane_2, logical_lane_1.start_s, logical_lane_1.end_s, logical_lane_2.start_s, logical_lane_2.end_s});
    logical_lane_2.left_adjacent_lanes.push_back(map_api::LogicalLaneRelation{
        logical_lane_1, logical_lane_2.start_s, logical_lane_2.end_s, logical_lane_1.start_s, logical_lane_1.end_s});

    GtGenMap map;

    map.Add(logical_lane_1);
    map.Add(logical_lane_2);

    const std::vector<std::pair<ChunkKey, std::vector<mantle_api::UniqueId>>> expected_chunk_ids = {
        {{1, 1}, {logical_lane_1.id}},
        {{2, 1}, {logical_lane_1.id, logical_lane_2.id}},
        {{2, 2}, {logical_lane_1.id, logical_lane_2.id}},
        {{3, 2}, {logical_lane_1.id, logical_lane_2.id}},
        {{3, 3}, {logical_lane_1.id, logical_lane_2.id}},
        {{4, 3}, {logical_lane_1.id, logical_lane_2.id}},
        {{4, 4}, {logical_lane_1.id, logical_lane_2.id}},
        {{5, 4}, {logical_lane_1.id, logical_lane_2.id}},
        {{5, 5}, {logical_lane_1.id, logical_lane_2.id}},
        {{5, 6}, {logical_lane_1.id, logical_lane_2.id}},
        {{6, 5}, {logical_lane_2.id}},
        {{6, 6}, {logical_lane_1.id, logical_lane_2.id}}};

    const StaticChunkList chunks = chunking_.InitializeChunks(map);

    // number of chunks should be equal to number of elements in expected_chunk_ids
    ASSERT_EQ(expected_chunk_ids.size(), chunks.size());
    for (auto& [chunk_key, logical_lane_ids] : expected_chunk_ids)
    {
        // check existence of chunk
        ASSERT_EQ(1, chunks.count(chunk_key));
        // check number of logical lane boundaries in this chunk
        const auto& current_chunk{chunks.at(chunk_key)};
        EXPECT_EQ(logical_lane_ids.size(), current_chunk.logical_lanes.size());
        for (auto logical_lane_id : logical_lane_ids)
        {
            // check if every logical lane is in this chunk
            EXPECT_TRUE(ContainElement(current_chunk.logical_lanes, logical_lane_id))
                << fmt::format("Error for chunk key {} - logical lane id {}", chunk_key, logical_lane_id);
        }
    }
}

TEST_F(PolylineChunkingMapTest, GivenPolylineChunk_WhenSetChunkSizeToZero_ThenExpectException)
{
    EXPECT_TRUE(ExpectExceptionContains<EnvironmentException>(
        [&]() { chunking_.SetChunkSize(0); },
        "Chunk size must be greater than zero, "
        "please check the ChunkGridSize value of your User Settings and ensure that this contains a valid value."));
}

}  // namespace gtgen::core::environment::chunking
