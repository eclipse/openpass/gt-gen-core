/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_HOST_HOSTVEHICLEMODEL_H
#define GTGEN_CORE_ENVIRONMENT_HOST_HOSTVEHICLEMODEL_H

#include "Core/Environment/Host/vehicle_model_in.h"
#include "Core/Environment/Host/vehicle_model_out.h"

#include <MantleAPI/Common/vector.h>

namespace gtgen::core::environment::host
{

/// @brief This struct represents the vehicle model of the host vehicle in GTGen
struct HostVehicleModel
{
    VehicleModelIn vehicle_model_in{};
    VehicleModelOut vehicle_model_out{};
};

}  // namespace gtgen::core::environment::host

#endif  // GTGEN_CORE_ENVIRONMENT_HOST_HOSTVEHICLEMODEL_H
