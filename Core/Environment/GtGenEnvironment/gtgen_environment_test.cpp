/*******************************************************************************
 * Copyright (c) 2021-2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GtGenEnvironment/gtgen_environment.h"

#include "Core/Environment/DataStore/test_utils.h"
#include "Core/Environment/DataStore/utils.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Environment/GtGenEnvironment/Internal/i_map_engine.h"
#include "Core/Service/FileSystem/filesystem.h"
#include "Core/Service/Utility/clock.h"
#include "Core/Service/Version/version.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"
#include "Core/Tests/TestUtils/MockMapEngine/mock_map_engine.h"
#include "Core/Tests/TestUtils/convert_first_custom_traffic_command_of_host_vehicle_to_route.h"

#include <MantleAPI/Traffic/entity_properties.h>
#include <MantleAPI/Traffic/i_controller_config.h>
#include <fmt/printf.h>
#include <gtest/gtest.h>
#include <nlohmann/json.hpp>

namespace gtgen::core::environment::api
{
using units::literals::operator""_ms;
using units::literals::operator""_m;
using units::literals::operator""_kg;
using units::literals::operator""_mps;

std::unique_ptr<GtGenEnvironment> CreateEnvironmentWithDummyMap(
    const fs::path& map_file_path = "external/Dummy.xodr",
    const fs::path& map_model_reference = "",
    const service::user_settings::UserSettings& user_settings = service::user_settings::UserSettings{},
    const std::uint32_t seed = 0,
    const std::int32_t run_index = 0,
    const std::int32_t total_runs = 1)
{
    auto map_engine = std::make_unique<test_utils::MockMapEngine>();
    map_engine->SetMap(test_utils::MapCatalogue::MapStraightRoad2km());
    auto env = std::make_unique<GtGenEnvironment>(
        user_settings,
        40_ms,
        datastore::OutputGenerator::Settings{
            user_settings.simulation_results.output_directory_path, run_index, total_runs},
        seed);
    env->SetMapEngine(std::move(map_engine));
    env->Init();
    env->CreateMap(map_file_path, {}, map_model_reference);
    return env;
}

class EnvironmentApiControllerTest : public ::testing::Test
{
  public:
    EnvironmentApiControllerTest()
    {
        user_settings.simulation_results.output_directory_path = output_path;
        user_settings.simulation_results.log_cyclics = true;
        user_settings.collision_detection.enabled = true;
    }

  protected:
    void TearDown() override { fs::remove_all(output_path); }

    const fs::path output_path{fs::temp_directory_path() / "GtGenTestPath"};
    static constexpr std::uint32_t kArbitrarySeed{123U};
    service::user_settings::UserSettings user_settings;
};

TEST_F(EnvironmentApiControllerTest, GivenEnvironment_WhenCreateController_ThenCreationSucceeds)
{
    auto config = std::make_unique<mantle_api::NoOpControllerConfig>();

    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};
    EXPECT_NO_THROW(env.GetControllerRepository().Create(0, std::move(config)));
}

TEST_F(EnvironmentApiControllerTest,
       GivenEnvironmentControllerAndEntity_WhenAddingEntityToController_ThenAddingSucceeds)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};
    auto& entity_repository = env.GetEntityRepository();

    auto config = std::make_unique<mantle_api::NoOpControllerConfig>();
    ASSERT_NO_THROW(env.GetControllerRepository().Create(0, std::move(config)));
    ASSERT_NO_THROW(entity_repository.Create(0, "vehicle", mantle_api::VehicleProperties{}));

    EXPECT_NO_THROW(env.AddEntityToController(entity_repository.Get(0)->get(), 0));
}

TEST_F(EnvironmentApiControllerTest, GivenEnvironmentWithoutController_WhenAddingEntityToController_ThenExceptionThrown)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};
    auto& entity_repository = env.GetEntityRepository();

    EXPECT_THROW(env.AddEntityToController(entity_repository.Get(0)->get(), 0), EnvironmentException);
}

TEST_F(EnvironmentApiControllerTest,
       GivenEnvironmentWithCompositeControllerWithEntity_WhenUpdatingControlStrategies_ThenUpdateSucceeds)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    std::uint64_t controller_id = 0;
    auto config = std::make_unique<mantle_api::NoOpControllerConfig>();
    auto& controller = env.GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    mantle_api::UniqueId entity_id = 10;
    auto& entity_repository = env.GetEntityRepository();
    ASSERT_NO_THROW(env.GetEntityRepository().Create(entity_id, "vehicle", mantle_api::VehicleProperties{}));
    EXPECT_NO_THROW(env.AddEntityToController(entity_repository.Get(entity_id)->get(), controller_id));

    std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies;
    control_strategies.emplace_back(std::make_unique<mantle_api::KeepVelocityControlStrategy>());
    EXPECT_NO_THROW(env.UpdateControlStrategies(entity_id, control_strategies));
}

TEST_F(
    EnvironmentApiControllerTest,
    GivenEnvironmentWithExternalCompositeControllerWithEntity_WhenUpdatingControlStrategies_ThenControlUnitsUnchanged)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};
    env.Init();

    std::uint64_t controller_id = 0;
    auto config = std::make_unique<mantle_api::ExternalControllerConfig>();
    auto& controller = env.GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    mantle_api::UniqueId entity_id = 10;
    auto& entity_repository = env.GetEntityRepository();
    ASSERT_NO_THROW(env.GetEntityRepository().Create(entity_id, "vehicle", mantle_api::VehicleProperties{}));
    EXPECT_NO_THROW(env.AddEntityToController(entity_repository.Get(entity_id)->get(), controller_id));

    std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies;
    control_strategies.emplace_back(std::make_shared<mantle_api::KeepVelocityControlStrategy>());
    EXPECT_NO_THROW(env.UpdateControlStrategies(entity_id, control_strategies));

    auto controllers = env.GetActiveControllerRepository()->GetControllersByEntityId(entity_id);
    ASSERT_EQ(controllers.size(), 1);
    auto& control_units = controllers.front()->GetControlUnits();
    ASSERT_EQ(2, control_units.size());
    EXPECT_EQ(control_units[0]->GetName(), "HostVehicleInterfaceControlUnit");
    EXPECT_EQ(control_units[1]->GetName(), "VehicleModelControlUnit");
}

TEST_F(
    EnvironmentApiControllerTest,
    GivenEnvironmentWithExternalCompositeControllerWithEntity_WhenUpdatingControlStrategiesAndStep_ThenTrafficCommandAvailable)
{
    auto map_engine = std::make_unique<test_utils::MockMapEngine>();
    auto map = test_utils::MapCatalogue::MapStraightRoad2km();
    test_utils::MockCoordinateConverter* coordinate_converter =
        dynamic_cast<test_utils::MockCoordinateConverter*>(map->coordinate_converter.get());
    EXPECT_CALL(*coordinate_converter, Convert(testing::_)).Times(1);
    map_engine->SetMap(std::move(map));

    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    env.SetMapEngine(std::move(map_engine));
    env.Init();

    const fs::path maps_path{"external"};
    env.CreateMap(maps_path / "Dummy.xodr", {});

    auto config = std::make_unique<mantle_api::ExternalControllerConfig>();
    auto& controller = env.GetControllerRepository().Create(std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    auto& entity_repository = env.GetEntityRepository();
    mantle_api::VehicleProperties vehicle_properties;
    vehicle_properties.is_host = true;
    auto& entity = entity_repository.Create("Host", vehicle_properties);
    EXPECT_NO_THROW(env.AddEntityToController(entity_repository.Get("Host")->get(), controller.GetUniqueId()));

    std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies;
    control_strategies.emplace_back(std::make_shared<mantle_api::FollowVelocitySplineControlStrategy>());
    EXPECT_NO_THROW(env.UpdateControlStrategies(entity.GetUniqueId(), control_strategies));
    env.Step(0_ms);
    EXPECT_EQ(1, env.GetTrafficCommands().size());
}

TEST_F(EnvironmentApiControllerTest, GivenEnvironmentWithoutActiveController_WhenUpdatingControlStrategies_ThenThrow)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies;
    control_strategies.emplace_back(std::make_unique<mantle_api::KeepVelocityControlStrategy>());
    EXPECT_THROW(env.UpdateControlStrategies(0, control_strategies), EnvironmentException);
}

TEST_F(EnvironmentApiControllerTest,
       GivenEnvironmentWithInternalControllerWithoutControlUnit_WhenAskingGoalReached_ThenThrow)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    std::uint64_t controller_id = 1;
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    auto& controller = env.GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    std::uint64_t entity_id = 0;
    auto& entity = env.GetEntityRepository().Create(entity_id, "vehicle", mantle_api::VehicleProperties{});
    env.AddEntityToController(entity, controller_id);

    EXPECT_THROW(env.HasControlStrategyGoalBeenReached(entity_id, mantle_api::ControlStrategyType::kKeepVelocity),
                 EnvironmentException);
}

TEST_F(EnvironmentApiControllerTest,
       GivenEnvironmentWithInternalCompositeControllerWithEntity_WhenAssignRoute_ThenControllerContainsPathControlUnit)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    std::uint64_t controller_id = 0;
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    auto& controller = env.GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    mantle_api::UniqueId entity_id = 10;
    auto& entity_repository = env.GetEntityRepository();
    env.GetEntityRepository().Create(entity_id, "vehicle", mantle_api::VehicleProperties{});
    env.AddEntityToController(entity_repository.Get(entity_id)->get(), controller_id);
    auto composite_controllers = env.GetActiveControllerRepository()->GetControllersByEntityId(entity_id);
    ASSERT_EQ(composite_controllers.size(), 1);
    EXPECT_EQ(composite_controllers.front()->GetControlUnits().size(), 0);

    mantle_api::RouteDefinition route_definition;
    route_definition.waypoints.push_back({{0_m, 0_m, 0_m}, mantle_api::RouteStrategy::kShortest});
    route_definition.waypoints.push_back({{10_m, 0_m, 0_m}, mantle_api::RouteStrategy::kShortest});
    EXPECT_NO_THROW(env.AssignRoute(entity_id, route_definition));

    auto& control_units = composite_controllers.front()->GetControlUnits();
    ASSERT_EQ(1, control_units.size());

    bool contains_path_control_unit = false;
    for (auto& controller : control_units)
    {
        if (controller->GetName() == "PathControlUnit")
        {
            contains_path_control_unit = true;
            break;
        }
    }
    EXPECT_TRUE(contains_path_control_unit);
}

TEST_F(EnvironmentApiControllerTest,
       GivenEnvironmentWithHostVehicleInterfaceControlUnit_WhenAssignRoute_ThenHostVehicleTrafficCommandsContainRoute)
{
    auto env = CreateEnvironmentWithDummyMap();

    mantle_api::UniqueId entity_id = 0;
    mantle_api::VehicleProperties vehicle_properties;
    vehicle_properties.is_host = true;
    auto& entity = env->GetEntityRepository().Create(entity_id, "vehicle", vehicle_properties);
    auto& controller = env->GetControllerRepository().Create(std::make_unique<mantle_api::ExternalControllerConfig>());
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);
    env->AddEntityToController(entity, controller.GetUniqueId());
    mantle_api::RouteDefinition route_definition;
    route_definition.waypoints.push_back({{0_m, 0_m, 0_m}, mantle_api::RouteStrategy::kShortest});
    route_definition.waypoints.push_back({{0_m, 100_m, 0_m}, mantle_api::RouteStrategy::kShortest});

    EXPECT_NO_THROW(env->AssignRoute(entity_id, route_definition));
    env->Step(0_ms);
    const auto actual_path =
        gtgen::core::test_utils::ConvertFirstCustomTrafficCommandOfHostVehicleToRoute(env->GetTrafficCommands());
    EXPECT_FALSE(actual_path.empty());
}

TEST_F(EnvironmentApiControllerTest, GivenUserSettingsThatDoNotRequireToLogCyclics_WhenExecuting_ThenDoNotLogTheCyclics)
{
    // Given
    const std::string cyclics_filename = "Cyclics_Run_0.csv";
    const fs::path output_file_path = output_path / cyclics_filename;
    service::user_settings::UserSettings user_settings;
    user_settings.simulation_results.output_directory_path = output_path;
    user_settings.simulation_results.log_cyclics = false;
    auto env = CreateEnvironmentWithDummyMap("external/Dummy.xodr", "", user_settings);
    mantle_api::UniqueId entity_id = 0;
    mantle_api::VehicleProperties vehicle_properties;
    vehicle_properties.is_host = true;
    auto& entity = env->GetEntityRepository().Create(entity_id, "vehicle", vehicle_properties);
    auto& controller = env->GetControllerRepository().Create(std::make_unique<mantle_api::ExternalControllerConfig>());
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);
    env->AddEntityToController(entity, controller.GetUniqueId());

    // When
    env->Step(0_ms);
    env.reset();

    // Then
    ASSERT_FALSE(fs::exists(output_file_path)) << "File exists even when not required: " << (output_file_path).string();
}

TEST_F(EnvironmentApiControllerTest, GivenUserSettingsThatDoRequireToLogCyclics_WhenExecuting_ThenLogTheCyclics)
{
    // Given
    const std::string cyclics_filename = "Cyclics_Run_0.csv";
    const fs::path expected_output_file_path = output_path / cyclics_filename;
    const std::vector<std::string> expected_csv{
        "Timestep, 00:AccelerationEgo, 00:IndicatorState, 00:Lane, 00:PitchAngle, 00:PositionRoute, 00:Road, "
        "00:RollAngle, "
        "00:TCoordinate, 00:VelocityEgo, 00:XPosition, 00:YPosition, 00:YawAngle, 00:ZPosition",
        "40, 0.000000, 2, 6, 0.000000, nan, nan, 0.000000, nan, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000"};
    service::user_settings::UserSettings user_settings;
    user_settings.simulation_results.output_directory_path = output_path;
    user_settings.simulation_results.log_cyclics = true;
    auto env = CreateEnvironmentWithDummyMap("external/Dummy.xodr", "", user_settings);
    mantle_api::UniqueId entity_id = 0;
    mantle_api::VehicleProperties vehicle_properties;
    vehicle_properties.is_host = true;
    auto& entity = env->GetEntityRepository().Create(entity_id, "vehicle", vehicle_properties);
    auto& controller = env->GetControllerRepository().Create(std::make_unique<mantle_api::ExternalControllerConfig>());
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);
    env->AddEntityToController(entity, controller.GetUniqueId());

    // When
    env->Step(0_ms);
    env.reset();

    // Then
    ASSERT_TRUE(fs::exists(expected_output_file_path))
        << "File does not exist: " << (expected_output_file_path).string();
    EXPECT_EQ(expected_csv, gtgen::core::environment::datastore::test_utils::readCSV(expected_output_file_path));
}

TEST_F(EnvironmentApiControllerTest,
       GivenEnvironmentWithControllerForKeepVelocityControlStrategy_WhenAskingGoalReached_ThenReturnFalse)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    std::uint64_t controller_id = 1;
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    config->control_strategies.emplace_back(std::make_unique<mantle_api::KeepVelocityControlStrategy>());
    auto& controller = env.GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    std::uint64_t entity_id = 0;
    auto& entity = env.GetEntityRepository().Create(entity_id, "vehicle", mantle_api::VehicleProperties{});
    env.AddEntityToController(entity, controller_id);

    EXPECT_FALSE(env.HasControlStrategyGoalBeenReached(entity_id, mantle_api::ControlStrategyType::kKeepVelocity));
}

TEST_F(EnvironmentApiControllerTest,
       GivenEnvironmentWithControllerForKeepLaneOffsetControlStrategy_WhenAskingGoalReached_ThenReturnFalse)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    std::uint64_t controller_id = 1;
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    config->control_strategies.emplace_back(std::make_unique<mantle_api::KeepLaneOffsetControlStrategy>());
    auto& controller = env.GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    std::uint64_t entity_id = 0;
    auto& entity = env.GetEntityRepository().Create(entity_id, "vehicle", mantle_api::VehicleProperties{});
    env.AddEntityToController(entity, controller_id);

    EXPECT_FALSE(env.HasControlStrategyGoalBeenReached(entity_id, mantle_api::ControlStrategyType::kKeepLaneOffset));
}

TEST_F(EnvironmentApiControllerTest,
       GivenEnvironmentWithControllerForFollowEmptyHeadingSplineControlStrategy_WhenAskingGoalReached_ThenReturnTrue)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    std::uint64_t controller_id = 1;
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    config->control_strategies.emplace_back(std::make_unique<mantle_api::FollowHeadingSplineControlStrategy>());
    auto& controller = env.GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    std::uint64_t entity_id = 0;
    auto& entity = env.GetEntityRepository().Create(entity_id, "vehicle", mantle_api::VehicleProperties{});
    env.AddEntityToController(entity, controller_id);

    EXPECT_TRUE(
        env.HasControlStrategyGoalBeenReached(entity_id, mantle_api::ControlStrategyType::kFollowHeadingSpline));
}

TEST_F(
    EnvironmentApiControllerTest,
    GivenEnvironmentWithControllerForFollowEmptyLateralOffsetSplineControlStrategy_WhenAskingGoalReached_ThenReturnTrue)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    std::uint64_t controller_id = 1;
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    config->control_strategies.emplace_back(std::make_unique<mantle_api::FollowLateralOffsetSplineControlStrategy>());
    auto& controller = env.GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    std::uint64_t entity_id = 0;
    auto& entity = env.GetEntityRepository().Create(entity_id, "vehicle", mantle_api::VehicleProperties{});
    env.AddEntityToController(entity, controller_id);

    EXPECT_TRUE(
        env.HasControlStrategyGoalBeenReached(entity_id, mantle_api::ControlStrategyType::kFollowLateralOffsetSpline));
}

TEST_F(EnvironmentApiControllerTest,
       GivenEnvironmentWithControllerForFollowEmptyVelocitySplineControlStrategy_WhenAskingGoalReached_ThenReturnTrue)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    std::uint64_t controller_id = 1;
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    config->control_strategies.emplace_back(std::make_unique<mantle_api::FollowVelocitySplineControlStrategy>());
    auto& controller = env.GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    std::uint64_t entity_id = 0;
    auto& entity = env.GetEntityRepository().Create(entity_id, "vehicle", mantle_api::VehicleProperties{});
    env.AddEntityToController(entity, controller_id);

    EXPECT_TRUE(
        env.HasControlStrategyGoalBeenReached(entity_id, mantle_api::ControlStrategyType::kFollowVelocitySpline));
}

TEST_F(EnvironmentApiControllerTest,
       GivenEnvironmentWithControllerForAcquireLaneOffsetControlStrategy_WhenAskingGoalReached_ThenThrow)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    std::uint64_t controller_id = 1;
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    config->control_strategies.emplace_back(std::make_unique<mantle_api::AcquireLaneOffsetControlStrategy>());
    auto& controller = env.GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    std::uint64_t entity_id = 0;
    auto& entity = env.GetEntityRepository().Create(entity_id, "vehicle", mantle_api::VehicleProperties{});
    env.AddEntityToController(entity, controller_id);

    EXPECT_THROW(env.HasControlStrategyGoalBeenReached(entity_id, mantle_api::ControlStrategyType::kAcquireLaneOffset),
                 EnvironmentException);
}

TEST_F(EnvironmentApiControllerTest, GivenEnvironment_WhenSettingTimeInEnvironment_ThenEnvironmentReturnsCorrectTime)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    service::utility::Clock::Instance().SetNow(mantle_api::Time{0});

    EXPECT_EQ(0_ms, env.GetSimulationTime());

    service::utility::Clock::Instance().SetNow(mantle_api::Time{100});
    EXPECT_EQ(100_ms, env.GetSimulationTime());

    service::utility::Clock::Instance().SetNow(mantle_api::Time{150});
    EXPECT_EQ(150_ms, env.GetSimulationTime());
}

TEST_F(EnvironmentApiControllerTest, GivenEnvironment_WhenCreateTpmControlUnit_ThenCreationSucceeds)
{
    auto config = std::make_unique<mantle_api::ExternalControllerConfig>();
    const fs::path maps_path{"external"};

    config->name = "traffic_participant_model_example";

    auto env = CreateEnvironmentWithDummyMap();
    EXPECT_NO_THROW(env->GetControllerRepository().Create(42, std::move(config)));
}

TEST_F(EnvironmentApiControllerTest, GivenEnvironmentWithTPM_WhenStepped_ThenSensorViewContainsTPMRelatedData)
{
    std::unique_ptr<GtGenEnvironment> env = CreateEnvironmentWithDummyMap();
    const fs::path maps_path{"external"};

    std::string entity_name = "host";
    mantle_api::VehicleProperties properties;
    properties.is_host = true;
    auto& entity = env->GetEntityRepository().Create(0, entity_name, properties);

    auto config = std::make_unique<mantle_api::ExternalControllerConfig>();
    config->name = "traffic_participant_model_example";
    auto& controller = env->GetControllerRepository().Create(42, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);
    env->AddEntityToController(entity, controller.GetUniqueId());

    // tick to initialize
    service::utility::Clock::Instance().SetNow(mantle_api::Time{0_ms});
    env->Step(40_ms);
    const double initial_steering_wheel_angle{
        env->GetSensorView().global_ground_truth().moving_object(0).vehicle_attributes().steering_wheel_angle()};

    // tick to step traffic_participant_model to receive TrafficUpdate
    service::utility::Clock::Instance().SetNow(mantle_api::Time{40_ms});
    env->Step(40_ms);
    const double updated_steering_wheel_angle{
        env->GetSensorView().global_ground_truth().moving_object(0).vehicle_attributes().steering_wheel_angle()};

    ASSERT_NE(initial_steering_wheel_angle, updated_steering_wheel_angle);
}

TEST_F(EnvironmentApiControllerTest, GivenEnvironment_WhenCreateExternalHostConfig_ThenCreationSucceeds)
{
    auto config = std::make_unique<mantle_api::ExternalControllerConfig>();
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    env.Init();

    EXPECT_NO_THROW(env.GetControllerRepository().Create(42, std::move(config)));
}

TEST(EnvironmentApiCustomCommandTest, GivenEnvironment_WhenCallExecuteCustomCommandWithoutActors_ThenNoThrow)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};
    EXPECT_NO_THROW(env.ExecuteCustomCommand({}, "MyType", "MyCommand"));
}

TEST(EnvironmentApiCustomCommandTest, GivenEnvironmentWithoutEntity_WhenCallExecuteCustomCommandWithActor_ThenThrow)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};
    EXPECT_ANY_THROW(env.ExecuteCustomCommand({"actor1"}, "MyType", "MyCommand"));
}

TEST(EnvironmentApiCustomCommandTest,
     GivenEnvironmentWithEntityWithoutController_WhenCallExecuteCustomCommandWithActor_ThenNoThrow)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};
    std::string entity_name = "host";
    env.GetEntityRepository().Create(0, entity_name, mantle_api::VehicleProperties{});
    EXPECT_NO_THROW(env.ExecuteCustomCommand({entity_name}, "MyType", "MyCommand"));
}

TEST(
    EnvironmentApiCustomCommandTest,
    GivenEnvironmentWithEntityAndActiveExternalController_WhenCallExecuteCustomCommandWithActor_ThenCreateTrafficCommand)
{
    auto env = CreateEnvironmentWithDummyMap();
    std::string entity_name = "host";
    mantle_api::VehicleProperties properties;
    properties.is_host = true;
    auto& entity = env->GetEntityRepository().Create(0, entity_name, properties);
    auto config = std::make_unique<mantle_api::ExternalControllerConfig>();
    auto controller_id = 42;
    auto& controller = env->GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);
    env->AddEntityToController(entity, controller_id);

    env->ExecuteCustomCommand({entity_name}, "MyType", "MyCommand");
    env->Step(0_ms);
    EXPECT_EQ(env->GetTrafficCommands().size(), 1);
}

TEST(
    EnvironmentApiCustomCommandTest,
    GivenEnvironmentWithEntityAndActiveInternalController_WhenCallExecuteCustomCommandWithActor_ThenCreateNoTrafficCommand)
{
    auto env = CreateEnvironmentWithDummyMap();
    std::string entity_name = "host";
    mantle_api::VehicleProperties properties;
    properties.is_host = true;
    auto& entity = env->GetEntityRepository().Create(0, entity_name, properties);
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    auto controller_id = 42;
    auto& controller = env->GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);
    env->AddEntityToController(entity, controller_id);

    env->ExecuteCustomCommand({entity_name}, "MyType", "MyCommand");
    env->Step(0_ms);
    EXPECT_EQ(env->GetTrafficCommands().size(), 0);
}

TEST(EnvironmentApiUserDefinedValueTest, GivenEnvironment_WhenUserDefinedValueNotSet_ThenGetReturnsNoValue)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};
    EXPECT_FALSE(env.GetUserDefinedValue("MyName").has_value());
}

TEST(EnvironmentApiUserDefinedValueTest, GivenEnvironment_WhenUserDefinedValueSet_ThenGetReturnsValue)
{
    GtGenEnvironment env{service::user_settings::UserSettings{}, 40_ms};
    env.SetUserDefinedValue("MyName", "MyValue");
    ASSERT_TRUE(env.GetUserDefinedValue("MyName").has_value());
    EXPECT_EQ("MyValue", env.GetUserDefinedValue("MyName").value());
}

TEST_F(EnvironmentApiControllerTest, GivenEnvironment_WhenMapWithModelReference_ThenModelReferenceInGtGenMapFilled)
{
    const std::string expected_model_reference = "DummyMapModelReference";
    auto env = CreateEnvironmentWithDummyMap("DummyMapPath.xodr", expected_model_reference);

    EXPECT_EQ(env->GetGtGenMap().GetModelReference(), expected_model_reference);
}

TEST_F(EnvironmentApiControllerTest, GivenEnvironment_WhenInit_ThenCoreInformationIsCreated)
{
    // Given
    const fs::path core_information_path = output_path / "CoreInformation.json";
    const std::string kCoreInformationSchemaVersion{"0.0.3"};
    const std::uint32_t kSimulationRunIndex = 0U;
    service::user_settings::UserSettings user_settings;
    user_settings.simulation_results.output_directory_path = output_path;
    auto env = CreateEnvironmentWithDummyMap("external/Dummy.xodr", "", user_settings, kArbitrarySeed);
    const mantle_api::UniqueId entity_one_id = 123U;
    const std::string entity_one_name{"host_vehicle"};
    const mantle_api::UniqueId entity_two_id = 456U;
    const std::string entity_two_name{"vehicle"};
    const mantle_api::UniqueId entity_three_id = 789U;
    const std::string entity_three_name{"not_vehicle"};
    std::ignore = env->GetEntityRepository().Create(entity_one_id, entity_one_name, mantle_api::VehicleProperties{});
    std::ignore = env->GetEntityRepository().Create(entity_two_id, entity_two_name, mantle_api::VehicleProperties{});
    std::ignore =
        env->GetEntityRepository().Create(entity_three_id, entity_three_name, mantle_api::PedestrianProperties{});
    const std::string expected_string{fmt::sprintf(
        R"({
    "simulatorVersion": "%s",
    "schemaVersion": "%s",
    "simulationRuns": [
        {
            "id": %d,
            "randomSeed": %d,
            "entities": [
                {
                    "id": %d,
                    "name": "%s"
                },
                {
                    "id": %d,
                    "name": "%s"
                },
                {
                    "id": %d,
                    "name": "%s"
                }
            ]
        }
    ]
}
)",
        gtgen::core::GetGtGenCoreVersion(),
        kCoreInformationSchemaVersion,
        kSimulationRunIndex,
        kArbitrarySeed,
        entity_one_id,
        entity_one_name,
        entity_two_id,
        entity_two_name,
        entity_three_id,
        entity_three_name)};
    const nlohmann::ordered_json expected_core_information = nlohmann::ordered_json::parse(expected_string);

    // When
    env->Init();
    env.reset();

    // Then
    ASSERT_TRUE(fs::exists(core_information_path)) << "File does not exist: " << (core_information_path).string();
    std::ifstream core_information_stream(core_information_path);
    const nlohmann::ordered_json actual_core_information = nlohmann::ordered_json::parse(core_information_stream);
    EXPECT_EQ(expected_core_information, actual_core_information);  // check for valid json
    core_information_stream.clear();
    core_information_stream.seekg(0, std::ios::beg);
    std::stringstream actual_string_buffer;
    actual_string_buffer << core_information_stream.rdbuf();
    const std::string actual_string{actual_string_buffer.str()};
    EXPECT_EQ(expected_string, actual_string);  // check for expected format
}

TEST_F(EnvironmentApiControllerTest, GivenRequireLogCyclics_WhenExecuting_ThenLogTheCyclicsWithSameRunIndex)
{
    // Given
    service::user_settings::UserSettings user_settings;
    user_settings.simulation_results.output_directory_path = output_path;
    user_settings.simulation_results.log_cyclics = true;
    constexpr std::int32_t total_runs{1099};

    for (std::int32_t run_index = 0; run_index < total_runs; ++run_index)
    {
        auto env = CreateEnvironmentWithDummyMap(
            "external/Dummy.xodr", "", user_settings, kArbitrarySeed, run_index, total_runs);
        mantle_api::UniqueId entity_id = 13;
        mantle_api::VehicleProperties vehicle_properties;
        vehicle_properties.is_host = true;
        auto& entity = env->GetEntityRepository().Create(entity_id, "vehicle", vehicle_properties);
        auto& controller =
            env->GetControllerRepository().Create(std::make_unique<mantle_api::ExternalControllerConfig>());
        controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                               mantle_api::IController::LongitudinalState::kActivate);
        env->AddEntityToController(entity, controller.GetUniqueId());

        // When
        env->Step(0_ms);
        env.reset();

        // Then
        const std::string cyclics_filename = fmt::format("Cyclics_Run_{:04}.csv", run_index);
        const fs::path expected_output_file_path = output_path / cyclics_filename;
        EXPECT_TRUE(fs::exists(expected_output_file_path)) << "File does not exist: " << cyclics_filename;
    }
}

TEST_F(EnvironmentApiControllerTest, GivenRunIndex_WhenInit_ThenExternalControllerConfigContainsRunIndex)
{
    struct TestableGtGenEnvironment : public GtGenEnvironment
    {
        using GtGenEnvironment::GetExternalControllerConfigConverterData;
        using GtGenEnvironment::GtGenEnvironment;
    };

    service::user_settings::UserSettings user_settings;
    user_settings.simulation_results.output_directory_path = output_path;
    user_settings.simulation_results.log_cyclics = true;

    constexpr std::int32_t total_runs{99};
    for (std::int32_t run_index = 0; run_index < total_runs; ++run_index)
    {
        TestableGtGenEnvironment env{user_settings, 40_ms, kArbitrarySeed, run_index, total_runs};
        env.Init();

        for (std::int32_t entity_id = 0; entity_id < 42; ++entity_id)
        {
            EXPECT_EQ(env.GetExternalControllerConfigConverterData().output_settings->GetEntityOutputFolder(entity_id),
                      output_path / fmt::format("run{:02}/entity{:04}", run_index, entity_id));
        }
    }
}

TEST_F(EnvironmentApiControllerTest, GivenZeroTotalRun_WhenInit_ThenThrowException)
{
    service::user_settings::UserSettings user_settings;
    user_settings.simulation_results.output_directory_path = output_path;

    constexpr std::int32_t run_index{0};
    constexpr std::int32_t total_runs{0};

    const auto create_env = [](auto&&... args) { return GtGenEnvironment(args...); };

    EXPECT_THROW(create_env(user_settings, 40_ms, kArbitrarySeed, run_index, total_runs), EnvironmentException);
}

TEST_F(EnvironmentApiControllerTest,
       GivenRunIndexLessThanTotalRun_WhenInit_ThenExternalControllerConfigContainsCorrectRunIndexString)
{
    struct TestableGtGenEnvironment : public GtGenEnvironment
    {
        using GtGenEnvironment::GetExternalControllerConfigConverterData;
        using GtGenEnvironment::GtGenEnvironment;
    };

    service::user_settings::UserSettings user_settings;
    user_settings.simulation_results.output_directory_path = output_path;

    {
        constexpr std::int32_t run_index{0};
        constexpr std::int32_t total_runs{9};
        TestableGtGenEnvironment env{user_settings, 40_ms, kArbitrarySeed, run_index, total_runs};
        env.Init();
        EXPECT_EQ(env.GetExternalControllerConfigConverterData().output_settings->GetEntityOutputFolder(0),
                  output_path / "run0/entity0000");
    }

    {
        constexpr std::int32_t run_index{3};
        constexpr std::int32_t total_runs{99};
        TestableGtGenEnvironment env{user_settings, 40_ms, kArbitrarySeed, run_index, total_runs};
        env.Init();
        EXPECT_EQ(env.GetExternalControllerConfigConverterData().output_settings->GetEntityOutputFolder(0),
                  output_path / "run03/entity0000");
    }

    {
        constexpr std::int32_t run_index{42};
        constexpr std::int32_t total_runs{99};
        TestableGtGenEnvironment env{user_settings, 40_ms, kArbitrarySeed, run_index, total_runs};
        env.Init();
        EXPECT_EQ(env.GetExternalControllerConfigConverterData().output_settings->GetEntityOutputFolder(0),
                  output_path / "run42/entity0000");
    }
}

TEST_F(EnvironmentApiControllerTest,
       GivenTotalRunsAsPowerOf10_WhenInit_ThenExternalControllerConfigContainsCorrectRunIndexPrefixSize)
{
    struct TestableGtGenEnvironment : public GtGenEnvironment
    {
        using GtGenEnvironment::GetExternalControllerConfigConverterData;
        using GtGenEnvironment::GtGenEnvironment;
    };

    const auto expected_dirname = [](std::int32_t power_index) -> std::string {
        return fs::path{"run" + std::string(power_index - 1, '0') + "3"} / "entity0000";
    };

    service::user_settings::UserSettings user_settings;
    user_settings.simulation_results.output_directory_path = output_path;

    constexpr std::int32_t kSimulationRunIndex{3U};

    for (std::int32_t power_index = 1U; power_index <= std::numeric_limits<std::int32_t>::digits10; ++power_index)
    {
        const std::int32_t total_runs{static_cast<std::int32_t>(std::pow(10, power_index))};
        TestableGtGenEnvironment env{user_settings, 40_ms, kArbitrarySeed, kSimulationRunIndex, total_runs};
        env.Init();

        EXPECT_EQ(env.GetExternalControllerConfigConverterData().output_settings->GetEntityOutputFolder(0),
                  output_path / expected_dirname(power_index));
    }
}

TEST_F(EnvironmentApiControllerTest, GivenEnvironmentTestCollision)
{
    auto env = CreateEnvironmentWithDummyMap("external/Dummy.xodr", "", user_settings, kArbitrarySeed);
    std::uint64_t controller_id = 0;
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    auto& controller = env->GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    // Set-up vehicle 1 properties
    mantle_api::BoundingBox bb_vehicle_1 = {{0_m, 0_m, 0_m}, {4_m, 2_m, 1_m}};
    mantle_api::UniqueId entity_id_1 = 0;
    mantle_api::VehicleProperties vehicle_properties_1;
    vehicle_properties_1.is_host = true;
    vehicle_properties_1.bounding_box = bb_vehicle_1;
    vehicle_properties_1.type = mantle_api::EntityType::kVehicle;
    vehicle_properties_1.mass = 2000_kg;
    // Set-up vehicle 2 properties
    mantle_api::BoundingBox bb_vehicle_2 = {{0_m, 0_m, 0_m}, {4_m, 3_m, 1_m}};
    mantle_api::UniqueId entity_id_2 = 1;
    mantle_api::VehicleProperties vehicle_properties_2;
    vehicle_properties_2.bounding_box = bb_vehicle_2;
    vehicle_properties_2.type = mantle_api::EntityType::kVehicle;
    vehicle_properties_2.mass = 1000_kg;

    auto& entity_1 = env->GetEntityRepository().Create(entity_id_1, "vehicle_1", vehicle_properties_1);
    auto& entity_2 = env->GetEntityRepository().Create(entity_id_2, "vehicle_2", vehicle_properties_2);

    entity_1.SetVelocity({40_mps, 0_mps, 0_mps});
    entity_2.SetVelocity({10_mps, 0_mps, 0_mps});

    entity_1.SetPosition({0_m, 0_m, 0_m});
    entity_2.SetPosition({3.98_m, 0_m, 0_m});

    env->AddEntityToController(entity_1, controller_id);

    env->Step(10_ms);

    auto* vehicle_entity = dynamic_cast<gtgen::core::environment::entities::BaseEntity*>(&entity_1);

    // entity 1 shall have as its collision partner entity 2
    const auto& partners = vehicle_entity->GetCollisionPartnersId();
    EXPECT_EQ(partners.size(), 1);
    EXPECT_NE(std::find(partners.begin(), partners.end(), entity_id_2), partners.end());
}

TEST_F(EnvironmentApiControllerTest, GivenCollisionDetectionDisabled_ThenCollisionIgnored)
{
    // Given
    user_settings.collision_detection.enabled = false;

    auto env = CreateEnvironmentWithDummyMap("external/Dummy.xodr", "", user_settings, kArbitrarySeed);

    std::uint64_t controller_id = 0;
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    auto& controller = env->GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    // Set-up vehicle 1 properties
    mantle_api::BoundingBox bb_vehicle_1 = {{0_m, 0_m, 0_m}, {4_m, 2_m, 1_m}};
    mantle_api::UniqueId entity_id_1 = 0;
    mantle_api::VehicleProperties vehicle_properties_1;
    vehicle_properties_1.is_host = true;
    vehicle_properties_1.bounding_box = bb_vehicle_1;
    vehicle_properties_1.type = mantle_api::EntityType::kVehicle;
    vehicle_properties_1.mass = 2000_kg;
    // Set-up vehicle 2 properties
    mantle_api::BoundingBox bb_vehicle_2 = {{0_m, 0_m, 0_m}, {4_m, 3_m, 1_m}};
    mantle_api::UniqueId entity_id_2 = 1;
    mantle_api::VehicleProperties vehicle_properties_2;
    vehicle_properties_2.bounding_box = bb_vehicle_2;
    vehicle_properties_2.type = mantle_api::EntityType::kVehicle;
    vehicle_properties_2.mass = 1000_kg;

    auto& entity_1 = env->GetEntityRepository().Create(entity_id_1, "vehicle_1", vehicle_properties_1);
    auto& entity_2 = env->GetEntityRepository().Create(entity_id_2, "vehicle_2", vehicle_properties_2);

    entity_1.SetVelocity({40_mps, 0_mps, 0_mps});
    entity_2.SetVelocity({10_mps, 0_mps, 0_mps});

    entity_1.SetPosition({0_m, 0_m, 0_m});
    entity_2.SetPosition({3.98_m, 0_m, 0_m});

    env->AddEntityToController(entity_1, controller_id);

    env->Step(10_ms);

    auto* vehicle_entity = dynamic_cast<gtgen::core::environment::entities::BaseEntity*>(&entity_1);

    // entity 1 shall have no collision partners
    EXPECT_EQ(vehicle_entity->GetCollisionPartnersId().size(), 0);
}

TEST_F(EnvironmentApiControllerTest, GivenEnvironment_ThenTestNoCollision)
{
    auto env = CreateEnvironmentWithDummyMap("external/Dummy.xodr", "", user_settings, kArbitrarySeed);
    std::uint64_t controller_id = 0;
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    auto& controller = env->GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    // Set-up vehicle 1 properties
    mantle_api::BoundingBox bb_vehicle_1 = {{0_m, 0_m, 0_m}, {4_m, 2_m, 1_m}};
    mantle_api::UniqueId entity_id_1 = 0;
    mantle_api::VehicleProperties vehicle_properties_1;
    vehicle_properties_1.is_host = true;
    vehicle_properties_1.bounding_box = bb_vehicle_1;
    vehicle_properties_1.type = mantle_api::EntityType::kVehicle;
    vehicle_properties_1.mass = 2000_kg;
    // Set-up vehicle 2 properties
    mantle_api::BoundingBox bb_vehicle_2 = {{0_m, 0_m, 0_m}, {4_m, 3_m, 1_m}};
    mantle_api::UniqueId entity_id_2 = 1;
    mantle_api::VehicleProperties vehicle_properties_2;
    vehicle_properties_2.bounding_box = bb_vehicle_2;
    vehicle_properties_2.type = mantle_api::EntityType::kVehicle;
    vehicle_properties_2.mass = 1000_kg;

    auto& entity_1 = env->GetEntityRepository().Create(entity_id_1, "vehicle_1", vehicle_properties_1);
    auto& entity_2 = env->GetEntityRepository().Create(entity_id_2, "vehicle_2", vehicle_properties_2);

    entity_1.SetVelocity({40_mps, 0_mps, 0_mps});
    entity_2.SetVelocity({10_mps, 0_mps, 0_mps});

    // Positions are set so that there is no collision between entities
    entity_1.SetPosition({0_m, 0_m, 0_m});
    entity_2.SetPosition({10_m, 10_m, 0_m});

    env->AddEntityToController(entity_1, controller_id);

    env->Step(10_ms);

    auto* vehicle_entity = dynamic_cast<gtgen::core::environment::entities::BaseEntity*>(&entity_1);

    // entity 1 shall have no collision partners
    EXPECT_EQ(vehicle_entity->GetCollisionPartnersId().size(), 0);
}

TEST_F(EnvironmentApiControllerTest, GivenRequireLogCyclics_WhenDetectingCollision_ThenCreateLogFile)
{
    auto env = CreateEnvironmentWithDummyMap("external/Dummy.xodr", "", user_settings, kArbitrarySeed);
    std::uint64_t controller_id = 0;
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    auto& controller = env->GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    // Set-up vehicle 1 properties
    mantle_api::BoundingBox bb_vehicle_1 = {{0_m, 0_m, 0_m}, {4_m, 2_m, 1_m}};
    mantle_api::UniqueId entity_id_1 = 0;
    mantle_api::VehicleProperties vehicle_properties_1;
    vehicle_properties_1.is_host = true;
    vehicle_properties_1.bounding_box = bb_vehicle_1;
    vehicle_properties_1.type = mantle_api::EntityType::kVehicle;
    vehicle_properties_1.mass = 2000_kg;
    // Set-up vehicle 2 properties
    mantle_api::BoundingBox bb_vehicle_2 = {{0_m, 0_m, 0_m}, {4_m, 3_m, 1_m}};
    mantle_api::UniqueId entity_id_2 = 1;
    mantle_api::VehicleProperties vehicle_properties_2;
    vehicle_properties_2.bounding_box = bb_vehicle_2;
    vehicle_properties_2.type = mantle_api::EntityType::kVehicle;
    vehicle_properties_2.mass = 1000_kg;

    auto& entity_1 = env->GetEntityRepository().Create(entity_id_1, "vehicle_1", vehicle_properties_1);
    auto& entity_2 = env->GetEntityRepository().Create(entity_id_2, "vehicle_2", vehicle_properties_2);

    entity_1.SetVelocity({40_mps, 0_mps, 0_mps});
    entity_2.SetVelocity({10_mps, 0_mps, 0_mps});

    entity_1.SetPosition({0_m, 0_m, 0_m});
    entity_2.SetPosition({3.98_m, 0_m, 0_m});

    env->AddEntityToController(entity_1, controller_id);

    env->Step(10_ms);

    auto* vehicle_entity = dynamic_cast<gtgen::core::environment::entities::BaseEntity*>(&entity_1);

    // entity 1 shall have as its collision partner entity 2
    EXPECT_EQ(vehicle_entity->GetCollisionPartnersId().size(), 1);

    const auto& partners_1 = vehicle_entity->GetCollisionPartnersId();
    EXPECT_TRUE(std::find(partners_1.begin(), partners_1.end(), entity_id_2) != partners_1.end());

    env.reset();
    // Then
    const fs::path expected_output_file_path = output_path / "Events_Run_0.json";
    EXPECT_TRUE(fs::exists(expected_output_file_path)) << "File does not exist" << expected_output_file_path;
}

TEST_F(EnvironmentApiControllerTest, GivenEnvironment_When3Runs_ThenCoreInformationContains3Runs)
{
    const fs::path core_information_path = output_path / "CoreInformation.json";
    const std::string kCoreInformationSchemaVersion{"0.0.3"};

    constexpr std::size_t total_entities{4};
    const std::array<mantle_api::UniqueId, total_entities> entity_ids{123U, 456U, 789U, 987U};
    const std::array<std::string, total_entities> entity_names{"host_vehicle", "vehicle", "still_vehicle", "parrot"};

    constexpr std::int32_t total_runs{3};
    const std::array<std::uint32_t, total_runs> random_seeds{42, 24, 13};

    nlohmann::ordered_json expected_core_information = {{"simulatorVersion", gtgen::core::GetGtGenCoreVersion()},
                                                        {"schemaVersion", kCoreInformationSchemaVersion}};

    for (std::int32_t run_index = 0; run_index < total_runs; ++run_index)
    {
        service::user_settings::UserSettings user_settings;
        user_settings.simulation_results.output_directory_path = output_path;

        // Scope to check Environment's desctructor
        {
            auto env = CreateEnvironmentWithDummyMap(
                "external/Dummy.xodr", "", user_settings, random_seeds[run_index], run_index, total_runs);
            env->Init();
            for (std::size_t entity_index = 0; entity_index < total_entities; ++entity_index)
            {
                std::ignore = env->GetEntityRepository().Create(
                    entity_ids[entity_index], entity_names[entity_index], mantle_api::VehicleProperties{});
            }
        }

        nlohmann::ordered_json expected_sim_run{};
        expected_sim_run["id"] = run_index;
        expected_sim_run["randomSeed"] = random_seeds[run_index];
        for (std::size_t entity_index = 0; entity_index < total_entities; ++entity_index)
        {
            expected_sim_run["entities"].push_back(
                nlohmann::ordered_json{{"id", entity_ids[entity_index]}, {"name", entity_names[entity_index]}});
        }
        expected_core_information["simulationRuns"].push_back(expected_sim_run);

        // Then
        ASSERT_TRUE(fs::exists(core_information_path)) << "File does not exist: " << (core_information_path).string();
        const nlohmann::ordered_json actual_core_information =
            gtgen::core::environment::datastore::utils::ParseJson(core_information_path);
        EXPECT_EQ(expected_core_information, actual_core_information);
    }
}

}  // namespace gtgen::core::environment::api
