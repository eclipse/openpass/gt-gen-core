/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_GTGENENVIRONMENT_IGTGENENVIRONMENT_H
#define GTGEN_CORE_ENVIRONMENT_GTGENENVIRONMENT_IGTGENENVIRONMENT_H

#include "Core/Environment/Chunking/chunking.h"
#include "Core/Environment/GroundTruth/sensor_view_builder.h"
#include "Core/Environment/Host/host_vehicle_interface.h"
#include "Core/Environment/Host/host_vehicle_model.h"
#include "Core/Environment/Map/GtGenMap/gtgen_map.h"

#include <MantleAPI/Execution/i_environment.h>
#include <MantleAPI/Execution/i_environment_engine.h>
#include <osi_trafficcommand.pb.h>

namespace gtgen::core::environment::api
{

class IGtGenEnvironment : public mantle_api::IEnvironment, public mantle_api::IEnvironmentEngine
{
  public:
    virtual ~IGtGenEnvironment() override = default;
    virtual void Init() = 0;
    virtual environment::host::HostVehicleModel& GetHostVehicleModel() = 0;
    virtual const environment::map::GtGenMap& GetGtGenMap() const = 0;
    virtual const environment::proto_groundtruth::SensorViewBuilder& GetSensorViewBuilder() const = 0;
    virtual environment::chunking::StaticChunkList GetStaticChunks() const = 0;
    virtual const osi3::SensorView& GetSensorView() const = 0;
    virtual const environment::host::HostVehicleInterface& GetHostVehicleInterface() const = 0;
    virtual const std::vector<osi3::TrafficCommand>& GetTrafficCommands() const = 0;
    virtual void FinishRun() = 0;
};

}  // namespace gtgen::core::environment::api

#endif  // GTGEN_CORE_ENVIRONMENT_GTGENENVIRONMENT_IGTGENENVIRONMENT_H
