/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GtGenEnvironment/Internal/controller_prototypes.h"

#include "Core/Environment/Exception/exception.h"

#include <gtest/gtest.h>

namespace gtgen::core::environment::api
{

TEST(ControllerPrototypesTest, GivenEmptyControllerRepository_WhenMove_ThenExceptionIsThrown)
{
    service::utility::UniqueIdProvider unique_id_provider;
    ControllerPrototypes controller_repo(&unique_id_provider);

    EXPECT_THROW(controller_repo.Move(0), EnvironmentException);
}

TEST(ControllerPrototypesTest, GivenEmptyControllerRepository_WhenCreate_ThenControllerCanBeMoved)
{
    std::uint64_t expected_id{123};
    service::utility::UniqueIdProvider unique_id_provider;
    ControllerPrototypes controller_repo(&unique_id_provider);

    auto config = std::make_unique<mantle_api::NoOpControllerConfig>();
    ASSERT_NO_THROW(controller_repo.Create(expected_id, std::move(config)));

    EXPECT_NO_THROW(controller_repo.Move(expected_id));
}

TEST(ControllerPrototypesTest,
     GivenControllerRepoWithCreatedController_WhenMove_ThenControllerCannotBeReturnedSecondTime)
{
    std::uint64_t expected_id{123};
    service::utility::UniqueIdProvider unique_id_provider;
    ControllerPrototypes controller_repo(&unique_id_provider);
    auto config = std::make_unique<mantle_api::NoOpControllerConfig>();
    ASSERT_NO_THROW(controller_repo.Create(expected_id, std::move(config)));

    {
        auto moved_controller = controller_repo.Move(expected_id);
        EXPECT_EQ(moved_controller->GetUniqueId(), expected_id);
    }

    {
        EXPECT_THROW(controller_repo.Move(expected_id), EnvironmentException);
    }
}

TEST(ControllerPrototypesTest,
     GivenControllerRepoWithAnExistingController_WhenCreatingWithTheSameIdAgain_ThenExceptionThrown)
{
    service::utility::UniqueIdProvider unique_id_provider;
    ControllerPrototypes controller_repo(&unique_id_provider);
    auto config0 = std::make_unique<mantle_api::NoOpControllerConfig>();
    ASSERT_NO_THROW(controller_repo.Create(0, std::move(config0)));

    auto config1 = std::make_unique<mantle_api::NoOpControllerConfig>();
    EXPECT_THROW(controller_repo.Create(0, std::move(config1)), EnvironmentException);
}

TEST(ControllerPrototypesTest, GivenEmptyControllerRepository_WhenCreatingTwoDifferentControllers_ThenBothCanBeMoved)
{
    service::utility::UniqueIdProvider unique_id_provider;
    ControllerPrototypes controller_repo(&unique_id_provider);
    auto config0 = std::make_unique<mantle_api::NoOpControllerConfig>();
    ASSERT_NO_THROW(controller_repo.Create(0, std::move(config0)));

    auto config1 = std::make_unique<mantle_api::NoOpControllerConfig>();
    ASSERT_NO_THROW(controller_repo.Create(1, std::move(config1)));

    EXPECT_NO_THROW(controller_repo.Move(0));
    EXPECT_NO_THROW(controller_repo.Move(1));
}

TEST(ControllerPrototypesTest, GivenTrafficSwarmController_WhenMove_ThenControllerIsRemovedFromPrototypesAfterwards)
{
    mantle_api::UniqueId traffic_swarm_id{10'000};
    service::utility::UniqueIdProvider unique_id_provider;
    ControllerPrototypes controller_repo(&unique_id_provider);
    auto config0 = std::make_unique<mantle_api::NoOpControllerConfig>();
    ASSERT_NO_THROW(controller_repo.Create(traffic_swarm_id, std::move(config0)));

    EXPECT_NO_THROW(controller_repo.Move(traffic_swarm_id));
    EXPECT_FALSE(controller_repo.Contains(traffic_swarm_id));
}

}  // namespace gtgen::core::environment::api
