/*******************************************************************************
 * Copyright (c) 2022-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GtGenEnvironment/Internal/entity_producer.h"

#include "Core/Environment/Entities/pedestrian_entity.h"
#include "Core/Environment/Entities/static_object_entity.h"
#include "Core/Environment/Entities/traffic_light_entity.h"
#include "Core/Environment/Entities/traffic_sign_entity.h"
#include "Core/Environment/Entities/vehicle_entity.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Environment/StaticObjects/road_marking_properties_provider.h"
#include "Core/Environment/StaticObjects/static_object_properties_provider.h"
#include "Core/Environment/StaticObjects/supplementary_sign_properties_provider.h"
#include "Core/Environment/StaticObjects/traffic_light_properties_provider.h"
#include "Core/Environment/StaticObjects/traffic_sign_properties_provider.h"
#include "Core/Service/Logging/logging.h"
#include "Core/Service/Utility/unique_id_provider.h"

#include <units.h>

#include <algorithm>
#include <optional>
namespace gtgen::core::environment::api
{

using units::literals::operator""_m;

const std::string kObjectTypePropertyString = "object_type";
const std::string kTypePropertyString = "type";
const std::string kSubTypePropertyString = "sub_type";
const std::string kTrafficLightPropertyString = "traffic_light";
const std::string kTrafficSignPropertyString = "traffic_sign";
const std::string kRoadMarkingPropertyString = "road_marking";
const std::string kSupplementarySignPropertyString = "supplementary_sign";

namespace detail
{

std::optional<std::string> Get(const mantle_api::StaticObjectProperties& static_object_properties,
                               const std::string& property)
{
    auto it = static_object_properties.properties.find(property);
    if (it != static_object_properties.properties.end())
    {
        return it->second;
    }

    return std::nullopt;
}

std::string GetType(const mantle_api::StaticObjectProperties& static_object_properties)
{
    return Get(static_object_properties, kTypePropertyString).value_or("");
}

std::string GetSubTypeWithPrefix(const mantle_api::StaticObjectProperties& static_object_properties,
                                 const std::string prefix)
{
    std::string sub_type = Get(static_object_properties, kSubTypePropertyString).value_or("");
    if (sub_type.empty() || sub_type == "none" || sub_type == "-1")
    {
        return "";
    }

    return prefix + sub_type;
}

bool HasType(const mantle_api::StaticObjectProperties& static_object_properties)
{
    std::optional<std::string> type = Get(static_object_properties, kTypePropertyString);
    return type.has_value() && !type.value().empty();
}

bool IsValidObjectType(const std::string& value)
{
    return value.empty() || value == kRoadMarkingPropertyString || value == kSupplementarySignPropertyString ||
           value == kTrafficLightPropertyString || value == kTrafficSignPropertyString;
}

bool IsGreaterOrEqual(const std::string& value, int operand)
{
    try
    {
        return std::stoi(value) >= operand;
    }
    catch (const std::invalid_argument& e)
    {
        return false;
    }
}

bool IsValidType(const std::string& value)
{
    return value.empty() || value == "none" || IsGreaterOrEqual(value, -1);
}

void ValidateProperties(mantle_api::UniqueId id,
                        const std::string& name,
                        const mantle_api::StaticObjectProperties& static_object_properties)
{
    auto object_type = Get(static_object_properties, kObjectTypePropertyString);
    if (object_type.has_value() && !IsValidObjectType(object_type.value()))
    {
        LogAndThrow(environment::EnvironmentException(
            "EntityProducer: Given object_type {} for entity {} with id {} is invalid. "
            "Valid type values are {}, {}, {} or {}",
            object_type.value_or(""),
            name,
            id,
            kRoadMarkingPropertyString,
            kSupplementarySignPropertyString,
            kTrafficLightPropertyString,
            kTrafficSignPropertyString));
    }

    auto type = Get(static_object_properties, kTypePropertyString);
    if (type.has_value() && !IsValidType(type.value()))
    {
        LogAndThrow(
            environment::EnvironmentException("EntityProducer: Given type {} for entity {} with id {} is invalid. "
                                              "Valid type values are none, -1 and positive integers",
                                              type.value_or(""),
                                              name,
                                              id));
    }

    auto subtype = Get(static_object_properties, kSubTypePropertyString);
    if (subtype.has_value() && !IsValidType(subtype.value()))
    {
        LogAndThrow(
            environment::EnvironmentException("EntityProducer: Given sub_type {} for entity {} with id {} is invalid. "
                                              "Valid sub_type values are none, -1 and positive integers",
                                              subtype.value_or(""),
                                              name,
                                              id));
    }
}

std::optional<osi::OsiTrafficSignValueUnit> GetUnitFromString(const std::string& unit_string)
{
    const static std::map<std::string, osi::OsiTrafficSignValueUnit> unit_string_to_unit = {
        {"kph", osi::OsiTrafficSignValueUnit::kKilometerPerHour},
        {"m", osi::OsiTrafficSignValueUnit::kMeter},
        {"km", osi::OsiTrafficSignValueUnit::kKilometer},
        {"ft", osi::OsiTrafficSignValueUnit::kFeet},
        {"mi", osi::OsiTrafficSignValueUnit::kMile},
        {"mph", osi::OsiTrafficSignValueUnit::kMilePerHour}};

    auto it = unit_string_to_unit.find(unit_string);
    if (it != unit_string_to_unit.end())
    {
        return it->second;
    }

    Warn(
        "No traffic sign value unit information could be extracted from the provided string '{}'. The given "
        "string could not be translated into a unit.",
        unit_string);

    return std::nullopt;
}

template <typename T>
void SetEntityUnitFromProperties(const mantle_api::StaticObjectProperties& properties_from,
                                 const std::unique_ptr<mantle_api::StaticObjectProperties>& properties_to)
{

    if (auto it = properties_from.properties.find("unit");
        it != properties_from.properties.end() && !it->second.empty())
    {
        auto* entity_properties = dynamic_cast<T*>(properties_to.get());
        assert(entity_properties != nullptr);
        auto unit = GetUnitFromString(it->second);
        if (unit)
        {
            entity_properties->unit = *unit;
        }
    }
}

template <typename T>
void SetValueFromProperties(const mantle_api::StaticObjectProperties& properties_from,
                            const std::unique_ptr<mantle_api::StaticObjectProperties>& properties_to)
{

    if (auto it = properties_from.properties.find("value");
        it != properties_from.properties.end() && !it->second.empty())
    {
        auto* entity_properties = dynamic_cast<T*>(properties_to.get());
        assert(entity_properties != nullptr);
        try
        {
            entity_properties->value = std::stod(it->second);
        }
        catch (const std::exception&)
        {
            entity_properties->value = 0.0;
        }
    }
}

void SetVerticalOffset(mantle_api::StaticObjectProperties* static_object_properties,
                       const mantle_api::StaticObjectProperties& user_defined_object_properties)
{
    auto it = user_defined_object_properties.properties.find("mount_height");
    if (it != user_defined_object_properties.properties.end())
    {
        static_object_properties->vertical_offset = units::length::meter_t(std::stod(it->second));
    }
}

void SetBoundingBox(mantle_api::StaticObjectProperties* static_object_properties,
                    const mantle_api::StaticObjectProperties& user_defined_object_properties)
{
    const mantle_api::Dimension3 invalid_dimension{0.0_m, 0.0_m, 0.0_m};
    if (user_defined_object_properties.bounding_box.dimension != invalid_dimension)
    {
        static_object_properties->bounding_box.dimension = user_defined_object_properties.bounding_box.dimension;
    }

    static_object_properties->bounding_box.geometric_center =
        user_defined_object_properties.bounding_box.geometric_center;
}

}  // namespace detail

using namespace detail;

void EntityProducer::SetUserDefinedProperties(
    mantle_api::StaticObjectProperties* static_object_properties,
    const mantle_api::StaticObjectProperties& user_defined_object_properties) const
{
    if (static_object_properties != nullptr)
    {
        for (const auto& entry : user_defined_object_properties.properties)
        {
            static_object_properties->properties[entry.first] = entry.second;
        }

        SetBoundingBox(static_object_properties, user_defined_object_properties);
        SetVerticalOffset(static_object_properties, user_defined_object_properties);
    }
}  // namespace detail

std::unique_ptr<mantle_api::IEntity> EntityProducer::Produce(mantle_api::UniqueId id,
                                                             const std::string& name,
                                                             const mantle_api::VehicleProperties& properties) const
{
    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(id, name);

    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>(properties));

    return entity;
}

std::unique_ptr<mantle_api::IEntity> EntityProducer::Produce(mantle_api::UniqueId id,
                                                             const std::string& name,
                                                             const mantle_api::PedestrianProperties& properties) const
{
    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::PedestrianEntity>(id, name);

    entity->SetProperties(std::make_unique<mantle_api::PedestrianProperties>(properties));

    return entity;
}

std::unique_ptr<mantle_api::IEntity> EntityProducer::Produce(
    mantle_api::UniqueId id,
    const std::string& name,
    const mantle_api::StaticObjectProperties& static_object_properties) const
{
    std::unique_ptr<mantle_api::IEntity> entity;

    if (HasType(static_object_properties))
    {
        ValidateProperties(id, name, static_object_properties);

        const std::string identifier{GetType(static_object_properties) +
                                     GetSubTypeWithPrefix(static_object_properties, "-")};

        if (auto properties = static_objects::StaticObjectPropertiesProvider::Get(identifier); properties != nullptr)
        {
            entity = std::make_unique<entities::StaticObject>(id, name);
            entity->SetProperties(std::move(properties));
        }
        else if (properties = static_objects::TrafficLightPropertiesProvider::Get(identifier); properties != nullptr)
        {
            entity = std::make_unique<entities::TrafficLightEntity>(id, name);
            entity->SetProperties(std::move(properties));
        }
        else if (properties = static_objects::RoadMarkingPropertiesProvider::Get(identifier); properties != nullptr)
        {
            entity = std::make_unique<entities::StaticObject>(id, name);
            SetEntityUnitFromProperties<mantle_ext::RoadMarkingProperties>(static_object_properties, properties);
            entity->SetProperties(std::move(properties));
        }
        else if (properties = static_objects::SupplementarySignPropertiesProvider::Get(identifier);
                 properties != nullptr)
        {
            entity = std::make_unique<entities::StaticObject>(id, name);
            SetValueFromProperties<mantle_ext::SupplementarySignProperties>(static_object_properties, properties);
            SetEntityUnitFromProperties<mantle_ext::SupplementarySignProperties>(static_object_properties, properties);
            entity->SetProperties(std::move(properties));
        }
        else if (properties = static_objects::TrafficSignPropertiesProvider::Get(identifier); properties != nullptr)
        {
            entity = std::make_unique<entities::TrafficSignEntity>(id, name);
            SetEntityUnitFromProperties<mantle_ext::TrafficSignProperties>(static_object_properties, properties);
            entity->SetProperties(std::move(properties));
        }
        else
        {
            throw EnvironmentException{"Not supported static object identifier '{}'. Please check your scenario file",
                                       identifier};
        }
    }
    else
    {
        entity = std::make_unique<entities::StaticObject>(id, name);
        entity->SetProperties(std::make_unique<mantle_api::StaticObjectProperties>(static_object_properties));
    }

    auto properties = dynamic_cast<mantle_api::StaticObjectProperties*>(entity->GetProperties());
    SetUserDefinedProperties(properties, static_object_properties);

    if (auto* traffic_light_entity = dynamic_cast<entities::TrafficLightEntity*>(entity.get()))
    {
        traffic_light_entity->SetInitialStateFromProperties();
    }

    return entity;
}

}  // namespace gtgen::core::environment::api
