/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_GTGENENVIRONMENT_INTERNAL_COORDINATECONVERTER_H
#define GTGEN_CORE_ENVIRONMENT_GTGENENVIRONMENT_INTERNAL_COORDINATECONVERTER_H

#include "Core/Environment/Map/Common/coordinate_converter.h"

#include <MantleAPI/Map/i_coord_converter.h>

namespace gtgen::core::environment::api
{

using UnderlyingMapCoordinate =
    std::variant<mantle_api::OpenDriveLanePosition, mantle_api::OpenDriveRoadPosition, mantle_api::LatLonPosition>;
using Base = environment::map::IConverter<UnderlyingMapCoordinate, mantle_api::Vec3<units::length::meter_t>>;

class CoordinateConverter : public mantle_api::ICoordConverter
{
  public:
    explicit CoordinateConverter(Base* underlying_converter) : underlying_converter_{underlying_converter} {}

    mantle_api::Vec3<units::length::meter_t> Convert(mantle_api::Position position) override;

    virtual mantle_api::Orientation3<units::angle::radian_t> GetLaneOrientation(
        const mantle_api::OpenDriveLanePosition& open_drive_lane_position) override;

    virtual mantle_api::Orientation3<units::angle::radian_t> GetRoadOrientation(
        const mantle_api::OpenDriveRoadPosition& open_drive_road_position) override;

  private:
    environment::map::IConverter<UnderlyingMapCoordinate, mantle_api::Vec3<units::length::meter_t>>*
        underlying_converter_;
};

}  // namespace gtgen::core::environment::api

#endif  // GTGEN_CORE_ENVIRONMENT_GTGENENVIRONMENT_INTERNAL_COORDINATECONVERTER_H
