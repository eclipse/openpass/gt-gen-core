/*******************************************************************************
 * Copyright (c) 2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_TRAFFICCOMMAND_JSONTYPESCONVERSION_H
#define GTGEN_CORE_ENVIRONMENT_TRAFFICCOMMAND_JSONTYPESCONVERSION_H

#include "Core/Environment/Exception/exception.h"

#include <MantleAPI/Common/clothoid_spline.h>
#include <MantleAPI/Common/poly_line.h>
#include <MantleAPI/Common/pose.h>
#include <MantleAPI/Traffic/control_strategy.h>
#include <MantleAPI/Traffic/vehicle_light_properties.h>
#include <nlohmann/json.hpp>

#include <string>
#include <variant>

namespace nlohmann
{

using namespace mantle_api;

NLOHMANN_JSON_SERIALIZE_ENUM(TrajectoryReferencePoint,
                             {{TrajectoryReferencePoint::kUndefined, "refPointUndefined"},
                              {TrajectoryReferencePoint::kRearAxle, "refPointRearAxle"},
                              {TrajectoryReferencePoint::kFrontAxle, "refPointFrontAxle"},
                              {TrajectoryReferencePoint::kCenterOfMass, "refPointCenterOfMass"},
                              {TrajectoryReferencePoint::kBoundingBoxCenter, "refPointBoundingBoxCenter"}})

NLOHMANN_JSON_SERIALIZE_ENUM(VehicleLightType,  // NOLINT(cppcoreguidelines-avoid-c-arrays)
                             {{VehicleLightType::kBrakeLights, "brakeLights"},
                              {VehicleLightType::kDaytimeRunningLights, "daytimeRunningLights"},
                              {VehicleLightType::kFogLights, "fogLights"},
                              {VehicleLightType::kFogLightsFront, "fogLightsFront"},
                              {VehicleLightType::kFogLightsRear, "fogLightsRear"},
                              {VehicleLightType::kHighBeam, "highBeam"},
                              {VehicleLightType::kIndicatorLeft, "indicatorLeft"},
                              {VehicleLightType::kIndicatorRight, "indicatorRight"},
                              {VehicleLightType::kLicensePlateIllumination, "licensePlateIllumination"},
                              {VehicleLightType::kLowBeam, "lowBeam"},
                              {VehicleLightType::kReversingLights, "reversingLights"},
                              {VehicleLightType::kSpecialPurposeLights, "specialPurposeLights"},
                              {VehicleLightType::kWarningLights, "warningLights"}})

NLOHMANN_JSON_SERIALIZE_ENUM(LightMode,  // NOLINT(cppcoreguidelines-avoid-c-arrays)
                             {{LightMode::kFlashing, "flashing"}, {LightMode::kOff, "off"}, {LightMode::kOn, "on"}})

#define ADL_MANTLE_TYPES_SERIALIZER(Type, ToJsonFunctionBody)                         \
    template <>                                                                       \
    struct adl_serializer<Type>                                                       \
    {                                                                                 \
        static void to_json(ordered_json& node, const Type& value) ToJsonFunctionBody \
    };

ADL_MANTLE_TYPES_SERIALIZER(LightType, {
    if (const VehicleLightType* vehicle_light_type = std::get_if<VehicleLightType>(&value))
    {
        node["vehicleLight"] = *vehicle_light_type;
    }
    else
    {
        node["userDefinedLight"] = std::get<std::string>(value);
    }
})

ADL_MANTLE_TYPES_SERIALIZER(LightState, { node["mode"] = value.light_mode; })

ADL_MANTLE_TYPES_SERIALIZER(VehicleLightStatesControlStrategy, {
    node["lightType"] = value.light_type;
    node["lightState"] = value.light_state;
})

ADL_MANTLE_TYPES_SERIALIZER(Pose, {
    node["x"] = value.position.x.value();
    node["y"] = value.position.y.value();
    node["z"] = value.position.z.value();

    node["yaw"] = value.orientation.yaw.value();
    node["pitch"] = value.orientation.pitch.value();
    node["roll"] = value.orientation.roll.value();
})

ADL_MANTLE_TYPES_SERIALIZER(ClothoidSplineSegment, {
    node["curvatureEnd"] = value.curvature_end.value();
    node["curvatureStart"] = value.curvature_start.value();
    node["hOffset"] = value.h_offset.value();
    node["length"] = value.length.value();
    if (value.position_start.has_value())
    {
        node["positionStart"] = value.position_start.value();
    }
})

ADL_MANTLE_TYPES_SERIALIZER(ClothoidSpline, {
    if (!value.segments.empty())
    {
        node["segments"] = value.segments;
    }
})

using TrajectoryTypes = std::variant<PolyLine, ClothoidSpline>;
ADL_MANTLE_TYPES_SERIALIZER(TrajectoryTypes, {
    if (const ClothoidSpline* clothoid_spline = std::get_if<ClothoidSpline>(&value))
    {
        node["clothoidSpline"] = *clothoid_spline;
    }
    else
    {
        throw gtgen::core::environment::EnvironmentException{"Unsupported trajectory type"};
    }
})

ADL_MANTLE_TYPES_SERIALIZER(FollowTrajectoryControlStrategy, {
    node["trajectory"] = value.trajectory.type;
    node["referencePoint"] = value.trajectory.reference;
})

}  // namespace nlohmann

#endif  // GTGEN_CORE_ENVIRONMENT_TRAFFICCOMMAND_JSONTYPESCONVERSION_H
