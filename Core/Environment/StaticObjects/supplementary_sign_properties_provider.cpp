/*******************************************************************************
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/StaticObjects/supplementary_sign_properties_provider.h"

#include "Core/Environment/StaticObjects/utils.h"
#include "Core/Service/Logging/logging.h"

namespace gtgen::core::environment::static_objects
{
using units::literals::operator""_m;

std::unique_ptr<mantle_api::StaticObjectProperties> SupplementarySignPropertiesProvider::Get(
    const std::string& identifier)
{
    auto it = kStvoSignsToOsiSupplementarySignTypes.find(identifier);
    if (it != kStvoSignsToOsiSupplementarySignTypes.end())
    {
        return ToProperties(identifier);
    }
    return nullptr;
}

std::unique_ptr<mantle_api::StaticObjectProperties> SupplementarySignPropertiesProvider::ToProperties(
    const std::string& identifier)
{
    auto supplementary_sign_properties = std::make_unique<mantle_ext::SupplementarySignProperties>();
    supplementary_sign_properties->type = mantle_api::EntityType::kStatic;

    supplementary_sign_properties->supplementary_sign_type = GetOsiSupplementarySignType(identifier);
    supplementary_sign_properties->actors = GetOsiSupplementarySignActors(identifier);
    supplementary_sign_properties->variability = osi::OsiTrafficSignVariability::kFixed;

    supplementary_sign_properties->text = "";

    return supplementary_sign_properties;
}

osi::OsiSupplementarySignType SupplementarySignPropertiesProvider::GetOsiSupplementarySignType(
    const std::string& identifier)
{
    // first try to find the whole type
    if (auto it = kStvoSignsToOsiSupplementarySignTypes.find(identifier);
        it != kStvoSignsToOsiSupplementarySignTypes.end())
    {
        return it->second;
    }

    // if that failed, only search for the type
    std::string type_identifier = GetTypeIdentifier(identifier);
    auto it = kStvoSignsToOsiSupplementarySignTypes.find(type_identifier);
    if (it != kStvoSignsToOsiSupplementarySignTypes.end())
    {
        return it->second;
    }

    Warn("Could not convert provided identifier '{}' to OSI supplementary sign type. It will be treated as TYPE_OTHER.",
         identifier);

    return osi::OsiSupplementarySignType::kOther;
}

std::vector<osi::OsiSupplementarySignActor> SupplementarySignPropertiesProvider::GetOsiSupplementarySignActors(
    const std::string& identifier)
{
    if (auto it = kStvoSignsToOsiSupplementarySignActors.find(identifier);
        it != kStvoSignsToOsiSupplementarySignActors.end())
    {
        return it->second;
    }

    Warn(
        "Could not convert provided identifier '{}' to OSI supplementary sign actors. It will be treated as "
        "TYPE_UNKNOWN.",
        identifier);

    return {osi::OsiSupplementarySignActor::kUnknown};
}

// maps german traffic law (StVO) supplementary sign labels to OSI supplementary signs
// NOLINTNEXTLINE(cert-err58-cpp)
const SupplementarySignPropertiesProvider::TypeDataMap
    SupplementarySignPropertiesProvider::kStvoSignsToOsiSupplementarySignTypes{
        {"1007-30", osi::OsiSupplementarySignType::kText},
        {"1007-31", osi::OsiSupplementarySignType::kText},
        {"1007-32", osi::OsiSupplementarySignType::kText},
        {"1007-33", osi::OsiSupplementarySignType::kText},
        {"1007-34", osi::OsiSupplementarySignType::kText},
        {"1007-35", osi::OsiSupplementarySignType::kText},
        {"1007-36", osi::OsiSupplementarySignType::kText},
        {"1007-37", osi::OsiSupplementarySignType::kText},
        {"1007-38", osi::OsiSupplementarySignType::kText},
        {"1007-39", osi::OsiSupplementarySignType::kText},
        {"1007-50", osi::OsiSupplementarySignType::kText},
        {"1007-51", osi::OsiSupplementarySignType::kText},
        {"1007-52", osi::OsiSupplementarySignType::kText},
        {"1007-53", osi::OsiSupplementarySignType::kText},
        {"1007-54", osi::OsiSupplementarySignType::kText},
        {"1007-57", osi::OsiSupplementarySignType::kText},
        {"1007-58", osi::OsiSupplementarySignType::kText},
        {"1007-60", osi::OsiSupplementarySignType::kText},
        {"1007-61", osi::OsiSupplementarySignType::kText},
        {"1007-62", osi::OsiSupplementarySignType::kText},
        {"1008-30", osi::OsiSupplementarySignType::kText},
        {"1008-31", osi::OsiSupplementarySignType::kText},
        {"1008-32", osi::OsiSupplementarySignType::kText},
        {"1008-33", osi::OsiSupplementarySignType::kText},
        {"1008-34", osi::OsiSupplementarySignType::kText},
        {"1012-30", osi::OsiSupplementarySignType::kText},
        {"1012-31", osi::OsiSupplementarySignType::kText},
        {"1012-34", osi::OsiSupplementarySignType::kText},
        {"1012-35", osi::OsiSupplementarySignType::kText},
        {"1012-36", osi::OsiSupplementarySignType::kText},
        {"1012-37", osi::OsiSupplementarySignType::kText},
        {"1012-38", osi::OsiSupplementarySignType::kText},
        {"1012-50", osi::OsiSupplementarySignType::kText},
        {"1012-51", osi::OsiSupplementarySignType::kText},
        {"1012-52", osi::OsiSupplementarySignType::kText},
        {"1012-53", osi::OsiSupplementarySignType::kText},
        {"1013-50", osi::OsiSupplementarySignType::kText},
        {"1013-51", osi::OsiSupplementarySignType::kText},
        {"1014-50", osi::OsiSupplementarySignType::kText},
        {"1028-31", osi::OsiSupplementarySignType::kText},
        {"1053-30", osi::OsiSupplementarySignType::kText},
        {"1053-31", osi::OsiSupplementarySignType::kText},
        {"1053-32", osi::OsiSupplementarySignType::kText},
        {"1053-34", osi::OsiSupplementarySignType::kText},
        {"1053-36", osi::OsiSupplementarySignType::kText},
        {"1053-52", osi::OsiSupplementarySignType::kText},
        {"1053-53", osi::OsiSupplementarySignType::kText},
        {"1004-32", osi::OsiSupplementarySignType::kSpace},
        {"1005-30", osi::OsiSupplementarySignType::kSpace},
        {"1007-59", osi::OsiSupplementarySignType::kSpace},
        {"1013-52", osi::OsiSupplementarySignType::kSpace},
        {"1028-33", osi::OsiSupplementarySignType::kSpace},
        {"1040-30", osi::OsiSupplementarySignType::kTime},
        {"1040-31", osi::OsiSupplementarySignType::kTime},
        {"1040-34", osi::OsiSupplementarySignType::kTime},
        {"1040-35", osi::OsiSupplementarySignType::kTime},
        {"1040-36", osi::OsiSupplementarySignType::kTime},
        {"1042-30", osi::OsiSupplementarySignType::kTime},
        {"1042-30", osi::OsiSupplementarySignType::kTime},
        {"1042-31", osi::OsiSupplementarySignType::kTime},
        {"1042-32", osi::OsiSupplementarySignType::kTime},
        {"1042-33", osi::OsiSupplementarySignType::kTime},
        {"1042-34", osi::OsiSupplementarySignType::kTime},
        {"1042-35", osi::OsiSupplementarySignType::kTime},
        {"1042-36", osi::OsiSupplementarySignType::kTime},
        {"1042-37", osi::OsiSupplementarySignType::kTime},
        {"1042-38", osi::OsiSupplementarySignType::kTime},
        {"1042-51", osi::OsiSupplementarySignType::kTime},
        {"1042-53", osi::OsiSupplementarySignType::kTime},
        {"1000-13", osi::OsiSupplementarySignType::kArow},
        {"1000-23", osi::OsiSupplementarySignType::kArow},
        {"1000-30", osi::OsiSupplementarySignType::kArow},
        {"1000-31", osi::OsiSupplementarySignType::kArow},
        {"1000-34", osi::OsiSupplementarySignType::kArow},
        {"1000-12", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1000-22", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1000-32", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1000-33", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1010-50", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1010-51", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1010-52", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1010-53", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1010-54", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1010-55", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1010-56", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1010-57", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1010-58", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1010-59", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1010-60", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1010-61", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1010-62", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1010-63", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1010-64", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1010-65", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1010-66", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1010-67", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1012-32", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1049-11", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1050-30", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1050-31", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1050-32", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1050-33", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1060-32", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1044-10", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1044-11", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1044-12", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1044-30", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1048-14", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1048-15", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1048-18", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1048-20", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1049-12", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1049-13", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1052-30", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1052-31", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1010-10", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1010-11", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1010-12", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1010-13", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1040-10", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1007-55", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1007-56", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1006-30", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1012-33", osi::OsiSupplementarySignType::kConstrainedTo},
        {"1020-11", osi::OsiSupplementarySignType::kExcept},
        {"1020-12", osi::OsiSupplementarySignType::kExcept},
        {"1020-14", osi::OsiSupplementarySignType::kExcept},
        {"1020-30", osi::OsiSupplementarySignType::kExcept},
        {"1020-31", osi::OsiSupplementarySignType::kExcept},
        {"1020-32", osi::OsiSupplementarySignType::kExcept},
        {"1022-10", osi::OsiSupplementarySignType::kExcept},
        {"1022-11", osi::OsiSupplementarySignType::kExcept},
        {"1022-12", osi::OsiSupplementarySignType::kExcept},
        {"1022-13", osi::OsiSupplementarySignType::kExcept},
        {"1022-14", osi::OsiSupplementarySignType::kExcept},
        {"1022-15", osi::OsiSupplementarySignType::kExcept},
        {"1024-10", osi::OsiSupplementarySignType::kExcept},
        {"1024-11", osi::OsiSupplementarySignType::kExcept},
        {"1024-12", osi::OsiSupplementarySignType::kExcept},
        {"1024-13", osi::OsiSupplementarySignType::kExcept},
        {"1024-14", osi::OsiSupplementarySignType::kExcept},
        {"1024-15", osi::OsiSupplementarySignType::kExcept},
        {"1024-16", osi::OsiSupplementarySignType::kExcept},
        {"1024-17", osi::OsiSupplementarySignType::kExcept},
        {"1024-18", osi::OsiSupplementarySignType::kExcept},
        {"1024-19", osi::OsiSupplementarySignType::kExcept},
        {"1024-20", osi::OsiSupplementarySignType::kExcept},
        {"1026-31", osi::OsiSupplementarySignType::kExcept},
        {"1026-32", osi::OsiSupplementarySignType::kExcept},
        {"1026-33", osi::OsiSupplementarySignType::kExcept},
        {"1026-34", osi::OsiSupplementarySignType::kExcept},
        {"1026-35", osi::OsiSupplementarySignType::kExcept},
        {"1026-36", osi::OsiSupplementarySignType::kExcept},
        {"1026-37", osi::OsiSupplementarySignType::kExcept},
        {"1026-38", osi::OsiSupplementarySignType::kExcept},
        {"1026-39", osi::OsiSupplementarySignType::kExcept},
        {"1026-60", osi::OsiSupplementarySignType::kExcept},
        {"1026-61", osi::OsiSupplementarySignType::kExcept},
        {"1026-62", osi::OsiSupplementarySignType::kExcept},
        {"1026-63", osi::OsiSupplementarySignType::kExcept},
        {"1028-30", osi::OsiSupplementarySignType::kExcept},
        {"1028-32", osi::OsiSupplementarySignType::kExcept},
        {"1028-34", osi::OsiSupplementarySignType::kExcept},
        {"1031-50", osi::OsiSupplementarySignType::kExcept},
        {"1031-51", osi::OsiSupplementarySignType::kExcept},
        {"1031-52", osi::OsiSupplementarySignType::kExcept},
        {"1001-30", osi::OsiSupplementarySignType::kValidForDistance},
        {"1001-32", osi::OsiSupplementarySignType::kValidForDistance},
        {"1001-33", osi::OsiSupplementarySignType::kValidForDistance},
        {"1001-34", osi::OsiSupplementarySignType::kValidForDistance},
        {"1001-35", osi::OsiSupplementarySignType::kValidForDistance},
        {"1002-10", osi::OsiSupplementarySignType::kPriorityRoadBottomLeftFourWay},
        {"1002-11", osi::OsiSupplementarySignType::kPriorityRoadTopLeftFourWay},
        {"1002-12", osi::OsiSupplementarySignType::kPriorityRoadBottomLeftThreeWayStraight},
        {"1002-13", osi::OsiSupplementarySignType::kPriorityRoadBottomLeftThreeWaySideways},
        {"1002-14", osi::OsiSupplementarySignType::kPriorityRoadTopLeftThreeWayStraight},
        {"1002-20", osi::OsiSupplementarySignType::kPriorityRoadBottomRightFourWay},
        {"1002-21", osi::OsiSupplementarySignType::kPriorityRoadTopRightFourWay},
        {"1002-22", osi::OsiSupplementarySignType::kPriorityRoadBottomRightThreeWayStraight},
        {"1002-23", osi::OsiSupplementarySignType::kPriorityRoadBottomRightThreeWaySideway},
        {"1002-24", osi::OsiSupplementarySignType::kPriorityRoadTopRightThreeWayStraight},
        {"1004-30", osi::OsiSupplementarySignType::kValidInDistance},
        {"1004-31", osi::OsiSupplementarySignType::kValidInDistance},
        {"1000-10", osi::OsiSupplementarySignType::kLeftArrow},
        {"1000-20", osi::OsiSupplementarySignType::kRightArrow},
        {"1000-21", osi::OsiSupplementarySignType::kRightBendArrow},
        {"1006-31", osi::OsiSupplementarySignType::kAccident},
        {"1010-14", osi::OsiSupplementarySignType::kRollingHighwayInformation},
        {"1010-15", osi::OsiSupplementarySignType::kServices},
        {"1040-32", osi::OsiSupplementarySignType::kParkingDiscTimeRestriction},
        {"1040-33", osi::OsiSupplementarySignType::kParkingDiscTimeRestriction},
        {"1053-33", osi::OsiSupplementarySignType::kWeight},
        {"1053-37", osi::OsiSupplementarySignType::kWeight},
        {"1060-33", osi::OsiSupplementarySignType::kWeight},
        {"1053-35", osi::OsiSupplementarySignType::kWet},
        {"1053-38", osi::OsiSupplementarySignType::kParkingConstraint},
        {"1053-39", osi::OsiSupplementarySignType::kParkingConstraint},
        {"1060-31", osi::OsiSupplementarySignType::kNoWaitingSideStripes}};

// maps german traffic law (StVO) supplementary sign labels to OSI supplementary sign actors
// NOLINTNEXTLINE(cert-err58-cpp)
const SupplementarySignPropertiesProvider::ActorDataMap
    SupplementarySignPropertiesProvider::kStvoSignsToOsiSupplementarySignActors{
        {"1000-12", {osi::OsiSupplementarySignActor::kPedestrians}},
        {"1000-22", {osi::OsiSupplementarySignActor::kPedestrians}},
        {"1000-32", {osi::OsiSupplementarySignActor::kBicycles}},
        {"1000-33", {osi::OsiSupplementarySignActor::kBicycles}},
        {"1010-50", {osi::OsiSupplementarySignActor::kMotorizedMultitrackVehicles}},
        {"1010-51", {osi::OsiSupplementarySignActor::kTrucks}},
        {"1010-52", {osi::OsiSupplementarySignActor::kBicycles}},
        {"1010-53", {osi::OsiSupplementarySignActor::kPedestrians}},
        {"1010-54", {osi::OsiSupplementarySignActor::kHorseRiders}},
        {"1010-55", {osi::OsiSupplementarySignActor::kCattle}},
        {"1010-56", {osi::OsiSupplementarySignActor::kTrams}},
        {"1010-57", {osi::OsiSupplementarySignActor::kBuses}},
        {"1010-58", {osi::OsiSupplementarySignActor::kCars}},
        {"1010-59", {osi::OsiSupplementarySignActor::kCarsWithTrailers}},
        {"1010-60", {osi::OsiSupplementarySignActor::kTrucksWithTrailers}},
        {"1010-61", {osi::OsiSupplementarySignActor::kTractors}},
        {"1010-62", {osi::OsiSupplementarySignActor::kMotorcycles}},
        {"1010-63", {osi::OsiSupplementarySignActor::kMopeds}},
        {"1010-64", {osi::OsiSupplementarySignActor::kHorseCarriages}},
        {"1010-65", {osi::OsiSupplementarySignActor::kEbikes}},
        {"1010-66", {osi::OsiSupplementarySignActor::kElectricVehicles}},
        {"1010-67", {osi::OsiSupplementarySignActor::kCampers}},
        {"1012-32", {osi::OsiSupplementarySignActor::kBicycles}},
        {"1049-11", {osi::OsiSupplementarySignActor::kTractors}},
        {"1050-30", {osi::OsiSupplementarySignActor::kTaxis}},
        {"1050-31", {osi::OsiSupplementarySignActor::kTaxis}},
        {"1050-32", {osi::OsiSupplementarySignActor::kElectricVehicles}},
        {"1050-33", {osi::OsiSupplementarySignActor::kElectricVehicles}},
        {"1060-32", {osi::OsiSupplementarySignActor::kBuses, osi::OsiSupplementarySignActor::kCarsWithTrailers}},
        {"1044-10", {osi::OsiSupplementarySignActor::kDisabledPersons}},
        {"1044-11", {osi::OsiSupplementarySignActor::kDisabledPersons}},
        {"1044-12", {osi::OsiSupplementarySignActor::kDisabledPersons}},
        {"1044-30", {osi::OsiSupplementarySignActor::kResidents}},
        {"1048-14", {osi::OsiSupplementarySignActor::kTrucksWithSemitrailers}},
        {"1048-15",
         {osi::OsiSupplementarySignActor::kTrucksWithSemitrailers,
          osi::OsiSupplementarySignActor::kTrucksWithSemitrailers}},
        {"1048-18", {osi::OsiSupplementarySignActor::kRailroadTraffic}},
        {"1048-20", {osi::OsiSupplementarySignActor::kCarsWithTrailers, osi::OsiSupplementarySignActor::kTrucks}},
        {"1049-12", {osi::OsiSupplementarySignActor::kMilitaryVehicles}},
        {"1049-13",
         {osi::OsiSupplementarySignActor::kBuses,
          osi::OsiSupplementarySignActor::kCarsWithTrailers,
          osi::OsiSupplementarySignActor::kTrucks}},
        {"1052-30", {osi::OsiSupplementarySignActor::kHazardousGoodsVehicles}},
        {"1052-31", {osi::OsiSupplementarySignActor::kWaterPollutantVehicles}},
        {"1010-10", {osi::OsiSupplementarySignActor::kChildren}},
        {"1010-11", {osi::OsiSupplementarySignActor::kWinterSportspeople}},
        {"1010-12", {osi::OsiSupplementarySignActor::kTrailers}},
        {"1010-13", {osi::OsiSupplementarySignActor::kCaravans}},
        {"1040-10", {osi::OsiSupplementarySignActor::kWinterSportspeople}},
        {"1007-55", {osi::OsiSupplementarySignActor::kWinterSportspeople}},
        {"1007-56", {osi::OsiSupplementarySignActor::kWinterSportspeople}},
        {"1006-30", {osi::OsiSupplementarySignActor::kCarsWithCaravans}},
        {"1012-33", {osi::OsiSupplementarySignActor::kMopeds}},
        {"1020-11", {osi::OsiSupplementarySignActor::kDisabledPersons}},
        {"1020-12", {osi::OsiSupplementarySignActor::kBicycles, osi::OsiSupplementarySignActor::kResidents}},
        {"1020-14", {osi::OsiSupplementarySignActor::kWinterSportspeople}},
        {"1020-30", {osi::OsiSupplementarySignActor::kResidents}},
        {"1020-31", {osi::OsiSupplementarySignActor::kResidents}},
        {"1020-32", {osi::OsiSupplementarySignActor::kResidents}},
        {"1022-10", {osi::OsiSupplementarySignActor::kBicycles}},
        {"1022-11", {osi::OsiSupplementarySignActor::kMopeds}},
        {"1022-12", {osi::OsiSupplementarySignActor::kMotorcycles}},
        {"1022-13", {osi::OsiSupplementarySignActor::kEbikes}},
        {"1022-14", {osi::OsiSupplementarySignActor::kBicycles, osi::OsiSupplementarySignActor::kMopeds}},
        {"1022-15", {osi::OsiSupplementarySignActor::kEbikes, osi::OsiSupplementarySignActor::kMopeds}},
        {"1024-10", {osi::OsiSupplementarySignActor::kCars}},
        {"1024-11", {osi::OsiSupplementarySignActor::kCarsWithTrailers}},
        {"1024-12", {osi::OsiSupplementarySignActor::kTrucks}},
        {"1024-13", {osi::OsiSupplementarySignActor::kTrucksWithTrailers}},
        {"1024-14", {osi::OsiSupplementarySignActor::kBuses}},
        {"1024-15", {osi::OsiSupplementarySignActor::kRailroadTraffic}},
        {"1024-16", {osi::OsiSupplementarySignActor::kTrams}},
        {"1024-17", {osi::OsiSupplementarySignActor::kTractors}},
        {"1024-18", {osi::OsiSupplementarySignActor::kHorseCarriages}},
        {"1024-19", {osi::OsiSupplementarySignActor::kCampers}},
        {"1024-20", {osi::OsiSupplementarySignActor::kElectricVehicles}},
        {"1026-31", {osi::OsiSupplementarySignActor::kBuses}},
        {"1026-32", {osi::OsiSupplementarySignActor::kPublicTransportVehicles}},
        {"1026-33", {osi::OsiSupplementarySignActor::kEmergencyVehicles}},
        {"1026-34", {osi::OsiSupplementarySignActor::kMedicalVehicles}},
        {"1026-35", {osi::OsiSupplementarySignActor::kDeliveryVehicles}},
        {"1026-36", {osi::OsiSupplementarySignActor::kAgriculturalVehicles}},
        {"1026-37", {osi::OsiSupplementarySignActor::kForestryVehicles}},
        {"1026-38",
         {osi::OsiSupplementarySignActor::kForestryVehicles, osi::OsiSupplementarySignActor::kAgriculturalVehicles}},
        {"1026-39", {osi::OsiSupplementarySignActor::kOperationalAndUtilityVehicles}},
        {"1026-60", {osi::OsiSupplementarySignActor::kElectricVehicles}},
        {"1026-61", {osi::OsiSupplementarySignActor::kElectricVehicles}},
        {"1026-62", {osi::OsiSupplementarySignActor::kSlurryTransport}},
        {"1026-63", {osi::OsiSupplementarySignActor::kEbikes}},
        {"1028-30", {osi::OsiSupplementarySignActor::kConstructionVehicles}},
        {"1028-32", {osi::OsiSupplementarySignActor::kResidents}},
        {"1028-34", {osi::OsiSupplementarySignActor::kFerryUsers}},
        {"1031-50",
         {osi::OsiSupplementarySignActor::kVehiclesWithRedBadges,
          osi::OsiSupplementarySignActor::kVehiclesWithYellowBadges,
          osi::OsiSupplementarySignActor::kVehiclesWithGreenBadges}},
        {"1031-51",
         {osi::OsiSupplementarySignActor::kVehiclesWithYellowBadges,
          osi::OsiSupplementarySignActor::kVehiclesWithGreenBadges}},
        {"1031-52", {osi::OsiSupplementarySignActor::kVehiclesWithGreenBadges}}};

}  // namespace gtgen::core::environment::static_objects
