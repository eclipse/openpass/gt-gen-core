/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_STATICOBJECTS_STATICOBJECTPROPERTIESPROVIDER_H
#define GTGEN_CORE_ENVIRONMENT_STATICOBJECTS_STATICOBJECTPROPERTIESPROVIDER_H

#include "Core/Service/MantleApiExtension/static_object_properties.h"
#include "Core/Service/Osi/stationary_object_types.h"

#include <map>
#include <memory>
#include <string>

namespace gtgen::core::environment::static_objects
{

class StaticObjectPropertiesProvider
{
  public:
    static std::unique_ptr<mantle_api::StaticObjectProperties> Get(const std::string& identifier);

  private:
    struct StaticObjectData
    {
        std::string name;
        mantle_api::Dimension3 dimension;
        mantle_api::StaticObjectType type;
        osi::StationaryObjectEntityMaterial material;
    };
    using DataMap = std::map<std::string, StaticObjectData>;
    const static DataMap kIdentifiersToStaticObjects;

    static std::unique_ptr<mantle_api::StaticObjectProperties> ToProperties(const StaticObjectData& data);
};

}  // namespace gtgen::core::environment::static_objects

#endif  // GTGEN_CORE_ENVIRONMENT_STATICOBJECTS_STATICOBJECTPROPERTIESPROVIDER_H
