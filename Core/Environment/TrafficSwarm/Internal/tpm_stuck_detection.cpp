/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/TrafficSwarm/Internal/tpm_stuck_detection.h"

#include "Core/Service/Logging/logging.h"
#include "Core/Service/Utility/clock.h"

namespace gtgen::core::environment::traffic_swarm
{
using units::literals::operator""_m;

bool DogStuckDetection::IsStuck(
    const mantle_api::IEntity* entity,
    map::IConverter<map::UnderlyingMapCoordinate, mantle_api::Vec3<units::length::meter_t>>* coord_converter)
{
    if (IsDog(entity))
    {
        auto id = entity->GetUniqueId();
        auto current_position = entity->GetPosition();
        auto current_time = service::utility::Clock::Instance().Now();

        auto& pos_to_time = stuck_map_[id];
        if (HasNotMoved(pos_to_time.first, current_position))
        {
            if (IsTimeout(current_time, pos_to_time.second))
            {
                Error("DOG with ID {} is stuck at {}",
                      id,
                      map::GetMapCoordinateString(current_position, coord_converter));
                stuck_map_.erase(id);
                return true;
            }
        }
        else
        {
            pos_to_time.first = current_position;
            pos_to_time.second = current_time;
        }
    }
    return false;
}

bool DogStuckDetection::IsDog(const mantle_api::IEntity* entity) const
{
    if (auto* vehicle_properties = dynamic_cast<const mantle_api::VehicleProperties*>(entity->GetProperties()))
    {
        auto it = vehicle_properties->properties.find("external");
        if (it != vehicle_properties->properties.end())
        {
            return it->second == "dog" && !vehicle_properties->is_host;
        }
    }
    return false;
}

bool DogStuckDetection::HasNotMoved(const mantle_api::Vec3<units::length::meter_t>& old_pos,
                                    const mantle_api::Vec3<units::length::meter_t>& new_pos) const
{
    return mantle_api::AlmostEqual(old_pos.x, new_pos.x, 0.0001_m, true) &&
           mantle_api::AlmostEqual(old_pos.y, new_pos.y, 0.0001_m, true);
}

bool DogStuckDetection::IsTimeout(const mantle_api::Time& current_time, const mantle_api::Time& last_move_time) const
{
    return current_time >= timeout && current_time - timeout >= last_move_time;
}

}  // namespace gtgen::core::environment::traffic_swarm
