/*******************************************************************************
 * Copyright (c) 2022-2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/composite_controller_validator.h"

#include "Core/Environment/Controller/Internal/ControlUnits/follow_trajectory_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/follow_trajectory_with_speed_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/host_vehicle_interface_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/indicator_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/keep_velocity_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/lane_change_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/lane_offset_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/maneuver_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/no_op_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/path_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/recovery_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/traffic_light_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/traffic_participant_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/vehicle_model_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/velocity_control_unit.h"

#include <algorithm>

namespace gtgen::core::environment::controller
{

namespace detail
{

bool IsLongitudinalControlUnit(const IControlUnit* control_unit)
{
    return dynamic_cast<const VelocityControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<const KeepVelocityControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<const HostVehicleInterfaceControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<const TrafficParticipantControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<const FollowTrajectoryWithSpeedControlUnit*>(control_unit) != nullptr;
}

bool IsRoutingControlUnit(const IControlUnit* control_unit)
{
    return dynamic_cast<const PathControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<const ManeuverControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<const VehicleModelControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<const TrafficParticipantControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<const LaneChangeControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<const FollowTrajectoryWithSpeedControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<const FollowTrajectoryControlUnit*>(control_unit) != nullptr;
}

bool IsLateralControlUnit(const IControlUnit* control_unit)
{
    return dynamic_cast<const LaneOffsetControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<const LaneChangeControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<const ManeuverControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<const HostVehicleInterfaceControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<const TrafficParticipantControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<const FollowTrajectoryWithSpeedControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<const FollowTrajectoryControlUnit*>(control_unit) != nullptr;
}

bool IsIndicatorControlUnit(const IControlUnit* control_unit)
{
    return dynamic_cast<const IndicatorControlUnit*>(control_unit) != nullptr;
}

template <bool (*Predicate)(const IControlUnit*)>
void DeleteControlUnit(const IControlUnit* control_unit,
                       std::vector<std::unique_ptr<IControlUnit>>& control_units,
                       mantle_api::UniqueId unique_id)
{
    if (!Predicate(control_unit))
    {
        return;
    }
    control_units.erase(
        std::remove_if(control_units.begin(),
                       control_units.end(),
                       [unique_id](const std::unique_ptr<IControlUnit>& unit) {
                           const bool result = Predicate(unit.get());
                           if (result)
                           {
                               Info("Deleting {} in composite controller #{}", unit->GetName(), unique_id);
                           }
                           return result;
                       }),
        control_units.end());
}
}  // namespace detail

void CompositeControllerValidator::DeleteConflictingControlUnits(
    const IControlUnit* control_unit,
    std::vector<std::unique_ptr<IControlUnit>>& control_units,
    mantle_api::UniqueId unique_id)
{
    detail::DeleteControlUnit<&detail::IsLongitudinalControlUnit>(control_unit, control_units, unique_id);
    detail::DeleteControlUnit<&detail::IsRoutingControlUnit>(control_unit, control_units, unique_id);
    detail::DeleteControlUnit<&detail::IsLateralControlUnit>(control_unit, control_units, unique_id);
    detail::DeleteControlUnit<&detail::IsIndicatorControlUnit>(control_unit, control_units, unique_id);
}

void CompositeControllerValidator::InsertControlUnitInOrder(std::unique_ptr<IControlUnit> control_unit,
                                                            std::vector<std::unique_ptr<IControlUnit>>& control_units,
                                                            mantle_api::UniqueId unique_id)
{
    Info("Adding {} to composite controller #{}", control_unit->GetName(), unique_id);

    /// Necessary order of control units for stepping:
    /// 1. Update scalars
    /// 2. Update position
    /// 3. Recover position
    /// (4. lane assignment (done separately))
    if (dynamic_cast<RecoveryControlUnit*>(control_unit.get()) != nullptr)
    {
        control_units.push_back(std::move(control_unit));
    }
    else if (detail::IsRoutingControlUnit(control_unit.get()))
    {
        auto it = control_units.begin();
        for (; it != control_units.end(); it++)
        {
            if (dynamic_cast<RecoveryControlUnit*>((*it).get()) != nullptr)
            {
                break;
            }
        }
        control_units.insert(it, std::move(control_unit));
    }
    else
    {
        control_units.insert(control_units.begin(), std::move(control_unit));
    }
}

bool CompositeControllerValidator::NeedsPathControlUnit(std::vector<std::unique_ptr<IControlUnit>>& control_units)
{
    auto needs_path_control_unit = [](const auto& control_unit) {
        return !detail::IsRoutingControlUnit(control_unit.get()) &&
               dynamic_cast<controller::NoOpControlUnit*>(control_unit.get()) == nullptr &&
               dynamic_cast<controller::TrafficLightControlUnit*>(control_unit.get()) == nullptr;
    };
    return std::all_of(control_units.cbegin(), control_units.cend(), needs_path_control_unit);
}

}  // namespace gtgen::core::environment::controller
