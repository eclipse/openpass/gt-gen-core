/*******************************************************************************
 * Copyright (c) 2021-2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/composite_controller.h"

#include "Core/Environment/Controller/Internal/ControlUnits/path_control_unit.h"
#include "Core/Environment/Controller/composite_controller_validator.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Service/Profiling/profiling.h"

#include <algorithm>

namespace gtgen::core::environment::controller
{

using units::literals::operator""_s;

CompositeController::CompositeController(mantle_api::UniqueId id,
                                         mantle_api::ILaneLocationQueryService* lane_location_query_service,
                                         Type type)
    : id_{id}, lane_location_query_service_{lane_location_query_service}, type_{type}
{
}

std::unique_ptr<CompositeController> CompositeController::Clone() const
{
    GTGEN_PROFILE_SCOPE

    auto composite_controller_copy = std::make_unique<CompositeController>(id_, lane_location_query_service_, type_);
    composite_controller_copy->SetDefaultRoutingBehavior(default_routing_behavior_);

    for (auto& control_unit : control_units_)
    {
        composite_controller_copy->AddControlUnit(control_unit->Clone());
    }

    composite_controller_copy->is_active_ = is_active_;

    return composite_controller_copy;
}

mantle_api::UniqueId CompositeController::GetUniqueId() const
{
    return id_;
}

void CompositeController::SetName(const std::string& name)
{
    name_ = name;
}

const std::string& CompositeController::GetName() const
{
    return name_;
}

void CompositeController::SetEntity(mantle_api::IEntity& entity)
{
    entity_container_ = std::make_unique<gtgen::core::environment::entities::EntityContainer>(&entity);
    for (auto& control_unit : control_units_)
    {
        control_unit->SetEntity(*entity_container_);
    }
}

void CompositeController::AddControlUnit(std::unique_ptr<IControlUnit> control_unit)  // NOLINT(misc-no-recursion)
{
    control_unit->SetControllerDataExchangeContainer(controller_data_exchange_container_);
    if (entity_container_ != nullptr)
    {
        control_unit->SetEntity(*entity_container_);
    }

    CompositeControllerValidator::DeleteConflictingControlUnits(control_unit.get(), GetControlUnits(), GetUniqueId());
    CompositeControllerValidator::InsertControlUnitInOrder(std::move(control_unit), GetControlUnits(), GetUniqueId());

    if (CompositeControllerValidator::NeedsPathControlUnit(GetControlUnits()))
    {
        auto path_control_unit = std::make_unique<controller::PathControlUnit>(lane_location_query_service_);
        AddControlUnit(std::move(path_control_unit));
    }
}

void CompositeController::Step(mantle_api::Time current_simulation_time)
{
    GTGEN_PROFILE_SCOPE
    controller_data_exchange_container_.Reset();

    if (default_routing_behavior_ == mantle_api::DefaultRoutingBehavior::kRandomRoute)
    {
        ReplaceFinishedPathControlUnit();
    }

    // Update entity container state from scenario init
    if (current_simulation_time == 0_s)
    {
        entity_container_->SynchronizeEntityContainer();
    }

    for (auto& control_unit : control_units_)
    {
        control_unit->Step(current_simulation_time);
    }
}

bool CompositeController::HasFinished() const
{
    return std::all_of(control_units_.begin(), control_units_.end(), [](const auto& control_unit) {
        return control_unit->HasFinished();
    });
}

bool CompositeController::HasControlStrategyGoalBeenReached(std::uint64_t entity_id,
                                                            mantle_api::ControlStrategyType type) const
{
    for (const auto& control_unit : control_units_)
    {
        if (control_unit->GetOriginatingControlStrategy() == type)
        {
            return control_unit->HasFinished();
        }
    }

    throw EnvironmentException(
        "Cannot find control unit corresponding to control strategy {} for "
        "entity with id {}. Please check, if control strategy was assigned "
        "to entity and corresponding control unit was created.",
        type,
        entity_id);
}

CompositeController::Type CompositeController::GetType() const
{
    return type_;
}

void CompositeController::SetType(CompositeController::Type type)
{
    type_ = type;
}

void CompositeController::SetDefaultRoutingBehavior(mantle_api::DefaultRoutingBehavior default_routing_behavior)
{
    default_routing_behavior_ = default_routing_behavior;
}

void CompositeController::ReplaceFinishedPathControlUnit()
{
    for (auto& control_unit : control_units_)
    {
        if (auto path_control_unit = dynamic_cast<PathControlUnit*>(control_unit.get());
            path_control_unit != nullptr && control_unit->HasFinished() &&
            !path_control_unit->HasReachedEndOfRoadNetwork())
        {
            auto new_path_control_unit = std::make_unique<controller::PathControlUnit>(lane_location_query_service_);
            AddControlUnit(std::move(new_path_control_unit));
            break;
        }
    }
}

void CompositeController::ChangeState(mantle_api::IController::LateralState lateral_state,
                                      mantle_api::IController::LongitudinalState longitudinal_state)
{
    if (lateral_state == mantle_api::IController::LateralState::kNoChange &&
        longitudinal_state == mantle_api::IController::LongitudinalState::kNoChange)
    {
        return;
    }

    is_active_ = (lateral_state == mantle_api::IController::LateralState::kActivate ||
                  longitudinal_state == mantle_api::IController::LongitudinalState::kActivate);
}

bool CompositeController::IsActive() const
{
    return is_active_;
}

void CompositeController::UpdateEntity()
{
    entity_container_->UpdateEntity();
}

}  // namespace gtgen::core::environment::controller
