/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_COMPOSITECONTROLLERSUT_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_COMPOSITECONTROLLERSUT_H

#include "Core/Environment/Controller/composite_controller.h"

#include <MantleAPI/Common/i_identifiable.h>

namespace gtgen::core::environment::controller
{

class CompositeControllerSUT : public controller::CompositeController
{
  public:
    CompositeControllerSUT(mantle_api::UniqueId id, mantle_api::ILaneLocationQueryService* lane_location_query_service)
        : controller::CompositeController(id, lane_location_query_service)
    {
    }

    std::size_t GetControlUnitCount() const { return control_units_.size(); }
};
}  // namespace gtgen::core::environment::controller
#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_COMPOSITECONTROLLERSUT_H
