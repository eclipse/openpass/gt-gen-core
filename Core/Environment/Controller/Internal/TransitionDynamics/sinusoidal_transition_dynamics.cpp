/*******************************************************************************
 * Copyright (c) 2025, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Environment/Controller/Internal/TransitionDynamics/sinusoidal_transition_dynamics.h"

#include "Core/Service/Utility/math_utils.h"
namespace gtgen::core::environment::controller
{

// For the calculations we assume: S = start_val, T = target_val,  V = param_target_val, x = param_val;
// Transition is between (x0, S) and (V , T) in lateral direction

SinusoidalTransitionDynamics::SinusoidalTransitionDynamics(
    double start_val,
    double target_val,
    const mantle_api::TransitionDynamics& transition_dynamics_config)
    : IAbstractTransitionDynamics(start_val, target_val, transition_dynamics_config)
{
    if (transition_dynamics_config.shape != mantle_api::Shape::kSinusoidal)
    {
        throw std::runtime_error("Wrong TransitionDynamicsStrategy (i.e. SinusoidalTransitionDynamics) used.");
    }
}

double SinusoidalTransitionDynamics::Evaluate() const
{
    // f(x) = (T+S)/2 + ((S-T)/2)cos(xπ/V)
    double start_val = GetStartVal();
    double param_val = GetParamVal();
    double target_val = GetTargetVal();
    double param_target_val = GetParamTargetVal();
    double s_minus_t_by_2 = (start_val - target_val) * 0.5;
    double t_plus_s_by_2 = (target_val + start_val) * 0.5;
    double safe_param_target_val = service::utility::AvoidZero(param_target_val);
    double fx = (t_plus_s_by_2 + s_minus_t_by_2 * cos((param_val * M_PI) / safe_param_target_val));
    return fx;
}

double SinusoidalTransitionDynamics::EvaluateFirstDerivative() const
{
    // f'(x) =  ((S-T)π/2V)sin(xπ/V)
    double start_val = GetStartVal();
    double param_val = GetParamVal();
    double target_val = GetTargetVal();
    double param_target_val = GetParamTargetVal();
    double safe_param_target_val = service::utility::AvoidZero(param_target_val);
    double amplitude = (((start_val - target_val) * M_PI * 0.5) / safe_param_target_val);
    double firstDerivative = (amplitude * sin((param_val * M_PI) / safe_param_target_val));
    return firstDerivative;
}

double SinusoidalTransitionDynamics::CalculateTargetParameterValByRate(double rate) const
{
    // f'(x) =  ((S-T)π/2V)sin(xπ/V)
    // V will be valid for above function when x is at V/2
    // f'(V/2) = R =  ((S-T)π/2V)
    // V = ((S-T)π/2R)     ---- [E1]
    // rate sign is as per S-T, to not allow phase change
    double start_val = GetStartVal();
    double target_val = GetTargetVal();
    double s_minus_t = start_val - target_val;
    double effectiveRate = service::utility::GetSign(s_minus_t) * fabs(rate);
    double param_target_val = (s_minus_t * M_PI * 0.5) / (effectiveRate);
    return param_target_val;
}

double SinusoidalTransitionDynamics::CalculatePeakRate() const
{
    // peak rate is always at the x=V/2
    // from [E1]
    // R =  ((S-T)π/2V)
    double start_val = GetStartVal();
    double target_val = GetTargetVal();
    double param_target_val = GetParamTargetVal();
    double safe_param_target_val = service::utility::AvoidZero(param_target_val);
    double peak_rate = ((start_val - target_val) * M_PI * 0.5) / (safe_param_target_val);
    return peak_rate;
}

}  // namespace gtgen::core::environment::controller
