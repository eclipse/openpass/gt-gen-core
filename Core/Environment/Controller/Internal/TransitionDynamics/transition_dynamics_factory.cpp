/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2025, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/TransitionDynamics/transition_dynamics_factory.h"

#include "Core/Environment/Controller/Internal/TransitionDynamics/cubic_transition_dynamics.h"
#include "Core/Environment/Controller/Internal/TransitionDynamics/linear_transition_dynamics.h"
#include "Core/Environment/Controller/Internal/TransitionDynamics/sinusoidal_transition_dynamics.h"
#include "Core/Environment/Exception/exception.h"

namespace gtgen::core::environment::controller
{

std::unique_ptr<controller::ITransitionDynamics> TransitionDynamicsFactory::Create(
    double start_val,
    double target_val,
    const mantle_api::TransitionDynamics& transition_dynamics_config)
{
    switch (transition_dynamics_config.shape)
    {
        case mantle_api::Shape::kLinear:
            return std::make_unique<LinearTransitionDynamics>(start_val, target_val, transition_dynamics_config);

        case mantle_api::Shape::kCubic:
            return std::make_unique<CubicTransitionDynamics>(start_val, target_val, transition_dynamics_config);

        case mantle_api::Shape::kSinusoidal:
            return std::make_unique<SinusoidalTransitionDynamics>(start_val, target_val, transition_dynamics_config);

        case mantle_api::Shape::kStep:
            throw EnvironmentException("TransitionDynamics shape 'Step' is not supported yet.");

        case mantle_api::Shape::kUndefined:
        default:
            throw EnvironmentException("Undefined TransitionDynamics shape");
    }
}

std::unique_ptr<ITransitionDynamics> TransitionDynamicsFactory::Create(
    const mantle_api::TransitionDynamics& transition_dynamics_config)
{
    return TransitionDynamicsFactory::Create(0.0, 0.0, transition_dynamics_config);
}

}  // namespace gtgen::core::environment::controller
