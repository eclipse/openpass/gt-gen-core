/*******************************************************************************
 * Copyright (c) 2025, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Environment/Controller/Internal/TransitionDynamics/sinusoidal_transition_dynamics.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <cmath>

namespace gtgen::core::environment::controller
{

class SinusoidalTransitionDynamicsTest : public testing::Test
{
  protected:
    const double given_start_val_{1.2};
    const double given_target_val_{3.4};
    const double given_transition_dynamics_field_value_{1.1};

    mantle_api::TransitionDynamics transition_dynamics_{};
};

TEST_F(SinusoidalTransitionDynamicsTest, GivenSinusoidalTransitionDynamics_WhenWrongShape_ThenRuntimeError)
{
    transition_dynamics_.shape = mantle_api::Shape::kUndefined;
    EXPECT_THROW(SinusoidalTransitionDynamics transition(given_start_val_, given_target_val_, transition_dynamics_),
                 std::runtime_error);
}

TEST_F(SinusoidalTransitionDynamicsTest,
       GivenSinusoidalTransitionDynamicsWithDimensionNotRate_WhenCalculatePeakRate_ThenReturnCorrectValue)
{
    transition_dynamics_.shape = mantle_api::Shape::kSinusoidal;
    transition_dynamics_.value = given_transition_dynamics_field_value_;
    transition_dynamics_.dimension = mantle_api::Dimension::kTime;

    // R =  ((S-T)π/2V)
    const auto expected_rate =
        ((given_start_val_ - given_target_val_) * M_PI) / (2.0 * given_transition_dynamics_field_value_);

    SinusoidalTransitionDynamics transition(given_start_val_, given_target_val_, transition_dynamics_);
    transition.Init();
    EXPECT_DOUBLE_EQ(expected_rate, transition.CalculatePeakRate());
}

TEST_F(SinusoidalTransitionDynamicsTest,
       GivenSinusoidalTransitionDynamicsWithDimensionRate_WhenCalculateTargetParameterValByRate_ThenReturnCorrectValue)
{

    transition_dynamics_.shape = mantle_api::Shape::kSinusoidal;
    transition_dynamics_.dimension = mantle_api::Dimension::kRate;
    transition_dynamics_.value = given_transition_dynamics_field_value_;

    // V = ((S-T)π/2R)
    auto S_minus_T = given_start_val_ - given_target_val_;
    auto effective_rate = (std::signbit(S_minus_T)) ? (-1.0 * given_transition_dynamics_field_value_)
                                                    : given_transition_dynamics_field_value_;
    const auto expected_parameter_target_value = ((S_minus_T)*M_PI) / (2.0 * effective_rate);

    SinusoidalTransitionDynamics transition(given_start_val_, given_target_val_, transition_dynamics_);
    transition.Init();

    EXPECT_DOUBLE_EQ(expected_parameter_target_value,
                     transition.CalculateTargetParameterValByRate(given_transition_dynamics_field_value_));
}

TEST_F(SinusoidalTransitionDynamicsTest,
       GivenSinusoidalTransitionDynamicsWithDimensionNotRate_WhenCallEvaluateBeforeStep_ThenReturnStartValue)
{
    // This test case is identical when the dimension is distance
    transition_dynamics_.shape = mantle_api::Shape::kSinusoidal;
    transition_dynamics_.dimension = mantle_api::Dimension::kTime;
    transition_dynamics_.value = given_transition_dynamics_field_value_;

    SinusoidalTransitionDynamics transition(given_start_val_, given_target_val_, transition_dynamics_);
    transition.Init();

    EXPECT_DOUBLE_EQ(given_start_val_, transition.Evaluate());
}

TEST_F(SinusoidalTransitionDynamicsTest,
       GivenSinusoidalTransitionDynamicsWithDimensionNotRate_WhenCallEvaluateReachedTarget_ThenReturnTargetValue)
{
    // This test case is identical when the dimension is distance

    transition_dynamics_.shape = mantle_api::Shape::kSinusoidal;
    transition_dynamics_.dimension = mantle_api::Dimension::kTime;
    transition_dynamics_.value = given_transition_dynamics_field_value_;
    auto fake_elapsed_time = given_transition_dynamics_field_value_;

    SinusoidalTransitionDynamics transition(given_start_val_, given_target_val_, transition_dynamics_);
    transition.Init();
    transition.Step(fake_elapsed_time);

    EXPECT_DOUBLE_EQ(given_target_val_, transition.Evaluate());
}

TEST_F(
    SinusoidalTransitionDynamicsTest,
    GivenSinusoidalTransitionDynamicsWithDimensionNotRate_WhenCallEvaluateInTheMiddleOfTransition_ThenReturnCorrectValue)
{
    // This test case is identical when the dimension is distance

    transition_dynamics_.shape = mantle_api::Shape::kSinusoidal;
    transition_dynamics_.dimension = mantle_api::Dimension::kTime;
    transition_dynamics_.value = given_transition_dynamics_field_value_;
    auto fake_elapsed_time = given_transition_dynamics_field_value_ / 2.0;

    SinusoidalTransitionDynamics transition(given_start_val_, given_target_val_, transition_dynamics_);
    transition.Init();
    transition.Step(fake_elapsed_time);

    const auto expected_value = given_start_val_ + (given_target_val_ - given_start_val_) / 2.0;
    EXPECT_DOUBLE_EQ(expected_value, transition.Evaluate());
}

TEST_F(SinusoidalTransitionDynamicsTest,
       GivenSinusoidalTransitionDynamicsWithDimensionRate_WhenCallEvaluateBeforeStep_ThenReturnStartValue)
{
    transition_dynamics_.shape = mantle_api::Shape::kSinusoidal;
    transition_dynamics_.dimension = mantle_api::Dimension::kRate;
    transition_dynamics_.value = given_transition_dynamics_field_value_;

    SinusoidalTransitionDynamics transition(given_start_val_, given_target_val_, transition_dynamics_);
    transition.Init();

    EXPECT_DOUBLE_EQ(given_start_val_, transition.Evaluate());
}

TEST_F(SinusoidalTransitionDynamicsTest,
       GivenSinusoidalTransitionDynamicsWithDimensionRate_WhenCallEvaluateReachedTarget_ThenReturnTargetValue)
{
    // V = ((S-T)π/2R)
    auto S_minus_T = given_start_val_ - given_target_val_;
    auto effective_rate = (std::signbit(S_minus_T)) ? (-1.0 * given_transition_dynamics_field_value_)
                                                    : given_transition_dynamics_field_value_;
    const double parameter_target_value = (S_minus_T * M_PI) / (2.0 * effective_rate);
    transition_dynamics_.shape = mantle_api::Shape::kSinusoidal;
    transition_dynamics_.dimension = mantle_api::Dimension::kRate;
    transition_dynamics_.value = given_transition_dynamics_field_value_;

    SinusoidalTransitionDynamics transition(given_start_val_, given_target_val_, transition_dynamics_);
    transition.Init();
    transition.Step(parameter_target_value);

    EXPECT_DOUBLE_EQ(given_target_val_, transition.Evaluate());
}

TEST_F(
    SinusoidalTransitionDynamicsTest,
    GivenSinusoidalTransitionDynamicsWithDimensionRate_WhenCallEvaluateInTheMiddleOfTransition_ThenReturnCorrectValue)
{
    // V = ((S-T)π/2R)
    auto S_minus_T = given_start_val_ - given_target_val_;
    auto effective_rate = (std::signbit(S_minus_T)) ? (-1.0 * given_transition_dynamics_field_value_)
                                                    : given_transition_dynamics_field_value_;
    const double parameter_target_value = (S_minus_T * M_PI) / (2.0 * effective_rate);

    transition_dynamics_.shape = mantle_api::Shape::kSinusoidal;
    transition_dynamics_.dimension = mantle_api::Dimension::kRate;
    transition_dynamics_.value = given_transition_dynamics_field_value_;

    SinusoidalTransitionDynamics transition(given_start_val_, given_target_val_, transition_dynamics_);
    transition.Init();
    transition.Step(parameter_target_value / 2.0);

    const auto expected_value = given_start_val_ + (given_target_val_ - given_start_val_) / 2.0;
    EXPECT_DOUBLE_EQ(expected_value, transition.Evaluate());
}

TEST_F(SinusoidalTransitionDynamicsTest,
       GivenSinusoidalTransitionDynamicsWithDimensionRate_WhenEvaluateFirstDerivative_ThenReturnCorrectValue)
{
    transition_dynamics_.shape = mantle_api::Shape::kSinusoidal;
    transition_dynamics_.dimension = mantle_api::Dimension::kRate;
    transition_dynamics_.value = given_transition_dynamics_field_value_;
    auto delta = 0.1;
    // V = ((S-T)π/2R)
    auto S_minus_T = given_start_val_ - given_target_val_;
    auto effective_rate = (std::signbit(S_minus_T)) ? (-1.0 * given_transition_dynamics_field_value_)
                                                    : given_transition_dynamics_field_value_;
    const double param_val = (S_minus_T * M_PI) / (2.0 * effective_rate);
    SinusoidalTransitionDynamics transition(given_start_val_, given_target_val_, transition_dynamics_);
    transition.Init();
    EXPECT_DOUBLE_EQ(0.0, transition.EvaluateFirstDerivative());
    // rate peaks at middle and is symmetric around peak
    transition.Step(param_val / 2.0 - delta);
    auto maximum_minus_delta = fabs(transition.EvaluateFirstDerivative());
    transition.Step(delta);
    auto maximum = fabs(transition.EvaluateFirstDerivative());
    transition.Step(delta);
    auto maximum_plus_delta = fabs(transition.EvaluateFirstDerivative());
    EXPECT_GT(maximum, maximum_minus_delta);
    EXPECT_GT(maximum, maximum_plus_delta);
    EXPECT_DOUBLE_EQ(maximum_minus_delta, maximum_plus_delta);
    transition.Step(param_val / 2.0 - delta);
    EXPECT_NEAR(0.0, transition.EvaluateFirstDerivative(), 1e-3);
}

TEST_F(SinusoidalTransitionDynamicsTest,
       GivenSinusoidalTransitionDynamicsWithDimensionTime_WhenEvaluateFirstDerivative_ThenReturnCorrectValue)
{
    // This test case is identical when the dimension is distance
    transition_dynamics_.shape = mantle_api::Shape::kSinusoidal;
    transition_dynamics_.dimension = mantle_api::Dimension::kTime;
    transition_dynamics_.value = given_transition_dynamics_field_value_;
    auto delta = 0.1;

    SinusoidalTransitionDynamics transition(given_start_val_, given_target_val_, transition_dynamics_);
    transition.Init();
    EXPECT_DOUBLE_EQ(0.0, transition.EvaluateFirstDerivative());
    // rate peaks at middle and is symmetric around peak
    transition.Step((given_transition_dynamics_field_value_ / 2.0) - delta);
    auto maximum_minus_delta = fabs(transition.EvaluateFirstDerivative());
    transition.Step(delta);
    auto maximum = fabs(transition.EvaluateFirstDerivative());
    transition.Step(delta);
    auto maximum_plus_delta = fabs(transition.EvaluateFirstDerivative());
    EXPECT_GT(maximum, maximum_minus_delta);
    EXPECT_GT(maximum, maximum_plus_delta);
    EXPECT_DOUBLE_EQ(maximum_minus_delta, maximum_plus_delta);
    transition.Step(given_transition_dynamics_field_value_ / 2.0 - delta);
    EXPECT_NEAR(0.0, transition.EvaluateFirstDerivative(), 1e-3);
}

}  // namespace gtgen::core::environment::controller
