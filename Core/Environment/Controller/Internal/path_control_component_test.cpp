/*******************************************************************************
 * Copyright (c) 2021-2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/composite_controller.h"
#include "Core/Environment/Controller/controller_factory.h"
#include "Core/Environment/Entities/vehicle_entity.h"
#include "Core/Environment/Map/GtGenMap/gtgen_map.h"
#include "Core/Service/GlmWrapper/glm_wrapper.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"
#include "Core/Tests/TestUtils/expect_extensions.h"
#include "Core/Tests/TestUtils/minimal_point_distance_to_polyline.h"

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/spline.h>
#include <MantleAPI/Traffic/i_controller_config.h>
#include <gtest/gtest.h>

#include <memory>
#include <vector>

namespace gtgen::core::environment::controller
{
using units::literals::operator""_m;
using units::literals::operator""_mps_pow4;
using units::literals::operator""_mps_cb;
using units::literals::operator""_mps_sq;
using units::literals::operator""_mps;
using units::literals::operator""_rad;

class PathControlWithVelocityComponentTest : public testing::Test
{
  public:
    PathControlWithVelocityComponentTest()
    {
        center_line_left_ = std::move(center_line_nw_);
        center_line_left_.insert(center_line_left_.end(),
                                 std::make_move_iterator(center_line_sw_.begin()),
                                 std::make_move_iterator(center_line_sw_.end()));
        start_position_ = center_line_left_.front();
    }

    CompositeController SetupCompositeControllerWithAcceleratingVelocityAndRouteControl()
    {
        std::vector<mantle_api::SplineSection<units::velocity::meters_per_second>> velocity_splines{
            mantle_api::SplineSection<units::velocity::meters_per_second>{
                mantle_api::Time{0}, mantle_api::Time{2'000}, {0_mps_pow4, 0_mps_cb, 2.5_mps_sq, 0_mps}},
            mantle_api::SplineSection<units::velocity::meters_per_second>{
                mantle_api::Time{2'000}, mantle_api::Time{3'000}, {0_mps_pow4, 0_mps_cb, 0_mps_sq, 5_mps}}};

        auto waypoints = {start_position_, target_waypoint_};

        CompositeController composite_controller(0, &lane_location_provider_);
        composite_controller.AddControlUnit(
            std::make_unique<controller::VelocityControlUnit>(velocity_splines, default_velocity_));
        composite_controller.AddControlUnit(
            std::make_unique<controller::PathControlUnit>(&lane_location_provider_, waypoints));

        return composite_controller;
    }

  protected:
    std::unique_ptr<map::GtGenMap> gtgen_map_{test_utils::MapCatalogue::CircleClosedLoopMapWithFourLaneGroups()};
    map::LaneLocationProvider lane_location_provider_{*gtgen_map_};
    units::velocity::meters_per_second_t default_velocity_{6.0_mps};
    units::length::meter_t max_distance_from_centerline_{0.015_m};

    // See lanes 13 and 23 from CircleClosedLoopMapWithFourLaneGroups diagram
    std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_nw_ = gtgen_map_->GetLanes().at(3).center_line;
    std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_sw_ = gtgen_map_->GetLanes().at(6).center_line;
    std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_left_{};

    mantle_api::Vec3<units::length::meter_t> start_position_{};
    mantle_api::Vec3<units::length::meter_t> target_waypoint_{-0.026853636_m,
                                                              -97.99956965_m,
                                                              0_m};  // shortly before last waypoint
};

TEST_F(PathControlWithVelocityComponentTest,
       GivenVelocityAndRouteControlAndEntityOnCurvedRoad_WhenStepping_ThenEntityVelocityAndAccelerationCorrect)
{
    auto entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

    auto composite_controller = SetupCompositeControllerWithAcceleratingVelocityAndRouteControl();
    composite_controller.SetEntity(*entity);

    std::int64_t step = 0;
    std::int64_t step_size = 1;
    double velocity_change_per_step = 0.0025;
    double starting_velocity = 0.0;
    for (; step < 2000; step += step_size)
    {
        // Constant velocity increase and no change to acceleration
        composite_controller.Step(mantle_api::Time{static_cast<double>(step)});
        composite_controller.UpdateEntity();

        double expected_velocity = starting_velocity + (velocity_change_per_step * static_cast<double>(step));

        ASSERT_DOUBLE_EQ(expected_velocity, entity->GetVelocity().Length()());
        ASSERT_DOUBLE_EQ(2.5, entity->GetAcceleration().Length()());
    }

    for (; step <= 3000; step += step_size)
    {
        // Constant velocity and no acceleration
        composite_controller.Step(mantle_api::Time{static_cast<double>(step)});
        composite_controller.UpdateEntity();
        ASSERT_DOUBLE_EQ(5.0, entity->GetVelocity().Length()());
        ASSERT_DOUBLE_EQ(0.0, entity->GetAcceleration().Length()());
    }
}

TEST_F(PathControlWithVelocityComponentTest,
       GivenVelocityAndRouteControlAndEntityOnCurvedRoad_WhenStepping_ThenEntityFollowsCenterLine)
{
    auto entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());
    entity->SetPosition(start_position_);

    auto composite_controller = SetupCompositeControllerWithAcceleratingVelocityAndRouteControl();
    composite_controller.SetEntity(*entity);

    std::int64_t step = 0;
    std::int64_t step_size = 100;

    mantle_api::Vec3<units::length::meter_t> previous_position = entity->GetPosition();

    for (; step < 2000; step += step_size)
    {
        composite_controller.Step(mantle_api::Time{static_cast<double>(step)});
        composite_controller.UpdateEntity();
        mantle_api::Vec3<units::length::meter_t> actual_position = entity->GetPosition();
        EXPECT_NEAR(0,
                    test_utils::MinimalPointDistanceToPolyline(center_line_left_, actual_position),
                    max_distance_from_centerline_());
        EXPECT_NEAR(service::glmwrapper::Distance(previous_position, actual_position),
                    entity->GetVelocity().Length()() * (static_cast<double>(step_size) / 1000.0),
                    0.1);

        previous_position = actual_position;
    }

    // From here onwards velocity is constant.
    for (; step <= 3000; step += step_size)
    {
        composite_controller.Step(mantle_api::Time{static_cast<double>(step)});
        composite_controller.UpdateEntity();
        mantle_api::Vec3<units::length::meter_t> actual_position = entity->GetPosition();
        EXPECT_NEAR(0,
                    test_utils::MinimalPointDistanceToPolyline(center_line_left_, actual_position),
                    max_distance_from_centerline_());
        EXPECT_NEAR(service::glmwrapper::Distance(previous_position, actual_position),
                    entity->GetVelocity().Length()() * (static_cast<double>(step_size) / 1000.0),
                    0.1);

        previous_position = actual_position;
    }
}

TEST_F(PathControlWithVelocityComponentTest, GivenRoute_WhenTargetNotYetReached_ThenMoveAlongCenterLine)
{
    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());
    entity->SetPosition(start_position_);

    auto composite_controller = SetupCompositeControllerWithAcceleratingVelocityAndRouteControl();
    composite_controller.SetEntity(*entity);

    // Initial step
    composite_controller.Step(mantle_api::Time{0});
    composite_controller.UpdateEntity();
    EXPECT_DOUBLE_EQ(0.0, entity->GetVelocity().Length()());

    std::int64_t delta_time{10};
    while (!composite_controller.HasFinished())
    {
        composite_controller.Step(mantle_api::Time{static_cast<double>(3000 + delta_time)});
        composite_controller.UpdateEntity();
        EXPECT_NEAR(0,
                    test_utils::MinimalPointDistanceToPolyline(center_line_left_, entity->GetPosition()),
                    max_distance_from_centerline_());
        EXPECT_DOUBLE_EQ(default_velocity_(), entity->GetVelocity().Length()());
        delta_time += delta_time;
    }

    EXPECT_TRIPLE(target_waypoint_, entity->GetPosition());
}

TEST_F(PathControlWithVelocityComponentTest,
       GivenVelocitySplineProcessed_WhenTargetPositionNotYetReached_ThenContinueWithDefaultVelocity)
{
    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

    auto composite_controller = SetupCompositeControllerWithAcceleratingVelocityAndRouteControl();
    composite_controller.SetEntity(*entity);

    // Initial step
    composite_controller.Step(mantle_api::Time{0});
    composite_controller.UpdateEntity();
    EXPECT_DOUBLE_EQ(0.0, entity->GetVelocity().Length()());

    std::int64_t delta_time{10};
    while (!composite_controller.HasFinished())
    {
        composite_controller.Step(mantle_api::Time{static_cast<double>(3000 + delta_time)});
        composite_controller.UpdateEntity();
        EXPECT_DOUBLE_EQ(default_velocity_(), entity->GetVelocity().Length()());
        delta_time += delta_time;
    }
}

TEST_F(PathControlWithVelocityComponentTest, GivenZeroVelocity_WhenOrientationModifiedFromOutside_ThenOrientationIsKept)
{
    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

    CompositeController composite_controller(0, &lane_location_provider_);
    composite_controller.AddControlUnit(std::make_unique<controller::KeepVelocityControlUnit>());
    auto waypoints = {start_position_, target_waypoint_};
    composite_controller.AddControlUnit(
        std::make_unique<controller::PathControlUnit>(&lane_location_provider_, waypoints));
    composite_controller.SetEntity(*entity);

    // Initial step
    composite_controller.Step(mantle_api::Time{0});
    composite_controller.UpdateEntity();
    EXPECT_DOUBLE_EQ(0.0, entity->GetOrientation().yaw());

    entity->SetOrientation({1_rad, 0_rad, 0_rad});
    composite_controller.Step(mantle_api::Time{10});
    composite_controller.UpdateEntity();
    EXPECT_DOUBLE_EQ(1.0, entity->GetOrientation().yaw());
}

class PathControlWithVelocityAndLaneOffsetComponentTest : public testing::Test
{
  public:
    CompositeController SetupCompositeControllerWithIncreasingOffsetAndRouteControl()
    {
        std::vector<mantle_api::SplineSection<units::velocity::meters_per_second>> velocity_splines{};

        mantle_api::SplineSection<units::length::meter> t_offset_profile{
            mantle_api::Time{0'000}, mantle_api::Time{2'000}, {0_mps_cb, 0_mps_sq, 1_mps, 0_m}};
        const std::vector<mantle_api::SplineSection<units::length::meter>> lateral_offsets{t_offset_profile};

        CompositeController composite_controller(0, &lane_location_provider_);
        composite_controller.AddControlUnit(
            std::make_unique<controller::VelocityControlUnit>(velocity_splines, default_velocity_));
        composite_controller.AddControlUnit(std::make_unique<controller::LaneOffsetControlUnit>(lateral_offsets));
        composite_controller.AddControlUnit(std::make_unique<controller::PathControlUnit>(&lane_location_provider_));

        return composite_controller;
    }

  protected:
    std::unique_ptr<map::GtGenMap> gtgen_map_{
        test_utils::MapCatalogue::MapWithThreeConnectedEastingLanesWithHundredPointsEach()};
    map::LaneLocationProvider lane_location_provider_{*gtgen_map_};
    units::velocity::meters_per_second_t default_velocity_{10.0_mps};
};

TEST_F(PathControlWithVelocityAndLaneOffsetComponentTest,
       GivenLinearOffsetProfile_WhenControllerIsStepped_ThenOrientationDoesNotChange)
{
    using units::literals::operator""_rad;
    using units::literals::operator""_rad_per_s;
    using units::literals::operator""_rad_per_s_sq;
    using units::literals::operator""_s;

    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

    auto composite_controller = SetupCompositeControllerWithIncreasingOffsetAndRouteControl();
    composite_controller.SetEntity(*entity);

    const mantle_api::Orientation3<units::angle::radian_t> expected_orientation_initial_step = {0_rad, 0_rad, 0_rad};
    const units::angle::radian_t constant_orientation{0.10016742};
    const mantle_api::Orientation3<units::angle::radian_t> expected_orientation = {constant_orientation, 0_rad, 0_rad};

    composite_controller.Step(mantle_api::Time{0});
    composite_controller.UpdateEntity();
    EXPECT_TRIPLE(expected_orientation_initial_step, entity->GetOrientation());
    EXPECT_TRIPLE((mantle_api::Orientation3<units::angular_velocity::radians_per_second_t>{
                      0_rad_per_s, 0_rad_per_s, 0_rad_per_s}),
                  entity->GetOrientationRate());
    EXPECT_TRIPLE((mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t>{
                      0_rad_per_s_sq, 0_rad_per_s_sq, 0_rad_per_s_sq}),
                  entity->GetOrientationAcceleration());

    composite_controller.Step(mantle_api::Time{1});
    composite_controller.UpdateEntity();
    EXPECT_TRIPLE(expected_orientation, entity->GetOrientation());
    EXPECT_TRIPLE_NEAR((mantle_api::Orientation3<units::angular_velocity::radians_per_second_t>{
                           constant_orientation / 0.001_s, 0_rad_per_s, 0_rad_per_s}),
                       entity->GetOrientationRate(),
                       0.001);
    EXPECT_TRIPLE_NEAR((mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t>{
                           constant_orientation / (0.000001_s * 1_s), 0_rad_per_s_sq, 0_rad_per_s_sq}),
                       entity->GetOrientationAcceleration(),
                       0.01);
    composite_controller.Step(mantle_api::Time{1000});
    composite_controller.UpdateEntity();
    EXPECT_TRIPLE(expected_orientation, entity->GetOrientation());
    EXPECT_TRIPLE((mantle_api::Orientation3<units::angular_velocity::radians_per_second_t>{
                      0_rad_per_s, 0_rad_per_s, 0_rad_per_s}),
                  entity->GetOrientationRate());
    EXPECT_TRIPLE_NEAR(mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t>(
                           (-constant_orientation / 0.001_s) / 0.999_s, 0_rad_per_s_sq, 0_rad_per_s_sq),
                       entity->GetOrientationAcceleration(),
                       0.001);

    composite_controller.Step(mantle_api::Time{2000});
    composite_controller.UpdateEntity();
    EXPECT_TRIPLE(expected_orientation, entity->GetOrientation());
    EXPECT_TRIPLE((mantle_api::Orientation3<units::angular_velocity::radians_per_second_t>{
                      0_rad_per_s, 0_rad_per_s, 0_rad_per_s}),
                  entity->GetOrientationRate());
    EXPECT_TRIPLE((mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t>{
                      0_rad_per_s_sq, 0_rad_per_s_sq, 0_rad_per_s_sq}),
                  entity->GetOrientationAcceleration());
}

}  // namespace gtgen::core::environment::controller
