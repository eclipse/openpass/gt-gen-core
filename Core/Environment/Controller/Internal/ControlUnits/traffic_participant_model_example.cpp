/*******************************************************************************
 * Copyright (c) 2023-2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/traffic_participant_model_example.h"

#include "Core/Service/Logging/logging.h"

#include <google/protobuf/stubs/common.h>

namespace gtgen::core::environment::controller
{

namespace detail
{

class StaticDeinit
{
  public:
    ~StaticDeinit() { google::protobuf::ShutdownProtobufLibrary(); }
};

const StaticDeinit kStaticDeinit;

}  // namespace detail

using osi_traffic_participant::ITrafficParticipantModel;

TpmExample::~TpmExample() = default;

std::shared_ptr<ITrafficParticipantModel> TpmExample::Create(
    [[maybe_unused]] uint64_t entity_id,
    [[maybe_unused]] const osi3::GroundTruth& ground_truth_init,
    [[maybe_unused]] const std::map<std::string, std::string>& parameters)
{
    return std::make_shared<TpmExample>();
}

std::optional<osi3::SensorViewConfiguration> TpmExample::GetSensorViewConfigurationRequest() const
{
    return std::nullopt;
}

void TpmExample::SetSensorViewConfiguration([[maybe_unused]] osi3::SensorViewConfiguration sensor_view_config)
{
    return;
}

osi_traffic_participant::TrafficParticipantUpdateResult TpmExample::Update(
    [[maybe_unused]] std::chrono::milliseconds delta_time,
    const osi3::SensorView& sensor_view,
    [[maybe_unused]] const std::optional<osi3::TrafficCommand>& traffic_command)
{
    auto& ground_truth = sensor_view.global_ground_truth();

    ASSERT(ground_truth.moving_object_size() > 0 &&
           "TpmExample: received groundtruth does not contain any moving objects.");

    auto& original_object = ground_truth.moving_object(0);

    osi3::TrafficUpdate traffic_update;

    auto* internal_state = traffic_update.add_internal_state();

    internal_state->mutable_host_vehicle_id()->CopyFrom(sensor_view.host_vehicle_id());

    auto* moving_object = traffic_update.add_update();

    moving_object->CopyFrom(original_object);

    auto* base = moving_object->mutable_base();
    base->mutable_position()->set_x(123.45);

    const double steering_wheel_angle{3.14};
    moving_object->mutable_vehicle_attributes()->set_steering_wheel_angle(steering_wheel_angle);
    internal_state->mutable_vehicle_steering()->mutable_vehicle_steering_wheel()->set_angle(steering_wheel_angle);

    return {traffic_update, osi3::TrafficCommandUpdate{}};
}

}  // namespace gtgen::core::environment::controller
