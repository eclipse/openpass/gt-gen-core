/*******************************************************************************
 * Copyright (c) 2021-2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_TRAFFICPARTICIPANTCONTROLUNIT_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_TRAFFICPARTICIPANTCONTROLUNIT_H

#include "Core/Environment/Controller/Internal/ControlUnits/i_abstract_control_unit.h"
#include "Core/Environment/Controller/Internal/Utilities/PlausibilityCheck/plausibility_check.h"
#include "Core/Environment/Controller/external_controller_config.h"
#include "Core/Environment/GroundTruth/sensor_view_builder.h"
#include "Core/Service/FileSystem/filesystem.h"
#include "osi_trafficupdate.pb.h"

#include <OsiTrafficParticipant/i_traffic_participant_model.h>
#include <boost/dll/shared_library.hpp>
#include <boost/function.hpp>

#include <map>
#include <memory>
#include <string>
#include <vector>

namespace osi3
{
class GroundTruth;
class MovingObject;
}  // namespace osi3

namespace gtgen::core::environment::controller
{

using osi_traffic_participant::ITrafficParticipantModel;

class TrafficParticipantControlUnit : public IAbstractControlUnit
{
  public:
    TrafficParticipantControlUnit(const std::map<std::string, std::string>& parameters,
                                  const TrafficParticipantControlUnitConfig& traffic_participant_control_unit_config);

    TrafficParticipantControlUnit(const std::map<std::string, std::string>& parameters,
                                  const TrafficParticipantControlUnitConfig& traffic_participant_control_unit_config,
                                  const boost::function<osi_traffic_participant::Create> tpm_creator);

    TrafficParticipantControlUnit(TrafficParticipantControlUnit const& traffic_participant_control_unit);

    TrafficParticipantControlUnit() = delete;
    TrafficParticipantControlUnit& operator=(const TrafficParticipantControlUnit&) = delete;
    TrafficParticipantControlUnit(TrafficParticipantControlUnit&&) = delete;
    TrafficParticipantControlUnit& operator=(TrafficParticipantControlUnit&&) = delete;

    ~TrafficParticipantControlUnit() override;

    std::unique_ptr<IControlUnit> Clone() const override;

    void SetEntity(mantle_api::IEntity& entity) override;

    void StepControlUnit() override;

    bool HasFinished() const override;

    fs::path FindPluginPath() const;

  private:
    void UpdateEntity(const osi3::TrafficUpdate& traffic_update);
    void UpdateBaseEntityProperties(const osi3::MovingObject& moving_object);
    void UpdateVehicleEntityProperties(const osi3::MovingObject& moving_object);

    static fs::path ResolvePluginsPath(fs::path& input_path, const std::vector<std::string>& search_directories);
    static fs::path FindPluginPath(const TrafficParticipantControlUnitConfig& traffic_participant_control_unit_config);

    static boost::function<osi_traffic_participant::Create> ImportTrafficParticipantCreator(
        const TrafficParticipantControlUnitConfig& traffic_participant_control_unit_config);

    std::map<std::string, std::string> parameters_;
    TrafficParticipantControlUnitConfig traffic_participant_control_unit_config_;
    boost::function<osi_traffic_participant::Create> tpm_creator_;

    std::shared_ptr<ITrafficParticipantModel> tpm_{nullptr};
    std::unique_ptr<proto_groundtruth::SensorViewBuilder> sensor_view_builder_{nullptr};

    plausibility_check::PlausibilityCheck plausibility_check_{};
};

}  // namespace gtgen::core::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_TRAFFICPARTICIPANTCONTROLUNIT_H
