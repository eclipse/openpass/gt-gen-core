/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/traffic_participant_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/traffic_participant_control_unit_base_fixture.h"
#include "Core/Service/FileSystem/file_system_utils.h"
#include "Core/Tests/TestUtils/ProtoUtils/proto_utilities.h"

#include <fmt/format.h>

namespace gtgen::core::environment::control_unit::test
{

class TrafficParticipantControlUnitFixture : public TrafficParticipantControlUnitBaseFixture
{
  protected:
    static void SetUpTestSuite()
    {
        const fs::path artifactory_path{fs::current_path() / artifactory_path_prefix};
        const fs::path orig_so_file{artifactory_path / fmt::format("{}.so", tpm_orig_library_name)};
        const fs::path so_file{artifactory_path / fmt::format("{}.so", tpm_library_name)};

        ASSERT_TRUE(fs::exists(orig_so_file));
        if (!fs::exists(so_file))
        {
            fs::copy_file(orig_so_file, so_file);
        }
        ASSERT_TRUE(fs::exists(so_file));
    }

    static constexpr std::string_view artifactory_path_prefix{"Core/Environment/Controller/Internal/ControlUnits"};
    static constexpr std::string_view tpm_orig_library_name{"traffic_participant_model_example"};
};

TEST_F(TrafficParticipantControlUnitFixture, GivenPathToLibrary_WhenFindPluginPath_ThenFoundExpectedPath)
{
    controller::TrafficParticipantControlUnit control_unit{tpm_params_, CreateTrafficParticipantControlConfig()};

    const fs::path plugin_path{control_unit.FindPluginPath()};
    const fs::path relative_plugin_path = service::file_system::Relative(plugin_path, fs::current_path());

    const fs::path expected_relative_path{fs::path{artifactory_path_prefix} / fmt::format("{}.so", tpm_library_name)};

    EXPECT_EQ(relative_plugin_path, expected_relative_path);
}

TEST_F(TrafficParticipantControlUnitFixture,
       GivenVehicleInGroundTruthAndEntityIsSet_WhenStepControlUnit_ThenNoThrownException)
{
    auto entity = CreateTpmEntity(1);
    controller::TrafficParticipantControlUnit control_unit{tpm_params_, CreateTrafficParticipantControlConfig()};

    control_unit.SetEntity(*entity);

    EXPECT_NO_THROW(control_unit.Step(mantle_api::Time{30}));
    EXPECT_NO_THROW(control_unit.Step(mantle_api::Time{60}));
}

}  // namespace gtgen::core::environment::control_unit::test
