/*******************************************************************************
 * Copyright (c) 2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/indicator_control_unit.h"

#include "Core/Environment/Entities/vehicle_entity.h"

#include <MantleAPI/Traffic/control_strategy.h>
#include <MantleAPI/Traffic/entity_properties.h>

namespace gtgen::core::environment::controller
{
IndicatorControlUnit::IndicatorControlUnit(const mantle_api::VehicleLightStatesControlStrategy& control_strategy)
    : control_strategy_{control_strategy}
{
}

std::unique_ptr<IControlUnit> IndicatorControlUnit::Clone() const
{
    return std::make_unique<IndicatorControlUnit>(control_strategy_);
}

void IndicatorControlUnit::StepControlUnit()
{
    entities::VehicleEntity* vehicle_entity = entities::CastEntityTypeTo<entities::VehicleEntity>(entity_);

    mantle_api::VehicleLightType light_type = std::get<mantle_api::VehicleLightType>(control_strategy_.light_type);

    if (light_type == mantle_api::VehicleLightType::kIndicatorLeft &&
        control_strategy_.light_state.light_mode == mantle_api::LightMode::kOn)
    {
        vehicle_entity->SetIndicatorState(mantle_api::IndicatorState::kLeft);
    }
    else if (light_type == mantle_api::VehicleLightType::kIndicatorRight &&
             control_strategy_.light_state.light_mode == mantle_api::LightMode::kOn)
    {
        vehicle_entity->SetIndicatorState(mantle_api::IndicatorState::kRight);
    }
    else if (control_strategy_.light_state.light_mode == mantle_api::LightMode::kOff)
    {
        vehicle_entity->SetIndicatorState(mantle_api::IndicatorState::kOff);
    }
}

bool IndicatorControlUnit::HasFinished() const
{
    return true;
}

}  // namespace gtgen::core::environment::controller
