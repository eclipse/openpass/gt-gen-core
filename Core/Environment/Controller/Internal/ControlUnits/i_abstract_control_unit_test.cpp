/*******************************************************************************
 * Copyright (c) 2021-2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/i_abstract_control_unit.h"

#include "Core/Environment/Entities/vehicle_entity.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace gtgen::core::environment::controller
{

class ControlUnitSUT : public IAbstractControlUnit
{
  public:
    std::unique_ptr<IControlUnit> Clone() const override { return nullptr; }
    bool HasFinished() const override { return false; }
    mantle_api::Time GetSpawnTime() { return spawn_time_; }
    mantle_api::Time GetLastSimulationTime() { return last_simulation_time_; }
    mantle_api::Time GetCurrentSimulationTime() const { return current_simulation_time_; }
    void SetCurrentSimulationTime(mantle_api::Time current_simulation_time)
    {
        current_simulation_time_ = current_simulation_time;
    }

    MOCK_METHOD(void, StepControlUnit, (), (override));
};

TEST(AbstractControlUnitTest,
     GivenControlUnit_WhenStepFirstTime_ThenSpawnTimeAndLastSimulationTimeAreInitalizedInControlUnit)
{
    ControlUnitSUT control_unit_sut;
    EXPECT_EQ(mantle_api::Time{0}, control_unit_sut.GetSpawnTime());
    EXPECT_EQ(mantle_api::Time{0}, control_unit_sut.GetLastSimulationTime());

    mantle_api::Time spawn_time = mantle_api::Time{10};
    mantle_api::Time second_step_time = mantle_api::Time{20};

    EXPECT_CALL(control_unit_sut, StepControlUnit()).Times(2);

    control_unit_sut.Step(spawn_time);
    EXPECT_EQ(spawn_time, control_unit_sut.GetSpawnTime());
    EXPECT_EQ(spawn_time, control_unit_sut.GetLastSimulationTime());
    control_unit_sut.Step(second_step_time);
    EXPECT_EQ(spawn_time, control_unit_sut.GetSpawnTime());
    EXPECT_EQ(second_step_time, control_unit_sut.GetLastSimulationTime());
}

TEST(AbstractControlUnitTest, GivenControlUnit_WhenGetName_ThenReturnClassName)
{
    ControlUnitSUT control_unit_sut;
    EXPECT_EQ("ControlUnitSUT", control_unit_sut.GetName());
}

TEST(
    AbstractControlUnitTest,
    GivenControlUnitAndPositionIsChangedByExternalAction_WhenCheckHasEntityBeenModifiedOutsideOfControlUnit_ThenReturnTrue)
{
    using units::literals::operator""_m;

    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

    ControlUnitSUT control_unit_sut;
    control_unit_sut.SetEntity(*entity);

    control_unit_sut.Step(mantle_api::Time{0});
    EXPECT_FALSE(control_unit_sut.HasEntityBeenModifiedOutsideOfControlUnit());

    const auto position_changed = mantle_api::Vec3<units::length::meter_t>{200_m, 0_m, 0_m};

    entity->SetPosition(position_changed);
    EXPECT_TRUE(control_unit_sut.HasEntityBeenModifiedOutsideOfControlUnit());
}

TEST(
    AbstractControlUnitTest,
    GivenControlUnitAndOrientationIsChangedByExternalAction_WhenCheckHasEntityBeenModifiedOutsideOfControlUnit_ThenReturnTrue)
{
    using units::literals::operator""_rad;

    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

    ControlUnitSUT control_unit_sut;
    control_unit_sut.SetEntity(*entity);

    control_unit_sut.Step(mantle_api::Time{0});
    EXPECT_FALSE(control_unit_sut.HasEntityBeenModifiedOutsideOfControlUnit());

    const auto orientation_changed = mantle_api::Orientation3<units::angle::radian_t>{1_rad, 0_rad, 0_rad};

    entity->SetOrientation(orientation_changed);
    EXPECT_TRUE(control_unit_sut.HasEntityBeenModifiedOutsideOfControlUnit());
}

}  // namespace gtgen::core::environment::controller
