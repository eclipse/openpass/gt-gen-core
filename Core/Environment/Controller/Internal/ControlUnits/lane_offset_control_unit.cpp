/*******************************************************************************
 * Copyright (c) 2021-2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/lane_offset_control_unit.h"

#include "Core/Environment/Controller/Internal/Utilities/spline_utilities.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Service/GlmWrapper/glm_basic_orientation_utils.h"
#include "Core/Service/GlmWrapper/glm_basic_vector_utils.h"
#include "Core/Service/Profiling/profiling.h"

namespace gtgen::core::environment::controller
{

LaneOffsetControlUnit::LaneOffsetControlUnit(
    const std::vector<mantle_api::SplineSection<units::length::meter>>& lateral_lane_offsets)
    : lateral_lane_offsets_{lateral_lane_offsets}, lane_location_provider_{nullptr}
{
    originating_control_strategy_ = mantle_api::ControlStrategyType::kFollowLateralOffsetSpline;
}

LaneOffsetControlUnit::LaneOffsetControlUnit(mantle_api::ILaneLocationQueryService* map_query_service)
    : lane_location_provider_{dynamic_cast<map::LaneLocationProvider*>(map_query_service)}
{
    originating_control_strategy_ = mantle_api::ControlStrategyType::kKeepLaneOffset;
}

std::unique_ptr<IControlUnit> LaneOffsetControlUnit::Clone() const
{
    return std::make_unique<LaneOffsetControlUnit>(*this);
}

void LaneOffsetControlUnit::StepControlUnit()
{
    GTGEN_PROFILE_SCOPE

    using units::literals::operator""_mps_cb;
    using units::literals::operator""_mps_sq;
    using units::literals::operator""_mps;
    using units::literals::operator""_m;

    if (first_step_ || HasEntityBeenModifiedOutsideOfControlUnit())
    {
        if (originating_control_strategy_ == mantle_api::ControlStrategyType::kKeepLaneOffset)
        {
            // Keep current lane offset of entity infinitely
            if (entity_ == nullptr)
            {
                throw EnvironmentException("No entity set to detect current lane offset.");
            }

            auto lane_location = lane_location_provider_->GetLaneLocation(entity_->GetPosition());
            if (!lane_location.IsValid())
            {
                throw EnvironmentException(
                    "Cannot keep lane offset for entity \"{}\" (id: {}): Position {} not on a lane",
                    entity_->GetName(),
                    entity_->GetUniqueId(),
                    entity_->GetPosition());
            }

            if (entity_->GetProperties() == nullptr)
            {
                throw EnvironmentException("Entity {} doesn't have properties.", entity_->GetUniqueId());
            }

            auto vertical_shift =
                -lane_location.lane_normal * (entity_->GetProperties()->bounding_box.dimension.height() / 2);
            auto position_on_road_surface = entity_->GetPosition() + vertical_shift;
            units::length::meter_t lateral_offset_distance{
                service::glmwrapper::Distance(lane_location.projected_centerline_point, position_on_road_surface)};
            // Check, if position on road surface is left or right of center line (the sign of the dot product is
            // consistent to the cosine of the angle between the left vector of the road and the vector from the
            // center line to the position on the road surface ==> if the angle is zero, then the cosine and the dot
            // product are positive ==> positive lane offset)
            double lateral_offset_sign =
                service::glmwrapper::Dot(service::glmwrapper::GetWorldSpaceLeftVector(lane_location.GetOrientation()),
                                         position_on_road_surface - lane_location.projected_centerline_point) > 0
                    ? 1.0
                    : -1.0;
            lateral_lane_offsets_.clear();
            lateral_lane_offsets_.emplace_back(mantle_api::SplineSection<units::length::meter>{
                mantle_api::Time{0},
                mantle_api::Time{std::numeric_limits<double>::max()},
                {0_mps_cb, 0_mps_sq, 0_mps, lateral_offset_distance * lateral_offset_sign}});
        }
        first_step_ = false;
    }

    auto sample_time = last_simulation_time_ + sub_sample_increment;
    if (current_simulation_time_ == last_simulation_time_)
    {
        sample_time = last_simulation_time_;
    }

    while (sample_time <= current_simulation_time_)
    {
        auto spline_time = sample_time - spawn_time_;

        if (!IsInvalidLateralOffset(spline_time))
        {
            auto current_lane_offset_index = GetSplineIndexForTime(spline_time, lateral_lane_offsets_);
            units::length::meter_t lateral_offset{
                EvaluateSpline(spline_time, lateral_lane_offsets_[current_lane_offset_index])};
            data_exchange_container_->lane_offset_scalars.push_back(lateral_offset);

            SetLocalOrientationScalars(spline_time);
        }
        else
        {
            data_exchange_container_->lane_offset_scalars.push_back(0.0_m);
        }

        sample_time += sub_sample_increment;
    }
}

bool LaneOffsetControlUnit::HasFinished() const
{
    return (originating_control_strategy_ != mantle_api::ControlStrategyType::kKeepLaneOffset) &&
           (lateral_lane_offsets_.empty() ||
            (last_simulation_time_ - spawn_time_) > lateral_lane_offsets_.back().end_time);
}

void LaneOffsetControlUnit::SetLocalOrientationScalars(const mantle_api::Time& time)
{
    using units::literals::operator""_rad_per_s;
    using units::literals::operator""_rad_per_s_sq;

    auto current_lane_offset_index = GetSplineIndexForTime(time, lateral_lane_offsets_);
    units::angular_velocity::radians_per_second_t offset_orientation{
        EvaluateSplineDerivative(time, lateral_lane_offsets_[current_lane_offset_index])};
    units::angular_acceleration::radians_per_second_squared_t offset_orientation_acceleration{
        EvaluateSplineSecondDerivative(time, lateral_lane_offsets_[current_lane_offset_index])};

    if (offset_orientation > 0_rad_per_s || offset_orientation < 0_rad_per_s)
    {
        data_exchange_container_->orientation_rate_scalar = offset_orientation;
    }

    if (offset_orientation_acceleration > 0_rad_per_s_sq || offset_orientation_acceleration < 0_rad_per_s_sq)
    {
        data_exchange_container_->orientation_acceleration_scalar = offset_orientation_acceleration;
    }
}

bool LaneOffsetControlUnit::IsInvalidLateralOffset(const mantle_api::Time& time) const
{
    return (lateral_lane_offsets_.empty() || time < lateral_lane_offsets_.front().start_time ||
            time > lateral_lane_offsets_.back().end_time);
}

}  // namespace gtgen::core::environment::controller
