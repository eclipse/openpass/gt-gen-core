/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/Utilities/spline_utilities.h"

#include <gtest/gtest.h>

namespace gtgen::core::environment::controller
{

TEST(SplineUtilitiesTest, GivenEmptySplineVector_WhenIsSplineEmptyOrTimeOutOfSplineRange_ThenReturnTrue)
{
    const std::vector<mantle_api::SplineSection<units::length::meter>> splines{};
    mantle_api::Time time{1};
    EXPECT_TRUE(IsSplineEmptyOrTimeOutOfSplineRange(splines, time));
}

TEST(SplineUtilitiesTest,
     GivenValidSplinesTimeOutsideOfSplineRange_WhenIsSplineEmptyOrTimeOutOfSplineRange_ThenReturnTrue)
{
    mantle_api::SplineSection<units::length::meter> spline{mantle_api::Time{1'000}, mantle_api::Time{2'000}, {}};
    mantle_api::SplineSection<units::length::meter> spline2{mantle_api::Time{2'000}, mantle_api::Time{3'000}, {}};

    const std::vector<mantle_api::SplineSection<units::length::meter>> spline_section{spline, spline2};
    EXPECT_TRUE(IsSplineEmptyOrTimeOutOfSplineRange(spline_section, mantle_api::Time(500)));
    EXPECT_TRUE(IsSplineEmptyOrTimeOutOfSplineRange(spline_section, mantle_api::Time(3500)));
}

TEST(SplineUtilitiesTest, GivenValidSplinesTimeInSplineRange_WhenIsSplineEmptyOrTimeOutOfSplineRange_ThenReturnFalse)
{
    mantle_api::SplineSection<units::length::meter> spline{mantle_api::Time{1'000}, mantle_api::Time{2'000}, {}};
    mantle_api::SplineSection<units::length::meter> spline2{mantle_api::Time{2'000}, mantle_api::Time{3'000}, {}};

    const std::vector<mantle_api::SplineSection<units::length::meter>> spline_section{spline, spline2};
    EXPECT_FALSE(IsSplineEmptyOrTimeOutOfSplineRange(spline_section, mantle_api::Time(1000)));
    EXPECT_FALSE(IsSplineEmptyOrTimeOutOfSplineRange(spline_section, mantle_api::Time(2000)));
}

}  // namespace gtgen::core::environment::controller
