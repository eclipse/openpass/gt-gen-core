/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Environment/Map/Geometry/project_query_point_on_boundaries.h"

#include "Core/Tests/TestUtils/expect_extensions.h"

#include <gtest/gtest.h>

namespace gtgen::core::environment::map
{

TEST(ProjectQueryPointOnBoundaries,
     GivenQueryPointBesideSingleBoundary_WhenProjectQueryPointOnBoundaries_ThenProjectedPointOnBoundary)
{
    using units::literals::operator""_m;

    const mantle_api::Vec3<units::length::meter_t> query_point{1.0_m, 1.0_m, 0.0_m};
    const mantle_api::Vec3<units::length::meter_t> expected_projected_point{1.0_m, 0.0_m, 0.0_m};

    LaneBoundary boundary1{1, LaneBoundary::Type::kSolidLine, LaneBoundary::Color::kWhite};
    boundary1.points = {{{0_m, 0_m, 0_m}, 0.0, 0.0}, {{10_m, 0_m, 0_m}, 0.0, 0.0}};
    const std::vector<const LaneBoundary*> lane_boundaries{&boundary1};

    auto projected_point = ProjectQueryPointOnBoundaries(query_point, lane_boundaries);

    EXPECT_TRIPLE(projected_point.first, expected_projected_point);
}

TEST(ProjectQueryPointOnBoundaries,
     GivenQueryPointBetweenTwoBoundaries_WhenProjectQueryPointOnBoundaries_ThenProjectedPointOnNearerBoundary)
{
    using units::literals::operator""_m;

    const mantle_api::Vec3<units::length::meter_t> query_point{1.0_m, 4.0_m, 0.0_m};
    const mantle_api::Vec3<units::length::meter_t> expected_projected_point{1.0_m, 5.0_m, 0.0_m};

    LaneBoundary boundary1{1, LaneBoundary::Type::kSolidLine, LaneBoundary::Color::kWhite};
    boundary1.points = {{{0_m, 0_m, 0_m}, 0.0, 0.0}, {{10_m, 0_m, 0_m}, 0.0, 0.0}};
    LaneBoundary boundary2{1, LaneBoundary::Type::kSolidLine, LaneBoundary::Color::kWhite};
    boundary2.points = {{{0_m, 5_m, 0_m}, 0.0, 0.0}, {{10_m, 5_m, 0_m}, 0.0, 0.0}};
    const std::vector<const LaneBoundary*> lane_boundaries{&boundary1, &boundary2};

    auto projected_point = ProjectQueryPointOnBoundaries(query_point, lane_boundaries);

    EXPECT_TRIPLE(projected_point.first, expected_projected_point);
}

}  // namespace gtgen::core::environment::map
