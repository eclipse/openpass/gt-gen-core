/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/Geometry/geometry.h"

#include "Core/Tests/TestUtils/expect_extensions.h"

#include <gtest/gtest.h>

namespace gtgen::core::environment::map
{

TEST(IsLineSegmentIntersectingOtherLineSegment, GivenTwoIntersectingLines_WhenCheckingIfIntersecting_ThenReturnTrue)
{
    const LineSegment2d line1{{0, 0}, {0, 2}};
    const LineSegment2d line2{{-1, 1}, {1, 1}};

    EXPECT_TRUE(IsLineSegmentIntersectingOtherLineSegment(line1, line2));
}

TEST(IsLineSegmentIntersectingOtherLineSegment, GivenNonIntersectingLines_WhenCheckingIfIntersecting_ThenReturnFalse)
{
    const LineSegment2d line1{{0, 0}, {0, 2}};
    const LineSegment2d line2{{1, 1}, {2, 1}};

    EXPECT_FALSE(IsLineSegmentIntersectingOtherLineSegment(line1, line2));
}

TEST(IsLineSegmentIntersectingOtherLineSegment, GivenTwoParallelLines_WhenCheckingIfIntersecting_ThenReturnFalse)
{
    const LineSegment2d line1{{0, 0}, {0, 2}};
    const LineSegment2d line2{{1, 0}, {1, 2}};

    EXPECT_FALSE(IsLineSegmentIntersectingOtherLineSegment(line1, line2));
}

TEST(IsLineSegmentIntersectingOtherLineSegment, GivenTwoIdenticallLines_WhenCheckingIfIntersecting_ThenReturnFalse)
{
    const LineSegment2d line1{{0, 0}, {0, 2}};
    const LineSegment2d line2{{0, 0}, {0, 2}};

    EXPECT_FALSE(IsLineSegmentIntersectingOtherLineSegment(line1, line2));
}

TEST(IsLineSegmentIntersectingOtherLineSegment, GivenTwoLinesWithASharedPoint_WhenCheckingIfIntersecting_ThenReturnTrue)
{
    const LineSegment2d line1{{0, 0}, {0, 2}};
    const LineSegment2d line2{{0, 0}, {1, -2}};

    EXPECT_TRUE(IsLineSegmentIntersectingOtherLineSegment(line1, line2));
}

TEST(IsLineSegmentIntersectingBoundingBoxBoundaries,
     GivenLineSegmentIsOutSideOfBoundingBox_WhenCheckingIfIntersecting_ThenReturnFalse)
{
    const BoundingBox2d bounding_box{{0, 0}, {1, 1}};
    const LineSegment2d line_segment{{0.5, 1.1}, {0.5, 2}};

    EXPECT_FALSE(IsLineSegmentIntersectingBoundingBoxBoundaries(bounding_box, line_segment));
}

TEST(IsLineSegmentIntersectingBoundingBoxBoundaries,
     GivenLineSegmentInsideBoundingBox_WhenCheckingIfIntersectiing_ThenReturnFalse)
{
    const BoundingBox2d bounding_box{{0, 0}, {1, 1}};
    const LineSegment2d line_segment{{0.1, 0.1}, {0.9, 0.9}};

    EXPECT_FALSE(IsLineSegmentIntersectingBoundingBoxBoundaries(bounding_box, line_segment));
}

TEST(IsLineSegmentIntersectingBoundingBoxBoundaries,
     GivenLineSegmentIntersectsBoundingBoxBorderOnce_WhenCheckingIfIntersecting_ThenReturnTrue)
{
    const BoundingBox2d bounding_box{{0, 0}, {1, 1}};
    const LineSegment2d line_segment{{0.5, 0.1}, {0.5, 2}};

    EXPECT_TRUE(IsLineSegmentIntersectingBoundingBoxBoundaries(bounding_box, line_segment));
}

TEST(IsLineSegmentIntersectingBoundingBoxBoundaries,
     GivenLineSegmentIntersectsBoundingBoxBorderTwice_WhenCheckingIfIntersecting_ThenReturnTrue)
{
    const BoundingBox2d bounding_box{{0, 0}, {1, 1}};
    const LineSegment2d line_segment{{0.5, -0.1}, {0.5, 2}};

    EXPECT_TRUE(IsLineSegmentIntersectingBoundingBoxBoundaries(bounding_box, line_segment));
}

TEST(IsLineSegmentIntersectingBoundingBoxBoundaries,
     GivenLineSegmentSharesBoundingBoxCornerPoint_WhenCheckingIfIntersecting_ThenReturnTrue)
{
    const BoundingBox2d bounding_box{{0, 0}, {1, 1}};
    const LineSegment2d line_segment{{1, 1}, {2, 2}};

    EXPECT_TRUE(IsLineSegmentIntersectingBoundingBoxBoundaries(bounding_box, line_segment));
}

TEST(IsPolygonIntersectingBoundingBox,
     GivenPolygonCompletelyOutsideBoundingBox_WhenCheckingIfIntersecting_ThenReturnFalse)
{
    const BoundingBox2d bounding_box{{0, 0}, {1, 1}};
    Polygon2d polygon;
    polygon.points = {{-0.9, -0.9}, {-2, -2}, {-1, -1}};

    EXPECT_FALSE(IsPolygonIntersectingBoundingBox(bounding_box, polygon));
}

TEST(IsPolygonIntersectingBoundingBox,
     GivenPolygonCompletelyInsideBoundingBox_WhenCheckingIfIntersecting_ThenReturnFalse)
{
    const BoundingBox2d bounding_box{{0, 0}, {10, 10}};
    Polygon2d polygon;
    polygon.points = {{0.9, 0.9}, {2, 2}, {1, 1}};

    EXPECT_FALSE(IsPolygonIntersectingBoundingBox(bounding_box, polygon));
}

TEST(IsPolygonIntersectingBoundingBox,
     GivenFirstLineSegmentIntersectingBoundingBox_WhenCheckingIfIntersecting_ThenReturnTrue)
{
    const BoundingBox2d bounding_box{{0, 0}, {1, 1}};
    Polygon2d polygon;
    polygon.points = {{-1, 0.5}, {2, 0.5}, {0.5, 10}};

    EXPECT_TRUE(IsPolygonIntersectingBoundingBox(bounding_box, polygon));
}

TEST(IsPolygonIntersectingBoundingBox,
     GivenLastLineSegmentIntersectingBoundingBox_WhenCheckingIfIntersecting_ThenReturnTrue)
{
    const BoundingBox2d bounding_box{{0, 0}, {1, 1}};
    Polygon2d polygon;
    polygon.points = {{2, 0.5}, {0.5, 10}, {-1, 0.5}};

    EXPECT_TRUE(IsPolygonIntersectingBoundingBox(bounding_box, polygon));
}

}  // namespace gtgen::core::environment::map
