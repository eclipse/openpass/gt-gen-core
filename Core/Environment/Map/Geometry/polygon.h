/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_POLYGON_H
#define GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_POLYGON_H

#include <MantleAPI/Common/vector.h>
#include <glm/vec2.hpp>

#include <vector>

namespace gtgen::core::environment::map
{

template <class T>
struct Polygon
{
    std::vector<T> points;
};

using Polygon2d = Polygon<glm::dvec2>;
using Polygon3d = Polygon<mantle_api::Vec3<units::length::meter_t>>;

}  // namespace gtgen::core::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_POLYGON_H
