/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/GtGenMap/gtgen_map.h"

#include "Core/Environment/Map/Common/exceptions.h"
#include "Core/Environment/Map/Geometry/point_in_polygon.h"

namespace gtgen::core::environment::map
{

void GtGenMap::ClearMap()
{
    MapApiDataWrapper::ClearMap();
    road_objects.clear();
    traffic_signs.clear();
    traffic_lights.clear();
    patches.clear();

    lane_boundaries_.clear();
    lane_groups_.clear();
    lanes_.clear();

    id_lane_index_map_.clear();
    id_boundary_index_map_.clear();

    path = "";
    projection_string = "";
    map_model_reference_ = "";
    axis_aligned_world_bounding_box = {};
    source_map_type_ = SourceMapType::kUnknown;

    coordinate_converter = nullptr;
    spatial_hash_ = nullptr;
}

MountedSigns GtGenMap::GetMountedSigns() const
{
    MountedSigns mounted_signs;
    for (const auto& traffic_sign : traffic_signs)
    {
        if (auto mounted_sign = std::dynamic_pointer_cast<MountedSign>(traffic_sign))
        {
            mounted_signs.emplace_back(mounted_sign);
        }
    }
    return mounted_signs;
}

GroundSigns GtGenMap::GetGroundSigns() const
{
    GroundSigns ground_signs;
    for (const auto& traffic_sign : traffic_signs)
    {
        if (const auto& ground_sign = std::dynamic_pointer_cast<GroundSign>(traffic_sign))
        {
            ground_signs.emplace_back(ground_sign);
        }
    }
    return ground_signs;
}

FixedFrictionPatches GtGenMap::GetFixedFrictionPatches() const
{
    FixedFrictionPatches fixed_friction_patches;
    for (const auto& patch : patches)
    {
        if (const auto& fixed_friction_patch = std::dynamic_pointer_cast<FixedFrictionPatch>(patch))
        {
            fixed_friction_patches.emplace_back(fixed_friction_patch);
        }
    }
    return fixed_friction_patches;
}

bool GtGenMap::IsOpenDrive() const
{
    return source_map_type_ == SourceMapType::kOpenDrive;
}

bool GtGenMap::IsNDS() const
{
    return source_map_type_ == SourceMapType::kNDS;
}

LaneBoundary& GtGenMap::GetLaneBoundary(const mantle_api::UniqueId lane_boundary_id)
{
    auto* lane_boundary = FindLaneBoundary(lane_boundary_id);
    if (lane_boundary == nullptr)
    {
        GTGEN_LOG_AND_THROW_WITH_FILE_DETAILS(
            exception::LaneBoundaryNotFound("Could not find lane boundary with ID {}", lane_boundary_id));
    }
    return *lane_boundary;
}

const LaneBoundary& GtGenMap::GetLaneBoundary(const mantle_api::UniqueId lane_boundary_id) const
{
    auto* lane_boundary = FindLaneBoundary(lane_boundary_id);
    if (lane_boundary == nullptr)
    {
        GTGEN_LOG_AND_THROW_WITH_FILE_DETAILS(
            exception::LaneBoundaryNotFound("Could not find lane boundary with ID {}", lane_boundary_id));
    }
    return *lane_boundary;
}

LaneBoundary* GtGenMap::FindLaneBoundary(const mantle_api::UniqueId lane_boundary_id)
{
    if (ContainsLaneBoundary(lane_boundary_id))
    {
        const std::size_t lane_boundary_index = id_boundary_index_map_[lane_boundary_id];
        return &lane_boundaries_[lane_boundary_index];
    }
    return nullptr;
}

const LaneBoundary* GtGenMap::FindLaneBoundary(const mantle_api::UniqueId lane_boundary_id) const
{
    if (ContainsLaneBoundary(lane_boundary_id))
    {
        const std::size_t lane_boundary_index = id_boundary_index_map_.at(lane_boundary_id);
        return &lane_boundaries_[lane_boundary_index];
    }
    return nullptr;
}

Lane& GtGenMap::GetLane(const mantle_api::UniqueId lane_id)
{
    auto* lane = FindLane(lane_id);
    if (lane == nullptr)
    {
        GTGEN_LOG_AND_THROW_WITH_FILE_DETAILS(exception::LaneNotFound("Could not find lane with ID {}", lane_id));
    }
    return *lane;
}

const Lane& GtGenMap::GetLane(const mantle_api::UniqueId lane_id) const
{
    auto* lane = FindLane(lane_id);
    if (lane == nullptr)
    {
        GTGEN_LOG_AND_THROW_WITH_FILE_DETAILS(exception::LaneNotFound("Could not find lane with ID {}", lane_id));
    }
    return *lane;
}

Lane* GtGenMap::FindLane(const mantle_api::UniqueId lane_id)
{
    if (ContainsLane(lane_id))
    {
        const std::size_t lane_index = id_lane_index_map_[lane_id];
        return &lanes_[lane_index];
    }
    return nullptr;
}

const Lane* GtGenMap::FindLane(const mantle_api::UniqueId lane_id) const
{
    if (ContainsLane(lane_id))
    {
        const std::size_t lane_index = id_lane_index_map_.at(lane_id);
        return &lanes_[lane_index];
    }
    return nullptr;
}

bool GtGenMap::ContainsLane(const mantle_api::UniqueId lane_id) const
{
    return id_lane_index_map_.count(lane_id) > 0;
}

bool GtGenMap::ContainsLaneBoundary(const mantle_api::UniqueId lane_boundary_id) const
{
    return id_boundary_index_map_.count(lane_boundary_id) > 0;
}

bool GtGenMap::ContainsLaneGroup(const mantle_api::UniqueId lane_group_id) const
{
    return FindLaneGroup(lane_group_id) != nullptr;
}

LaneGroup& GtGenMap::GetLaneGroup(const mantle_api::UniqueId lane_group_id)
{
    for (auto& lane_group : lane_groups_)
    {
        if (lane_group.id == lane_group_id)
        {
            return lane_group;
        }
    }

    GTGEN_LOG_AND_THROW_WITH_FILE_DETAILS(
        exception::MapException("Could not find lane group with ID {}", lane_group_id));
}

LaneGroup& GtGenMap::AddLaneGroup(const LaneGroup& lane_group)
{
    return lane_groups_.emplace_back(lane_group);
}

void GtGenMap::AddLaneBoundary(const mantle_api::UniqueId lane_group_id, const LaneBoundary& lane_boundary)
{
    mantle_api::UniqueId boundary_id = lane_boundary.id;

    if (ContainsLaneBoundary(boundary_id))
    {
        GTGEN_LOG_AND_THROW_WITH_FILE_DETAILS(
            exception::LaneNotFound("Tried to add lane boundary already contained in the map ID {}", boundary_id));
    }

    auto& lane_group = GetLaneGroup(lane_group_id);
    lane_group.lane_boundary_ids.push_back(boundary_id);

    auto& added_lane_boundary = lane_boundaries_.emplace_back(lane_boundary);
    id_boundary_index_map_[boundary_id] = lane_boundaries_.size() - 1;
    added_lane_boundary.parent_lane_group_id = lane_group_id;
}

void GtGenMap::AddBarrierLaneBoundary(const std::vector<mantle_api::UniqueId>& lane_ids,
                                      const LaneBoundary& barrier_lane_boundary,
                                      const bool is_left_boundary)
{
    auto& added_lane_boundary = lane_boundaries_.emplace_back(barrier_lane_boundary);
    id_boundary_index_map_[barrier_lane_boundary.id] = lane_boundaries_.size() - 1;

    // TODO: (Issue# 2326720)
    // Ideally parent_lane_group_id should be vector of all the lane group ids barrier is next to
    added_lane_boundary.parent_lane_group_id = GetLane(lane_ids.front()).parent_lane_group_id;

    for (const auto lane_id : lane_ids)
    {
        auto& lane = GetLane(lane_id);
        if (is_left_boundary)
        {
            lane.left_lane_boundaries.emplace_back(barrier_lane_boundary.id);
        }
        else
        {
            lane.right_lane_boundaries.emplace_back(barrier_lane_boundary.id);
        }

        auto& lane_group = GetLaneGroup(lane.parent_lane_group_id);
        lane_group.lane_boundary_ids.push_back(barrier_lane_boundary.id);
    }
}

void GtGenMap::AddLaneBoundaries(const mantle_api::UniqueId lane_group_id, const LaneBoundaries& lane_boundaries)
{
    for (const auto& boundary : lane_boundaries)
    {
        AddLaneBoundary(lane_group_id, boundary);
    }
}

void GtGenMap::AddLane(const mantle_api::UniqueId lane_group_id, const Lane& lane)
{
    mantle_api::UniqueId lane_id = lane.id;

    if (ContainsLane(lane_id))
    {
        GTGEN_LOG_AND_THROW_WITH_FILE_DETAILS(
            exception::LaneNotFound("Tried to add lane already contained in the map ID {}", lane_id));
    }

    auto& lane_group = GetLaneGroup(lane_group_id);
    lane_group.lane_ids.push_back(lane_id);

    auto& added_lane = lanes_.emplace_back(lane);
    id_lane_index_map_[lane_id] = lanes_.size() - 1;
    added_lane.parent_lane_group_id = lane_group_id;
}

void GtGenMap::AddLanes(const mantle_api::UniqueId lane_group_id, const Lanes& lanes)
{
    for (const auto& lane : lanes)
    {
        AddLane(lane_group_id, lane);
    }
}

const LaneGroup* GtGenMap::FindLaneGroup(const mantle_api::UniqueId id) const
{
    auto lane_group_it = std::find_if(
        lane_groups_.cbegin(), lane_groups_.cend(), [id](const LaneGroup& lane_group) { return lane_group.id == id; });
    if (lane_group_it == lane_groups_.end())
    {
        return nullptr;
    }
    return &*lane_group_it;
}

LaneGroup* GtGenMap::FindLaneGroup(const mantle_api::UniqueId id)
{
    auto lane_group_it = std::find_if(
        lane_groups_.begin(), lane_groups_.end(), [id](const LaneGroup& lane_group) { return lane_group.id == id; });
    if (lane_group_it == lane_groups_.end())
    {
        return nullptr;
    }
    return &*lane_group_it;
}

void GtGenMap::DoForAllLanes(const std::function<void(Lane&)>& func)
{
    for (auto& lane : lanes_)
    {
        func(lane);
    }
}

void GtGenMap::DoForAllLanes(const std::function<void(const Lane&)>& func) const
{
    for (const auto& lane : lanes_)
    {
        func(lane);
    }
}

void GtGenMap::DoForAllBoundaries(const std::function<void(const LaneBoundary&)>& func) const
{
    for (const auto& lane_boundary : lane_boundaries_)
    {
        func(lane_boundary);
    }
}

void GtGenMap::DoForAllBoundaries(const std::function<void(LaneBoundary&)>& func)
{
    for (auto& lane_boundary : lane_boundaries_)
    {
        func(lane_boundary);
    }
}

void GtGenMap::SetSpatialHash(std::unique_ptr<SpatialHash> spatial_hash)
{
    spatial_hash_ = std::move(spatial_hash);
}

const environment::map::SpatialHash& GtGenMap::GetSpatialHash() const
{
    ASSERT(spatial_hash_ != nullptr && "GtGenMap::SetSpatialHash() needs to be called first");
    return *spatial_hash_;
}

double GtGenMap::GetFrictionAt(const mantle_api::Vec3<units::length::meter_t>& position) const
{
    double road_friction_at_position{1.0};

    for (const auto& patch : patches)
    {
        if (auto friction_patch = dynamic_cast<environment::map::FixedFrictionPatch*>(patch.get()))
        {
            if (map::PointInPolygon(friction_patch->shape_2d.points, glm::dvec2(position.x(), position.y())))
            {
                road_friction_at_position = friction_patch->mue;
                break;
            }
        }
    }
    return road_friction_at_position;
}
std::vector<const LaneBoundary*> GtGenMap::GetLeftLaneBoundaries(const Lane* lane) const
{
    std::vector<const LaneBoundary*> left_lane_boundaries;
    left_lane_boundaries.reserve(lane->left_lane_boundaries.size());
    for (const auto& id : lane->left_lane_boundaries)
    {
        left_lane_boundaries.push_back(&GetLaneBoundary(id));
    }

    return left_lane_boundaries;
}

std::vector<const LaneBoundary*> GtGenMap::GetRightLaneBoundaries(const Lane* lane) const
{
    std::vector<const LaneBoundary*> right_lane_boundaries;
    right_lane_boundaries.reserve(lane->right_lane_boundaries.size());
    for (const auto& id : lane->right_lane_boundaries)
    {
        right_lane_boundaries.push_back(&GetLaneBoundary(id));
    }

    return right_lane_boundaries;
}

void GtGenMap::SetModelReference(const std::string& map_model_reference)
{
    map_model_reference_ = map_model_reference;
}

const std::string& GtGenMap::GetModelReference() const
{
    return map_model_reference_;
}

}  // namespace gtgen::core::environment::map
