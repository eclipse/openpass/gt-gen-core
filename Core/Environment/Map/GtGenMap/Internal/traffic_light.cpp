/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/GtGenMap/Internal/traffic_light.h"

#include "Core/Service/Logging/logging.h"

namespace gtgen::core::environment::map
{

void TrafficLight::SetBulbStates(const TrafficLightBulbStates& bulb_states)
{
    for (auto& bulb : light_bulbs)
    {
        ASSERT(bulb_states.count(bulb.color) > 0 && "Traffic light bulb mode is not set for all traffic light bulbs.");
        bulb.mode = bulb_states.at(bulb.color);
    }
}

}  // namespace gtgen::core::environment::map
