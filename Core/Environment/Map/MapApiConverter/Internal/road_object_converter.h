/*******************************************************************************
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_ROADOBJECTCONVERTER_H
#define GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_ROADOBJECTCONVERTER_H

#include "Core/Environment/Map/GtGenMap/road_objects.h"

#include <MapAPI/stationary_object.h>

namespace gtgen::core::environment::map
{

RoadObject ConvertRoadObject(const map_api::StationaryObject& from);

}  // namespace gtgen::core::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_ROADOBJECTCONVERTER_H
