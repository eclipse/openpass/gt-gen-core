/*******************************************************************************
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/MapApiConverter/Internal/road_object_types_converter.h"

#include "Core/Service/Logging/logging.h"

namespace gtgen::core::environment::map
{

mantle_api::StaticObjectType ConvertRoadObjectType(const map_api::StationaryObject::Type& from)
{
    switch (from)
    {
        case map_api::StationaryObject::Type::kBarrier:
            return mantle_api::StaticObjectType::kBarrier;
        case map_api::StationaryObject::Type::kBridge:
            return mantle_api::StaticObjectType::kBridge;
        case map_api::StationaryObject::Type::kBuilding:
            return mantle_api::StaticObjectType::kBuilding;
        case map_api::StationaryObject::Type::kConstructionSiteElement:
            return mantle_api::StaticObjectType::kConstructionSiteElement;
        case map_api::StationaryObject::Type::kCurbstone:
            return mantle_api::StaticObjectType::kCurbstone;
        case map_api::StationaryObject::Type::kDelineator:
            return mantle_api::StaticObjectType::kDelineator;
        case map_api::StationaryObject::Type::kEmittingStructure:
            return mantle_api::StaticObjectType::kEmittingStructure;
        case map_api::StationaryObject::Type::kOverheadStructure:
            return mantle_api::StaticObjectType::kOverheadStructure;
        case map_api::StationaryObject::Type::kRectangularStructure:
            return mantle_api::StaticObjectType::kRectangularStructure;
        case map_api::StationaryObject::Type::kReflectiveStructure:
            return mantle_api::StaticObjectType::kReflectiveStructure;
        case map_api::StationaryObject::Type::kVerticalStructure:
            return mantle_api::StaticObjectType::kVerticalStructure;
        case map_api::StationaryObject::Type::kPole:
            return mantle_api::StaticObjectType::kPole;
        case map_api::StationaryObject::Type::kPylon:
            return mantle_api::StaticObjectType::kPylon;
        case map_api::StationaryObject::Type::kSpeedBump:
            return mantle_api::StaticObjectType::kSpeedBump;
        case map_api::StationaryObject::Type::kTree:
            return mantle_api::StaticObjectType::kTree;
        case map_api::StationaryObject::Type::kVegetation:
            return mantle_api::StaticObjectType::kVegetation;
        case map_api::StationaryObject::Type::kWall:
            return mantle_api::StaticObjectType::kWall;
        case map_api::StationaryObject::Type::kUnknown:
            return mantle_api::StaticObjectType::kInvalid;
        case map_api::StationaryObject::Type::kOther:
            return mantle_api::StaticObjectType::kOther;
        default:
            Warn("The given map_api::StationaryObject::Type value={} cannot be converted! Return Invalid as default.",
                 static_cast<std::uint8_t>(from));
            return mantle_api::StaticObjectType::kInvalid;
    }
}

osi::StationaryObjectEntityMaterial ConvertRoadObjectMaterial(const map_api::StationaryObject::Material& from)
{
    switch (from)
    {
        case map_api::StationaryObject::Material::kUnknown:
            return osi::StationaryObjectEntityMaterial::kUnknown;
        case map_api::StationaryObject::Material::kOther:
            return osi::StationaryObjectEntityMaterial::kOther;
        case map_api::StationaryObject::Material::kWood:
            return osi::StationaryObjectEntityMaterial::kWood;
        case map_api::StationaryObject::Material::kPlastic:
            return osi::StationaryObjectEntityMaterial::kPlastic;
        case map_api::StationaryObject::Material::kConcrete:
            return osi::StationaryObjectEntityMaterial::kConcrete;
        case map_api::StationaryObject::Material::kMetal:
            return osi::StationaryObjectEntityMaterial::kMetal;
        case map_api::StationaryObject::Material::kStone:
            return osi::StationaryObjectEntityMaterial::kStone;
        case map_api::StationaryObject::Material::kGlas:
            return osi::StationaryObjectEntityMaterial::kGlas;
        case map_api::StationaryObject::Material::kMud:
            return osi::StationaryObjectEntityMaterial::kMud;
        default:
            Warn("The given map_api::StationaryObject::Material value={} cannot be converted! Return Other as default.",
                 static_cast<std::uint8_t>(from));
            return osi::StationaryObjectEntityMaterial::kUnknown;
    }
}

}  // namespace gtgen::core::environment::map
