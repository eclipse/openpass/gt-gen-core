/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_LANEGROUPCONVERTER_H
#define GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_LANEGROUPCONVERTER_H

#include "Core/Environment/Map/GtGenMap/lane_group.h"

#include <MapAPI/lane_group.h>

namespace gtgen::core::environment::map
{

LaneGroup::Type ConvertLaneGroupType(const map_api::LaneGroup::Type from);

LaneGroup CreateLaneGroup(const map_api::Identifier id, const map_api::LaneGroup::Type type);

void AppendLaneAndLaneBoundaryToLaneGroupIdMap(
    const map_api::LaneGroup& lane_group,
    std::unordered_map<mantle_api::UniqueId, mantle_api::UniqueId>& lane_to_lane_group_id_map,
    std::unordered_map<mantle_api::UniqueId, mantle_api::UniqueId>& lane_boundary_to_lane_group_id_map);

}  // namespace gtgen::core::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_LANEGROUPCONVERTER_H
