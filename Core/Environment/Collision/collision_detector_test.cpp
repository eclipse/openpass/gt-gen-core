/*******************************************************************************
 * Copyright (c) 2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Collision/collision_detector.h"

#include "Core/Environment/DataStore/output_generator.h"
#include "Core/Environment/Entities/static_object_entity.h"
#include "Core/Environment/Entities/traffic_sign_entity.h"
#include "Core/Environment/Entities/vehicle_entity.h"
#include "Core/Environment/GtGenEnvironment/entity_repository.h"
#include "Core/Service/MantleApiExtension/static_object_properties.h"
#include "Core/Service/Utility/unique_id_provider.h"

#include <gtest/gtest.h>
#include <sys/stat.h>

#include <fstream>
#include <memory>

namespace gtgen::core::environment::collision
{

using units::literals::operator""_m;
using units::literals::operator""_rad;
using units::literals::operator""_kg;

/// @brief The test is about collision between a vehicle and a static object.
///        As a result of collision, collisionPartnersId of both entities shall be updated with
///        Id of the collided objects
TEST(CollisionDetectorTest, TestCollisionVehicleStaticObject)
{
    // name entities
    std::string expected_vehicle_name{"test-vehicle"};
    std::string expected_static_object_name{"test-static-object"};

    // initialize necessary components
    service::utility::UniqueIdProvider id_provider{};
    gtgen::core::environment::api::EntityRepository repo{&id_provider};

    gtgen::core::environment::datastore::OutputGenerator::Settings output_settings_{fs::path{}};
    gtgen::core::environment::datastore::OutputGenerator output_generator{output_settings_};

    // Set-up vehicle properties
    mantle_api::BoundingBox bb_vehicle = {{0_m, 0_m, 0_m}, {3_m, 2_m, 1_m}};
    auto vehicle_properties = mantle_api::VehicleProperties();
    vehicle_properties.bounding_box = bb_vehicle;
    vehicle_properties.type = mantle_api::EntityType::kVehicle;
    vehicle_properties.mass = 10_kg;

    // Create vehicle entity and set position
    auto* entity = dynamic_cast<gtgen::core::environment::entities::VehicleEntity*>(
        &repo.Create(expected_vehicle_name, vehicle_properties));
    entity->SetPosition({5.0_m, 5.0_m, 0.0_m});

    // Set-up static object properties
    mantle_api::BoundingBox bb_static = {{0_m, 0_m, 0_m}, {1_m, 1_m, 1_m}};
    auto static_object_properties = mantle_api::StaticObjectProperties();
    static_object_properties.bounding_box = bb_static;
    static_object_properties.type = mantle_api::EntityType::kStatic;
    static_object_properties.mass = 10_kg;
    // Create static object and set position
    auto* static_object = dynamic_cast<gtgen::core::environment::entities::StaticObject*>(
        &repo.Create(expected_static_object_name, static_object_properties));
    static_object->SetPosition({5.0_m, 4.0_m, 0.0_m});

    // Create Collision detector
    auto collision_detector = CollisionDetector(&repo, &output_generator);

    collision_detector.Step(mantle_api::Time{0});

    const auto& partners_1 = entity->GetCollisionPartnersId();
    EXPECT_TRUE(std::find(partners_1.begin(), partners_1.end(), static_object->GetUniqueId()) != partners_1.end());

    const auto& partners_2 = static_object->GetCollisionPartnersId();
    EXPECT_TRUE(std::find(partners_2.begin(), partners_2.end(), entity->GetUniqueId()) != partners_2.end());
}

/// @brief  No collision should be deteced between the vehicle and static object.
///         Bounding boxes of the objects does not intersect and collisionpartnersId shall be empty
TEST(CollisionDetectorTest, TestNoCollisionVehicleStaticObject)
{
    // name entities
    std::string expected_vehicle_name{"test-vehicle"};
    std::string expected_static_object_name{"test-static-object"};

    // initialize necessary components
    service::utility::UniqueIdProvider id_provider{};
    gtgen::core::environment::api::EntityRepository repo{&id_provider};
    gtgen::core::environment::datastore::OutputGenerator::Settings output_settings_{fs::path{}};
    gtgen::core::environment::datastore::OutputGenerator output_generator{output_settings_};

    // Set-up vehicle properties
    mantle_api::BoundingBox bb_vehicle = {{0_m, 0_m, 0_m}, {4_m, 3_m, 1_m}};
    auto vehicle_properties = mantle_api::VehicleProperties();
    vehicle_properties.bounding_box = bb_vehicle;
    vehicle_properties.type = mantle_api::EntityType::kVehicle;
    vehicle_properties.mass = 10_kg;

    // Create vehicle entity and set position
    auto* entity = dynamic_cast<gtgen::core::environment::entities::VehicleEntity*>(
        &repo.Create(expected_vehicle_name, vehicle_properties));
    entity->SetPosition({3.0_m, 6.5_m, 0.0_m});

    // Set-up static object properties
    mantle_api::BoundingBox bb_static = {{0_m, 0_m, 0_m}, {3_m, 3_m, 3_m}};
    auto static_object_properties = mantle_api::StaticObjectProperties();
    static_object_properties.bounding_box = bb_static;
    static_object_properties.type = mantle_api::EntityType::kStatic;
    static_object_properties.mass = 10_kg;

    // Create static object and set position
    auto* static_object = dynamic_cast<gtgen::core::environment::entities::StaticObject*>(
        &repo.Create(expected_static_object_name, static_object_properties));
    static_object->SetPosition({7.5_m, 6.5_m, 0.0_m});

    // Create Collision detector
    auto collision_detector = CollisionDetector(&repo, &output_generator);

    collision_detector.Step(mantle_api::Time{0});

    const auto& partners_1 = entity->GetCollisionPartnersId();
    EXPECT_FALSE(std::find(partners_1.begin(), partners_1.end(), static_object->GetUniqueId()) != partners_1.end());

    const auto& partners_2 = static_object->GetCollisionPartnersId();
    EXPECT_FALSE(std::find(partners_2.begin(), partners_2.end(), entity->GetUniqueId()) != partners_2.end());
}

/// @brief CollisionDetection between two vehicles. Bounding boxes shall intersect
TEST(CollisionDetectorTest, TestCollisionVehicleVehicle)
{
    // name entities
    std::string expected_vehicle_name_1{"test-vehicle-1"};
    std::string expected_vehicle_name_2{"test-vehicle-2"};

    // initialize necessary components
    service::utility::UniqueIdProvider id_provider{};
    gtgen::core::environment::api::EntityRepository repo{&id_provider};
    gtgen::core::environment::datastore::OutputGenerator::Settings output_settings_{fs::path{}};
    gtgen::core::environment::datastore::OutputGenerator output_generator{output_settings_};

    // Set-up vehicle properties
    mantle_api::BoundingBox bb_vehicle_1 = {{0_m, 0_m, 0_m}, {4_m, 3_m, 1_m}};
    auto vehicle_properties_1 = mantle_api::VehicleProperties();
    vehicle_properties_1.bounding_box = bb_vehicle_1;
    vehicle_properties_1.type = mantle_api::EntityType::kVehicle;
    vehicle_properties_1.mass = 10_kg;

    // Create vehicle entity and set position
    auto* entity_1 = dynamic_cast<gtgen::core::environment::entities::VehicleEntity*>(
        &repo.Create(expected_vehicle_name_1, vehicle_properties_1));
    entity_1->SetPosition({3.0_m, 4.5_m, 0.0_m});

    // Set-up vehicle properties
    mantle_api::BoundingBox bb_vehicle_2 = {{0_m, 0_m, 0_m}, {4_m, 6_m, 3_m}};
    auto vehicle_properties_2 = mantle_api::StaticObjectProperties();
    vehicle_properties_2.bounding_box = bb_vehicle_2;
    vehicle_properties_2.type = mantle_api::EntityType::kVehicle;
    vehicle_properties_2.mass = 10_kg;
    // Create vehicle entity and set position
    auto* entity_2 = dynamic_cast<gtgen::core::environment::entities::StaticObject*>(
        &repo.Create(expected_vehicle_name_2, vehicle_properties_2));
    entity_2->SetPosition({6_m, 5_m, 0.0_m});

    // Create Collision detector
    auto collision_detector = CollisionDetector(&repo, &output_generator);

    collision_detector.Step(mantle_api::Time{0});

    const auto& partners_1 = entity_1->GetCollisionPartnersId();
    EXPECT_TRUE(std::find(partners_1.begin(), partners_1.end(), entity_2->GetUniqueId()) != partners_1.end());

    const auto& partners_2 = entity_2->GetCollisionPartnersId();
    EXPECT_TRUE(std::find(partners_2.begin(), partners_2.end(), entity_1->GetUniqueId()) != partners_2.end());
}

/// @brief CollisionDetection between two vehicles. Bounding boxes shall intersect, yaw angle of the first vehiclle is
///        90degrees
TEST(CollisionDetectorTest, TestCollisionVehicleVehicleYaw1)
{
    // name entities
    std::string expected_vehicle_name_1{"test-vehicle-1"};
    std::string expected_vehicle_name_2{"test-vehicle-2"};

    // initialize necessary components
    service::utility::UniqueIdProvider id_provider{};
    gtgen::core::environment::api::EntityRepository repo{&id_provider};
    gtgen::core::environment::datastore::OutputGenerator::Settings output_settings_{fs::path{}};
    gtgen::core::environment::datastore::OutputGenerator output_generator{output_settings_};

    // Set-up vehicle properties
    mantle_api::BoundingBox bb_vehicle_1 = {{0_m, 0_m, 0_m}, {4_m, 3_m, 1_m}};
    auto vehicle_properties_1 = mantle_api::VehicleProperties();
    vehicle_properties_1.bounding_box = bb_vehicle_1;
    vehicle_properties_1.type = mantle_api::EntityType::kVehicle;
    vehicle_properties_1.mass = 10_kg;

    // Create vehicle entity and set position
    auto* entity_1 = dynamic_cast<gtgen::core::environment::entities::VehicleEntity*>(
        &repo.Create(expected_vehicle_name_1, vehicle_properties_1));
    entity_1->SetPosition({3.0_m, 4.5_m, 0.0_m});
    entity_1->SetOrientation({1.57_rad, 0_rad, 0_rad});

    // Set-up vehicle properties
    mantle_api::BoundingBox bb_vehicle_2 = {{0_m, 0_m, 0_m}, {4_m, 6_m, 3_m}};
    auto vehicle_properties_2 = mantle_api::StaticObjectProperties();
    vehicle_properties_2.bounding_box = bb_vehicle_2;
    vehicle_properties_2.type = mantle_api::EntityType::kVehicle;
    vehicle_properties_2.mass = 10_kg;

    // Create vehicle entity and set position
    auto* entity_2 = dynamic_cast<gtgen::core::environment::entities::StaticObject*>(
        &repo.Create(expected_vehicle_name_2, vehicle_properties_2));
    entity_2->SetPosition({6_m, 5_m, 0.0_m});

    // Create Collision detector
    auto collision_detector = CollisionDetector(&repo, &output_generator);

    collision_detector.Step(mantle_api::Time{0});

    const auto& partners_1 = entity_1->GetCollisionPartnersId();
    EXPECT_TRUE(std::find(partners_1.begin(), partners_1.end(), entity_2->GetUniqueId()) != partners_1.end());

    const auto& partners_2 = entity_2->GetCollisionPartnersId();
    EXPECT_TRUE(std::find(partners_2.begin(), partners_2.end(), entity_1->GetUniqueId()) != partners_2.end());
}

/// @brief CollisionDetection between two vehicles. Bounding boxes shall not intersect
TEST(CollisionDetectorTest, TestNoCollisionVehicleVehicle)
{
    // name entities
    std::string expected_vehicle_name_1{"test-vehicle-1"};
    std::string expected_vehicle_name_2{"test-vehicle-2"};

    // initialize necessary components
    service::utility::UniqueIdProvider id_provider{};
    gtgen::core::environment::api::EntityRepository repo{&id_provider};
    gtgen::core::environment::datastore::OutputGenerator::Settings output_settings_{fs::path{}};
    gtgen::core::environment::datastore::OutputGenerator output_generator{output_settings_};

    // Set-up vehicle properties
    mantle_api::BoundingBox bb_vehicle_1 = {{0_m, 0_m, 0_m}, {4_m, 3_m, 1_m}};
    auto vehicle_properties_1 = mantle_api::VehicleProperties();
    vehicle_properties_1.bounding_box = bb_vehicle_1;
    vehicle_properties_1.type = mantle_api::EntityType::kVehicle;
    vehicle_properties_1.mass = 10_kg;

    // Create vehicle entity and set position
    auto* entity_1 = dynamic_cast<gtgen::core::environment::entities::VehicleEntity*>(
        &repo.Create(expected_vehicle_name_1, vehicle_properties_1));
    entity_1->SetPosition({3.0_m, 4.5_m, 0.0_m});

    // Set-up vehicle properties
    mantle_api::BoundingBox bb_vehicle_2 = {{0_m, 0_m, 0_m}, {4_m, 6_m, 3_m}};
    auto vehicle_properties_2 = mantle_api::StaticObjectProperties();
    vehicle_properties_2.bounding_box = bb_vehicle_2;
    vehicle_properties_2.type = mantle_api::EntityType::kVehicle;
    vehicle_properties_2.mass = 10_kg;

    // Create vehicle entity and set position
    auto* entity_2 = dynamic_cast<gtgen::core::environment::entities::StaticObject*>(
        &repo.Create(expected_vehicle_name_2, vehicle_properties_2));
    entity_2->SetPosition({7.5_m, 5_m, 0.0_m});

    // Create Collision detector
    auto collision_detector = CollisionDetector(&repo, &output_generator);

    collision_detector.Step(mantle_api::Time{0});

    const auto& partners_1 = entity_1->GetCollisionPartnersId();
    EXPECT_FALSE(std::find(partners_1.begin(), partners_1.end(), entity_2->GetUniqueId()) != partners_1.end());

    const auto& partners_2 = entity_2->GetCollisionPartnersId();
    EXPECT_FALSE(std::find(partners_2.begin(), partners_2.end(), entity_1->GetUniqueId()) != partners_2.end());
}

/// @brief Collision between two vehicles. Bounding boxes shall intersect, if yaw angle of the first vehicle is 0
TEST(CollisionDetectorTest, TestCollisionVehicleVehicleYaw2)
{
    // name entities
    std::string expected_vehicle_name_1{"test-vehicle-1"};
    std::string expected_vehicle_name_2{"test-vehicle-2"};

    // initialize necessary components
    service::utility::UniqueIdProvider id_provider{};
    gtgen::core::environment::api::EntityRepository repo{&id_provider};
    gtgen::core::environment::datastore::OutputGenerator::Settings output_settings_{fs::path{}};
    gtgen::core::environment::datastore::OutputGenerator output_generator{output_settings_};

    // Set-up vehicle properties
    mantle_api::BoundingBox bb_vehicle_1 = {{0_m, 0_m, 0_m}, {4_m, 1_m, 1_m}};
    auto vehicle_properties_1 = mantle_api::VehicleProperties();
    vehicle_properties_1.bounding_box = bb_vehicle_1;
    vehicle_properties_1.type = mantle_api::EntityType::kVehicle;
    vehicle_properties_1.mass = 10_kg;

    // Create vehicle entity and set position
    auto* entity_1 = dynamic_cast<gtgen::core::environment::entities::VehicleEntity*>(
        &repo.Create(expected_vehicle_name_1, vehicle_properties_1));
    entity_1->SetPosition({3.0_m, 4.5_m, 0.0_m});
    entity_1->SetOrientation({0_rad, 0_rad, 0_rad});

    // Set-up vehicle properties
    mantle_api::BoundingBox bb_vehicle_2 = {{0_m, 0_m, 0_m}, {3_m, 3_m, 3_m}};
    auto vehicle_properties_2 = mantle_api::StaticObjectProperties();
    vehicle_properties_2.bounding_box = bb_vehicle_2;
    vehicle_properties_2.type = mantle_api::EntityType::kVehicle;
    vehicle_properties_2.mass = 10_kg;

    // Create vehicle entity and set position
    auto* entity_2 = dynamic_cast<gtgen::core::environment::entities::StaticObject*>(
        &repo.Create(expected_vehicle_name_2, vehicle_properties_2));
    entity_2->SetPosition({5.5_m, 4.5_m, 0.0_m});

    // Create Collision detector
    auto collision_detector = CollisionDetector(&repo, &output_generator);

    collision_detector.Step(mantle_api::Time{0});

    const auto& partners_1 = entity_1->GetCollisionPartnersId();
    EXPECT_TRUE(std::find(partners_1.begin(), partners_1.end(), entity_2->GetUniqueId()) != partners_1.end());

    const auto& partners_2 = entity_2->GetCollisionPartnersId();
    EXPECT_TRUE(std::find(partners_2.begin(), partners_2.end(), entity_1->GetUniqueId()) != partners_2.end());
}

/// @brief CollisionDetection between two vehicles. Bounding boxes shall not intersect, if yaw angle of the first
///        vehicle is 90 degrees
TEST(CollisionDetectorTest, TestCollisionVehicleVehicleYaw3)
{
    // name entities
    std::string expected_vehicle_name_1{"test-vehicle-1"};
    std::string expected_vehicle_name_2{"test-vehicle-2"};

    // initialize necessary components
    service::utility::UniqueIdProvider id_provider{};
    gtgen::core::environment::api::EntityRepository repo{&id_provider};
    gtgen::core::environment::datastore::OutputGenerator::Settings output_settings_{fs::path{}};
    gtgen::core::environment::datastore::OutputGenerator output_generator{output_settings_};

    // Set-up vehicle properties
    mantle_api::BoundingBox bb_vehicle_1 = {{0_m, 0_m, 0_m}, {4_m, 1_m, 1_m}};
    auto vehicle_properties_1 = mantle_api::VehicleProperties();
    vehicle_properties_1.bounding_box = bb_vehicle_1;
    vehicle_properties_1.type = mantle_api::EntityType::kVehicle;
    vehicle_properties_1.mass = 10_kg;

    // Create vehicle entity and set position
    auto* entity_1 = dynamic_cast<gtgen::core::environment::entities::VehicleEntity*>(
        &repo.Create(expected_vehicle_name_1, vehicle_properties_1));
    entity_1->SetPosition({3.0_m, 4.5_m, 0.0_m});
    entity_1->SetOrientation({1.57_rad, 0_rad, 0_rad});

    // Set-up vehicle properties
    mantle_api::BoundingBox bb_vehicle_2 = {{0_m, 0_m, 0_m}, {3_m, 3_m, 3_m}};
    auto vehicle_properties_2 = mantle_api::StaticObjectProperties();
    vehicle_properties_2.bounding_box = bb_vehicle_2;
    vehicle_properties_2.type = mantle_api::EntityType::kVehicle;
    vehicle_properties_2.mass = 10_kg;

    // Create vehicle entity and set position
    auto* entity_2 = dynamic_cast<gtgen::core::environment::entities::StaticObject*>(
        &repo.Create(expected_vehicle_name_2, vehicle_properties_2));
    entity_2->SetPosition({5.5_m, 4.5_m, 0.0_m});

    // Create Collision detector
    auto collision_detector = CollisionDetector(&repo, &output_generator);

    collision_detector.Step(mantle_api::Time{0});

    const auto& partners_1 = entity_1->GetCollisionPartnersId();
    EXPECT_FALSE(std::find(partners_1.begin(), partners_1.end(), entity_2->GetUniqueId()) != partners_1.end());

    const auto& partners_2 = entity_2->GetCollisionPartnersId();
    EXPECT_FALSE(std::find(partners_2.begin(), partners_2.end(), entity_1->GetUniqueId()) != partners_2.end());
}

/// @brief  CollisionDetection between two vehicles and static object. The same as TestCollisionVehicleVehicleYaw2,
///         with addition of static object which does not collide with vehicles, when vehicles collide with each other.
TEST(CollisionDetectorTest, TestCollisionVehicleVehicleYaw4)
{
    // name entities
    std::string expected_vehicle_name_1{"test-vehicle-1"};
    std::string expected_vehicle_name_2{"test-vehicle-2"};
    std::string expected_static_object_name{"test-static-object"};

    // initialize necessary components
    service::utility::UniqueIdProvider id_provider{};
    gtgen::core::environment::api::EntityRepository repo{&id_provider};
    gtgen::core::environment::datastore::OutputGenerator::Settings output_settings_{fs::path{}};
    gtgen::core::environment::datastore::OutputGenerator output_generator{output_settings_};

    // Set-up vehicle properties
    mantle_api::BoundingBox bb_vehicle_1 = {{0_m, 0_m, 0_m}, {4_m, 1_m, 1_m}};
    auto vehicle_properties_1 = mantle_api::VehicleProperties();
    vehicle_properties_1.bounding_box = bb_vehicle_1;
    vehicle_properties_1.type = mantle_api::EntityType::kVehicle;
    vehicle_properties_1.mass = 10_kg;

    // Create vehicle entity and set position
    auto* entity_1 = dynamic_cast<gtgen::core::environment::entities::VehicleEntity*>(
        &repo.Create(expected_vehicle_name_1, vehicle_properties_1));
    entity_1->SetPosition({3.0_m, 4.5_m, 0.0_m});
    entity_1->SetOrientation({0_rad, 0_rad, 0_rad});

    // Set-up vehicle properties
    mantle_api::BoundingBox bb_vehicle_2 = {{0_m, 0_m, 0_m}, {3_m, 3_m, 3_m}};
    auto vehicle_properties_2 = mantle_api::StaticObjectProperties();
    vehicle_properties_2.bounding_box = bb_vehicle_2;
    vehicle_properties_2.type = mantle_api::EntityType::kVehicle;
    vehicle_properties_2.mass = 10_kg;

    // Create vehicle entity and set position
    auto* entity_2 = dynamic_cast<gtgen::core::environment::entities::StaticObject*>(
        &repo.Create(expected_vehicle_name_2, vehicle_properties_2));
    entity_2->SetPosition({5.5_m, 4.5_m, 0.0_m});

    // Set-up static object properties
    mantle_api::BoundingBox bb_static = {{0_m, 0_m, 0_m}, {1_m, 1_m, 1_m}};
    auto static_object_properties = mantle_api::StaticObjectProperties();
    static_object_properties.bounding_box = bb_static;
    static_object_properties.type = mantle_api::EntityType::kStatic;
    static_object_properties.mass = 10_kg;

    // Create static object and set position
    auto* static_object = dynamic_cast<gtgen::core::environment::entities::StaticObject*>(
        &repo.Create(expected_static_object_name, static_object_properties));
    static_object->SetPosition({7.5_m, 1.5_m, 0.0_m});

    // Create Collision detector
    auto collision_detector = CollisionDetector(&repo, &output_generator);

    collision_detector.Step(mantle_api::Time{0});

    /// collision between vehicles
    const auto& partners_1 = entity_1->GetCollisionPartnersId();
    EXPECT_TRUE(std::find(partners_1.begin(), partners_1.end(), entity_2->GetUniqueId()) != partners_1.end());

    const auto& partners_2 = entity_2->GetCollisionPartnersId();
    EXPECT_TRUE(std::find(partners_2.begin(), partners_2.end(), entity_1->GetUniqueId()) != partners_2.end());

    // collision between static object and vehicles
    EXPECT_FALSE(std::find(partners_1.begin(), partners_1.end(), static_object->GetUniqueId()) != partners_1.end());
    EXPECT_FALSE(std::find(partners_2.begin(), partners_2.end(), static_object->GetUniqueId()) != partners_2.end());

    // collision between static object and vehicles
    auto& partners_3 = static_object->GetCollisionPartnersId();
    EXPECT_FALSE(std::find(partners_3.begin(), partners_3.end(), entity_1->GetUniqueId()) != partners_3.end());
    EXPECT_FALSE(std::find(partners_3.begin(), partners_3.end(), entity_2->GetUniqueId()) != partners_3.end());
}

/// @brief  CollisionDetection between two vehicles. Bounding boxes shall not intersect, if yaw angle of
///         the first vehicle is 90degrees
///         The same test as TestCollisionVehicleVehicleYaw3 with addition of static object
TEST(CollisionDetectorTest, TestCollisionVehicleVehicleYaw5)
{
    // name entities
    std::string expected_vehicle_name_1{"test-vehicle-1"};
    std::string expected_vehicle_name_2{"test-vehicle-2"};
    std::string expected_static_object_name{"test-static-object"};

    // initialize necessary components
    service::utility::UniqueIdProvider id_provider{};
    gtgen::core::environment::api::EntityRepository repo{&id_provider};
    gtgen::core::environment::datastore::OutputGenerator::Settings output_settings_{fs::path{}};
    gtgen::core::environment::datastore::OutputGenerator output_generator{output_settings_};

    // Set-up vehicle properties
    mantle_api::BoundingBox bb_vehicle_1 = {{0_m, 0_m, 0_m}, {4_m, 1_m, 1_m}};
    auto vehicle_properties_1 = mantle_api::VehicleProperties();
    vehicle_properties_1.bounding_box = bb_vehicle_1;
    vehicle_properties_1.type = mantle_api::EntityType::kVehicle;
    vehicle_properties_1.mass = 10_kg;

    // Create vehicle entity and set position
    auto* entity_1 = dynamic_cast<gtgen::core::environment::entities::VehicleEntity*>(
        &repo.Create(expected_vehicle_name_1, vehicle_properties_1));
    entity_1->SetPosition({3.0_m, 4.5_m, 0.0_m});
    entity_1->SetOrientation({1.57_rad, 0_rad, 0_rad});

    // Set-up vehicle properties
    mantle_api::BoundingBox bb_vehicle_2 = {{0_m, 0_m, 0_m}, {3_m, 3_m, 3_m}};
    auto vehicle_properties_2 = mantle_api::StaticObjectProperties();
    vehicle_properties_2.bounding_box = bb_vehicle_2;
    vehicle_properties_2.type = mantle_api::EntityType::kVehicle;
    vehicle_properties_1.mass = 10_kg;

    // Create vehicle entity and set position
    auto* entity_2 = dynamic_cast<gtgen::core::environment::entities::StaticObject*>(
        &repo.Create(expected_vehicle_name_2, vehicle_properties_2));
    entity_2->SetPosition({5.5_m, 4.5_m, 0.0_m});

    // Set-up static object properties
    mantle_api::BoundingBox bb_static = {{0_m, 0_m, 0_m}, {1_m, 1_m, 1_m}};
    auto static_object_properties = mantle_api::StaticObjectProperties();
    static_object_properties.bounding_box = bb_static;
    static_object_properties.type = mantle_api::EntityType::kStatic;
    static_object_properties.mass = 10_kg;

    // Create static object and set position
    auto* static_object = dynamic_cast<gtgen::core::environment::entities::StaticObject*>(
        &repo.Create(expected_static_object_name, static_object_properties));
    static_object->SetPosition({7.5_m, 1.5_m, 0.0_m});

    // Create Collision detector
    auto collision_detector = CollisionDetector(&repo, &output_generator);

    collision_detector.Step(mantle_api::Time{0});

    /// collision between vehicles
    const auto& partners_1 = entity_1->GetCollisionPartnersId();
    EXPECT_FALSE(std::find(partners_1.begin(), partners_1.end(), entity_2->GetUniqueId()) != partners_1.end());

    const auto& partners_2 = entity_2->GetCollisionPartnersId();
    EXPECT_FALSE(std::find(partners_2.begin(), partners_2.end(), entity_1->GetUniqueId()) != partners_2.end());

    // collision between static object and vehicles
    EXPECT_FALSE(std::find(partners_1.begin(), partners_1.end(), static_object->GetUniqueId()) != partners_1.end());
    EXPECT_FALSE(std::find(partners_2.begin(), partners_2.end(), static_object->GetUniqueId()) != partners_2.end());

    // collision between static object and vehicles
    auto& partners_3 = static_object->GetCollisionPartnersId();
    EXPECT_FALSE(std::find(partners_3.begin(), partners_3.end(), entity_1->GetUniqueId()) != partners_3.end());
    EXPECT_FALSE(std::find(partners_3.begin(), partners_3.end(), entity_2->GetUniqueId()) != partners_3.end());
}

/// @brief  CollisionDetection between two vehicles and static object. The same as TestCollisionVehicleVehicleYaw2,
///         with addition of static object which shall collide with vehicle 2. Vehicle 2 also collides with
///         vehicle 1, thus vehicle 1 shall have as a collision partner static object
TEST(CollisionDetectorTest, TestCollisionVehicleVehicleStatic)
{
    // name entities
    std::string expected_vehicle_name_1{"test-vehicle-1"};
    std::string expected_vehicle_name_2{"test-vehicle-2"};
    std::string expected_static_object_name{"test-static-object"};

    // initialize necessary components
    service::utility::UniqueIdProvider id_provider{};
    gtgen::core::environment::api::EntityRepository repo{&id_provider};
    gtgen::core::environment::datastore::OutputGenerator::Settings output_settings_{fs::path{}};
    gtgen::core::environment::datastore::OutputGenerator output_generator{output_settings_};

    // Set-up vehicle properties
    mantle_api::BoundingBox bb_vehicle_1 = {{0_m, 0_m, 0_m}, {4_m, 1_m, 1_m}};
    auto vehicle_properties_1 = mantle_api::VehicleProperties();
    vehicle_properties_1.bounding_box = bb_vehicle_1;
    vehicle_properties_1.type = mantle_api::EntityType::kVehicle;
    vehicle_properties_1.mass = 10_kg;

    // Create vehicle entity and set position
    auto* entity_1 = dynamic_cast<gtgen::core::environment::entities::VehicleEntity*>(
        &repo.Create(expected_vehicle_name_1, vehicle_properties_1));
    entity_1->SetPosition({3.0_m, 4.5_m, 0.0_m});
    entity_1->SetOrientation({0_rad, 0_rad, 0_rad});

    // Set-up vehicle properties
    mantle_api::BoundingBox bb_vehicle_2 = {{0_m, 0_m, 0_m}, {3_m, 3_m, 3_m}};
    auto vehicle_properties_2 = mantle_api::StaticObjectProperties();
    vehicle_properties_2.bounding_box = bb_vehicle_2;
    vehicle_properties_2.type = mantle_api::EntityType::kVehicle;
    vehicle_properties_2.mass = 10_kg;

    // Create vehicle entity and set position
    auto* entity_2 = dynamic_cast<gtgen::core::environment::entities::StaticObject*>(
        &repo.Create(expected_vehicle_name_2, vehicle_properties_2));
    entity_2->SetPosition({5.5_m, 4.5_m, 0.0_m});

    // Set-up static object properties
    mantle_api::BoundingBox bb_static = {{0_m, 0_m, 0_m}, {2_m, 2_m, 3_m}};
    auto static_object_properties = mantle_api::StaticObjectProperties();
    static_object_properties.bounding_box = bb_static;
    static_object_properties.type = mantle_api::EntityType::kStatic;
    static_object_properties.mass = 10_kg;

    // Create static object and set position
    auto* static_object = dynamic_cast<gtgen::core::environment::entities::StaticObject*>(
        &repo.Create(expected_static_object_name, static_object_properties));
    static_object->SetPosition({7_m, 3_m, 0.0_m});

    // Create Collision detector
    auto collision_detector = CollisionDetector(&repo, &output_generator);

    collision_detector.Step(mantle_api::Time{0});

    /// collision between vehicles
    const auto& partners_1 = entity_1->GetCollisionPartnersId();
    EXPECT_TRUE(std::find(partners_1.begin(), partners_1.end(), entity_2->GetUniqueId()) != partners_1.end());

    const auto& partners_2 = entity_2->GetCollisionPartnersId();
    EXPECT_TRUE(std::find(partners_2.begin(), partners_2.end(), entity_1->GetUniqueId()) != partners_2.end());

    // collision between static object and vehicles
    EXPECT_TRUE(std::find(partners_1.begin(), partners_1.end(), static_object->GetUniqueId()) != partners_1.end());
    EXPECT_TRUE(std::find(partners_2.begin(), partners_2.end(), static_object->GetUniqueId()) != partners_2.end());
}

/// @brief CollisionDetection between two vehicles. Vehicles have the same dimenstions and
//         orientation. Collision shall happen in x-direction
TEST(CollisionDetectorTest, TestCollisionVehicleVehicleXDirection)
{
    // name entities
    std::string expected_vehicle_name_1{"test-vehicle-1"};
    std::string expected_vehicle_name_2{"test-vehicle-2"};

    // initialize necessary components
    service::utility::UniqueIdProvider id_provider{};
    gtgen::core::environment::api::EntityRepository repo{&id_provider};
    gtgen::core::environment::datastore::OutputGenerator::Settings output_settings_{fs::path{}};
    gtgen::core::environment::datastore::OutputGenerator output_generator{output_settings_};
    // Set-up vehicle properties
    mantle_api::BoundingBox bb_vehicle_1 = {{0_m, 0_m, 0_m}, {4_m, 2_m, 1_m}};
    auto vehicle_properties_1 = mantle_api::VehicleProperties();
    vehicle_properties_1.bounding_box = bb_vehicle_1;
    vehicle_properties_1.type = mantle_api::EntityType::kVehicle;
    vehicle_properties_1.mass = 1000_kg;

    // Create vehicle entity and set position
    auto* entity_1 = dynamic_cast<gtgen::core::environment::entities::VehicleEntity*>(
        &repo.Create(expected_vehicle_name_1, vehicle_properties_1));
    entity_1->SetPosition({1.0_m, 1.0_m, 0.0_m});

    // Set-up vehicle properties
    mantle_api::BoundingBox bb_vehicle_2 = {{0_m, 0_m, 0_m}, {4_m, 2_m, 1_m}};
    auto vehicle_properties_2 = mantle_api::StaticObjectProperties();
    vehicle_properties_2.bounding_box = bb_vehicle_2;
    vehicle_properties_2.type = mantle_api::EntityType::kVehicle;
    vehicle_properties_2.mass = 1000_kg;
    // Create vehicle entity and set position
    auto* entity_2 = dynamic_cast<gtgen::core::environment::entities::StaticObject*>(
        &repo.Create(expected_vehicle_name_2, vehicle_properties_2));
    entity_2->SetPosition({5_m, 1.0_m, 0.0_m});

    // Create Collision detector
    auto collision_detector = CollisionDetector(&repo, &output_generator);

    collision_detector.Step(mantle_api::Time{0});

    const auto& partners_1 = entity_1->GetCollisionPartnersId();
    EXPECT_TRUE(std::find(partners_1.begin(), partners_1.end(), entity_2->GetUniqueId()) != partners_1.end());

    const auto& partners_2 = entity_2->GetCollisionPartnersId();
    EXPECT_TRUE(std::find(partners_2.begin(), partners_2.end(), entity_1->GetUniqueId()) != partners_2.end());
}

/// @brief CollisionDetection between two vehicles. Vehicles have the same dimenstions and
//         orientation. Collision shall happen in y-direction
TEST(CollisionDetectorTest, TestCollisionVehicleVehicleYDirection)
{
    // name entities
    std::string expected_vehicle_name_1{"test-vehicle-1"};
    std::string expected_vehicle_name_2{"test-vehicle-2"};

    // initialize necessary components
    service::utility::UniqueIdProvider id_provider{};
    gtgen::core::environment::api::EntityRepository repo{&id_provider};
    gtgen::core::environment::datastore::OutputGenerator::Settings output_settings_{fs::path{}};
    gtgen::core::environment::datastore::OutputGenerator output_generator{output_settings_};

    // Set-up vehicle properties
    mantle_api::BoundingBox bb_vehicle_1 = {{0_m, 0_m, 0_m}, {4_m, 2_m, 1_m}};
    auto vehicle_properties_1 = mantle_api::VehicleProperties();
    vehicle_properties_1.bounding_box = bb_vehicle_1;
    vehicle_properties_1.type = mantle_api::EntityType::kVehicle;
    vehicle_properties_1.mass = 1000_kg;

    // Create vehicle entity and set position
    auto* entity_1 = dynamic_cast<gtgen::core::environment::entities::VehicleEntity*>(
        &repo.Create(expected_vehicle_name_1, vehicle_properties_1));
    entity_1->SetPosition({1.0_m, 1.0_m, 0.0_m});
    entity_1->SetOrientation({1.57_rad, 0_rad, 0_rad});

    // Set-up vehicle properties
    mantle_api::BoundingBox bb_vehicle_2 = {{0_m, 0_m, 0_m}, {4_m, 2_m, 1_m}};
    auto vehicle_properties_2 = mantle_api::StaticObjectProperties();
    vehicle_properties_2.bounding_box = bb_vehicle_2;
    vehicle_properties_2.type = mantle_api::EntityType::kVehicle;
    vehicle_properties_2.mass = 1000_kg;
    // Create vehicle entity and set position
    auto* entity_2 = dynamic_cast<gtgen::core::environment::entities::StaticObject*>(
        &repo.Create(expected_vehicle_name_2, vehicle_properties_2));
    entity_2->SetPosition({1_m, 5.0_m, 0.0_m});
    entity_2->SetOrientation({1.57_rad, 0_rad, 0_rad});

    // Create Collision detector
    auto collision_detector = CollisionDetector(&repo, &output_generator);

    collision_detector.Step(mantle_api::Time{0});

    const auto& partners_1 = entity_1->GetCollisionPartnersId();
    EXPECT_TRUE(std::find(partners_1.begin(), partners_1.end(), entity_2->GetUniqueId()) != partners_1.end());

    const auto& partners_2 = entity_2->GetCollisionPartnersId();
    EXPECT_TRUE(std::find(partners_2.begin(), partners_2.end(), entity_1->GetUniqueId()) != partners_2.end());
}

}  // namespace gtgen::core::environment::collision