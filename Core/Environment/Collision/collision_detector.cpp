/*******************************************************************************
 * Copyright (c) 2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2016-2018, ITK Engineering GmbH
 *               2017-2021, in-tech GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Collision/collision_detector.h"

#include <algorithm>

namespace gtgen::core::environment::collision
{

using units::literals::operator""_m;
using units::literals::operator""_rad;

CollisionDetector::CollisionDetector(mantle_api::IEntityRepository* entity_repository,
                                     OutputGenerator* output_generator)
    : entity_repository_(entity_repository), output_generator_(output_generator)
{
}

void CollisionDetector::Step([[maybe_unused]] mantle_api::Time current_simulation_time)
{
    const auto& entities{entity_repository_->GetEntities()};

    for (auto it = entities.cbegin(); it != entities.cend(); ++it)
    {
        for (auto other_it = std::next(it); other_it != entities.cend(); ++other_it)
        {
            if (it->get()->GetProperties()->type == mantle_api::EntityType::kVehicle)
            {
                if (DetectCollision(other_it->get(), it->get()))
                {
                    bool opponent_is_vehicle =
                        (other_it->get()->GetProperties()->type == mantle_api::EntityType::kVehicle);
                    const CollisionInfo collision_info = {
                        opponent_is_vehicle, it->get()->GetUniqueId(), other_it->get()->GetUniqueId()};
                    collisions_.push_back(collision_info);
                }
            }
        }
    }

    UpdateCollisions();
}

bool CollisionDetector::DetectCollision(mantle_api::IEntity* other_entity, mantle_api::IEntity* entity) const
{
    auto base_entity = dynamic_cast<gtgen::core::environment::entities::BaseEntity*>(entity);
    auto other_base_entity = dynamic_cast<gtgen::core::environment::entities::BaseEntity*>(other_entity);

    if (!(base_entity && other_base_entity))
    {
        return false;
    }

    // Check if collision was already detected in a previous timestep
    const auto& partners = base_entity->GetCollisionPartnersId();

    if (std::find(partners.begin(), partners.end(), other_base_entity->GetUniqueId()) != partners.end())
    {
        return false;
    }

    const auto base_entity_bounding_box = base_entity->GetProperties()->bounding_box;
    const auto other_bounding_box = other_base_entity->GetProperties()->bounding_box;

    // height check for static traffic objects
    if (!IsEntityCollidable(other_base_entity) ||
        base_entity_bounding_box.dimension.height <
            other_base_entity->GetPosition().z - 0.5 * other_bounding_box.dimension.height)
    {
        return false;
    }

    // Calculate bounding boxes
    auto base_entity_bounding_box_points = CalculateBoundingBox(entity);
    auto other_entity_bounding_box_points = CalculateBoundingBox(other_entity);

    const auto net_x =
        GetCartesianDistanceAlongAxis<0>(base_entity_bounding_box_points, other_entity_bounding_box_points);
    const auto net_y =
        GetCartesianDistanceAlongAxis<1>(base_entity_bounding_box_points, other_entity_bounding_box_points);

    if (!mantle_api::AlmostEqual(net_x, 0.0) || !mantle_api::AlmostEqual(net_y, 0.0))
    {
        return false;
    }

    return bg::intersects(base_entity_bounding_box_points, other_entity_bounding_box_points);
}

void CollisionDetector::UpdateCollisions()
{
    for (const auto collision : collisions_)
    {
        auto* collision_entity = dynamic_cast<gtgen::core::environment::entities::BaseEntity*>(
            &entity_repository_->Get(collision.collision_entity_id)->get());
        auto* collision_opponent = dynamic_cast<gtgen::core::environment::entities::BaseEntity*>(
            &entity_repository_->Get(collision.collision_opponent_id)->get());

        if (collision.collision_with_vehicle)
        {
            UpdateCollision(collision_entity, collision_opponent);
        }
        else
        {
            for (auto partner_id : collision_entity->GetCollisionPartnersId())
            {
                auto* partner = dynamic_cast<gtgen::core::environment::entities::BaseEntity*>(
                    &entity_repository_->Get(partner_id)->get());
                if (partner->GetProperties()->type == mantle_api::EntityType::kVehicle)
                {
                    partner->SetCollisionPartnerId(collision.collision_opponent_id);
                }
            }
            collision_opponent->SetCollisionPartnerId(collision.collision_entity_id);
            collision_entity->SetCollisionPartnerId(collision.collision_opponent_id);
        }

        PublishCrash(collision);
    }
    collisions_.clear();
}

void CollisionDetector::UpdateCollision(gtgen::core::environment::entities::BaseEntity* entity,
                                        gtgen::core::environment::entities::BaseEntity* opponent)
{
    if (entity == nullptr || opponent == nullptr || entity->GetUniqueId() == opponent->GetUniqueId())
    {
        return;
    }

    // check if object is already in stored collision partners
    for (auto partner_id : entity->GetCollisionPartnersId())
    {
        auto* partner =
            dynamic_cast<gtgen::core::environment::entities::BaseEntity*>(&entity_repository_->Get(partner_id)->get());

        if (partner != nullptr && partner_id == opponent->GetUniqueId() &&
            partner->GetProperties()->type == mantle_api::EntityType::kVehicle)
        {
            return;
        }
    }

    // Update the collision partners
    entity->SetCollisionPartnerId(opponent->GetUniqueId());
    opponent->SetCollisionPartnerId(entity->GetUniqueId());

    // Recursion for entity
    for (const auto partner_id : entity->GetCollisionPartnersId())
    {
        auto* partner =
            dynamic_cast<gtgen::core::environment::entities::BaseEntity*>(&entity_repository_->Get(partner_id)->get());

        if (partner != nullptr && partner->GetProperties()->type != mantle_api::EntityType::kVehicle)
        {
            opponent->SetCollisionPartnerId(partner_id);
        }
        else
        {
            UpdateCollision(partner, opponent);
        }
    }

    // Recursion for opponent
    for (const auto partner_id : opponent->GetCollisionPartnersId())
    {
        auto* partner =
            dynamic_cast<gtgen::core::environment::entities::BaseEntity*>(&entity_repository_->Get(partner_id)->get());

        if (partner != nullptr && partner->GetProperties()->type != mantle_api::EntityType::kVehicle)
        {
            entity->SetCollisionPartnerId(partner_id);
        }
        else
        {
            UpdateCollision(partner, entity);
        }
    }
}

bool CollisionDetector::IsEntityCollidable(const mantle_api::IEntity* entity) const
{
    return entity->GetProperties()->bounding_box.dimension != mantle_api::Dimension3{0_m, 0_m, 0_m};
}

ring_t CollisionDetector::CalculateBoundingBox(const mantle_api::IEntity* entity) const
{
    const auto length = entity->GetProperties()->bounding_box.dimension.length;
    const auto width = entity->GetProperties()->bounding_box.dimension.width;
    const auto height = entity->GetProperties()->bounding_box.dimension.height;
    const auto roll = entity->GetOrientation().roll;
    const auto rotation = entity->GetOrientation().yaw;

    const auto halfWidth = width / 2.0;

    const auto widthLeft = halfWidth * units::math::cos(roll) + (roll < 0_rad ? height * units::math::sin(-roll) : 0_m);
    const auto widthRight = halfWidth * units::math::cos(roll) + (roll > 0_rad ? height * units::math::sin(roll) : 0_m);

    ring_t box_points{{units::unit_cast<double>(-length / 2.0), units::unit_cast<double>(-widthRight)},
                      {units::unit_cast<double>(-length / 2.0), units::unit_cast<double>(widthLeft)},
                      {units::unit_cast<double>(length / 2.0), units::unit_cast<double>(widthLeft)},
                      {units::unit_cast<double>(length / 2.0), units::unit_cast<double>(-widthRight)},
                      {units::unit_cast<double>(-length / 2.0), units::unit_cast<double>(-widthRight)}};

    ring_t box;
    ring_t box_temp;

    bg::strategy::transform::translate_transformer<double, 2, 2> translate(entity->GetPosition().x(),
                                                                           entity->GetPosition().y());

    // rotation in mathematical negativ order (boost) -> invert to match
    bg::strategy::transform::rotate_transformer<bg::radian, double, 2, 2> rotate(-rotation.value());

    bg::transform(box_points, box_temp, rotate);
    bg::transform(box_temp, box, translate);

    return box;
}

void CollisionDetector::PublishCrash(const CollisionInfo& collision_info)
{
    if (output_generator_ != nullptr)
    {
        Acyclic acyclic_data;
        acyclic_data.name = "Collision";
        acyclic_data.triggering_entities = {collision_info.collision_entity_id, collision_info.collision_opponent_id};
        acyclic_data.parameter.insert({"CollisionWithAgent", collision_info.collision_with_vehicle});

        output_generator_->PutAcyclic(collision_info.collision_entity_id, "Collision", acyclic_data);
    }
}

}  // namespace gtgen::core::environment::collision
