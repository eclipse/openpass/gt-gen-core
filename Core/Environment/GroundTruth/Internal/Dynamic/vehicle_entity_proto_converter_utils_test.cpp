/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Dynamic/vehicle_entity_proto_converter_utils.h"

#include <gtest/gtest.h>

namespace gtgen::core::environment::proto_groundtruth
{
using OsiLightState = osi3::MovingObject::VehicleClassification::LightState;
using OsiVehicleType = osi3::MovingObject::VehicleClassification;

// IndicatorState converter
class IndicatorStateConverterTestFixture
    : public testing::TestWithParam<std::tuple<mantle_api::IndicatorState, OsiLightState::IndicatorState>>
{
};

INSTANTIATE_TEST_SUITE_P(
    VehicleEntityConverterUtils,
    IndicatorStateConverterTestFixture,
    testing::ValuesIn(std::vector<std::tuple<mantle_api::IndicatorState, OsiLightState::IndicatorState>>{
        std::make_tuple(mantle_api::IndicatorState::kOff, OsiLightState::INDICATOR_STATE_OFF),
        std::make_tuple(mantle_api::IndicatorState::kWarning, OsiLightState::INDICATOR_STATE_WARNING),
        std::make_tuple(mantle_api::IndicatorState::kRight, OsiLightState::INDICATOR_STATE_RIGHT),
        std::make_tuple(mantle_api::IndicatorState::kLeft, OsiLightState::INDICATOR_STATE_LEFT),
        std::make_tuple(mantle_api::IndicatorState::kUnknown, OsiLightState::INDICATOR_STATE_UNKNOWN),
        std::make_tuple(mantle_api::IndicatorState::kOther, OsiLightState::INDICATOR_STATE_OTHER)}));

TEST_P(IndicatorStateConverterTestFixture, GivenIndicatorState_WhenConvertToProtoType_ThenCorrectTypeReturned)
{
    mantle_api::IndicatorState gtgen_type = std::get<0>(GetParam());
    OsiLightState::IndicatorState expected_proto_type = std::get<1>(GetParam());

    auto actual_proto_type = ConvertGtGenIndicatorStateToProto(gtgen_type);

    EXPECT_EQ(expected_proto_type, actual_proto_type);
}

// Vehicletype converter
class VehicleTypeConverterTestFixture
    : public testing::TestWithParam<std::tuple<mantle_api::VehicleClass, OsiVehicleType::Type>>
{
};

INSTANTIATE_TEST_SUITE_P(VehicleEntityConverterUtils,
                         VehicleTypeConverterTestFixture,
                         testing::ValuesIn(std::vector<std::tuple<mantle_api::VehicleClass, OsiVehicleType::Type>>{
                             std::make_tuple(mantle_api::VehicleClass::kOther, OsiVehicleType::TYPE_OTHER),
                             std::make_tuple(mantle_api::VehicleClass::kSmall_car, OsiVehicleType::TYPE_SMALL_CAR),
                             std::make_tuple(mantle_api::VehicleClass::kCompact_car, OsiVehicleType::TYPE_COMPACT_CAR),
                             std::make_tuple(mantle_api::VehicleClass::kMedium_car, OsiVehicleType::TYPE_MEDIUM_CAR),
                             std::make_tuple(mantle_api::VehicleClass::kLuxury_car, OsiVehicleType::TYPE_LUXURY_CAR),
                             std::make_tuple(mantle_api::VehicleClass::kDelivery_van,
                                             OsiVehicleType::TYPE_DELIVERY_VAN),
                             std::make_tuple(mantle_api::VehicleClass::kHeavy_truck, OsiVehicleType::TYPE_HEAVY_TRUCK),
                             std::make_tuple(mantle_api::VehicleClass::kSemitrailer, OsiVehicleType::TYPE_SEMITRAILER),
                             std::make_tuple(mantle_api::VehicleClass::kTrailer, OsiVehicleType::TYPE_TRAILER),
                             std::make_tuple(mantle_api::VehicleClass::kMotorbike, OsiVehicleType::TYPE_MOTORBIKE),
                             std::make_tuple(mantle_api::VehicleClass::kBicycle, OsiVehicleType::TYPE_BICYCLE),
                             std::make_tuple(mantle_api::VehicleClass::kBus, OsiVehicleType::TYPE_BUS),
                             std::make_tuple(mantle_api::VehicleClass::kTram, OsiVehicleType::TYPE_TRAM),
                             std::make_tuple(mantle_api::VehicleClass::kTrain, OsiVehicleType::TYPE_TRAIN),
                             std::make_tuple(mantle_api::VehicleClass::kWheelchair, OsiVehicleType::TYPE_WHEELCHAIR)}));

TEST_P(VehicleTypeConverterTestFixture, GivenVehicleType_WhenConvertToProtoType_ThenCorrectTypeReturned)
{
    mantle_api::VehicleClass gtgen_type = std::get<0>(GetParam());
    OsiVehicleType::Type expected_proto_type = std::get<1>(GetParam());

    auto actual_proto_type = ConvertVehicleTypeToProtoVehicleType(gtgen_type);

    EXPECT_EQ(expected_proto_type, actual_proto_type);
}

// Wheel data
class VehicleEntityWheelDataTestFixture
    : public testing::TestWithParam<std::tuple<std::uint32_t, std::uint32_t, double>>
{
};

INSTANTIATE_TEST_SUITE_P(VehicleEntityConverterUtils,
                         VehicleEntityWheelDataTestFixture,
                         ::testing::Values(std::make_tuple(std::numeric_limits<std::uint32_t>::min(),
                                                           std::numeric_limits<std::uint32_t>::min(),
                                                           std::numeric_limits<double>::min()),
                                           std::make_tuple(std::numeric_limits<std::uint32_t>::max(),
                                                           std::numeric_limits<std::uint32_t>::max(),
                                                           std::numeric_limits<double>::max())));

TEST_P(VehicleEntityWheelDataTestFixture, GivenInputWheelData_WhenFillWheelData_ThenGivenWheelDataIsSet)
{
    osi3::MovingObject_VehicleAttributes_WheelData wheel_data;

    std::uint32_t axel_index = std::get<0>(GetParam());
    std::uint32_t wheel_index = std::get<1>(GetParam());
    double friction_coefficient = std::get<2>(GetParam());

    FillWheelData(axel_index, wheel_index, friction_coefficient, &wheel_data);

    EXPECT_EQ(wheel_data.axle(), axel_index);
    EXPECT_EQ(wheel_data.index(), wheel_index);
    EXPECT_EQ(wheel_data.friction_coefficient(), friction_coefficient);
}

}  // namespace gtgen::core::environment::proto_groundtruth
