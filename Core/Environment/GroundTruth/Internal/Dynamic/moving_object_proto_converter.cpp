/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Dynamic/moving_object_proto_converter.h"

#include "Core/Environment/GroundTruth/Internal/Dynamic/pedestrian_entity_proto_converter.h"
#include "Core/Environment/GroundTruth/Internal/Dynamic/vehicle_entity_proto_converter.h"

namespace gtgen::core::environment::proto_groundtruth
{

void FillProtoGroundTruthMovingObject(const mantle_api::IEntity* entity, osi3::GroundTruth& proto_groundtruth)
{
    if (auto vehicle_entity_ptr = dynamic_cast<const entities::VehicleEntity*>(entity))
    {
        FillProtoGroundTruthVehicleEntity(vehicle_entity_ptr, proto_groundtruth);
    }
    else if (auto pedestrian_entity_ptr = dynamic_cast<const mantle_api::IPedestrian*>(entity))
    {
        FillProtoGroundTruthPedestrianEntity(pedestrian_entity_ptr, proto_groundtruth);
    }
}

}  // namespace gtgen::core::environment::proto_groundtruth
