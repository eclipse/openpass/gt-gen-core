/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_LANERELATIONGROUNDTRUTHBUILDER_H
#define GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_LANERELATIONGROUNDTRUTHBUILDER_H

#include "Core/Environment/Map/GtGenMap/lane.h"
#include "osi_lane.pb.h"

#include <MantleAPI/Common/i_identifiable.h>

#include <unordered_set>

namespace gtgen::core::environment::proto_groundtruth
{

class LaneRelationGroundTruthBuilder
{
  public:
    /// @brief Fills the lane relation OSI fields
    /// @param existing_lane_ids  A list of lanes existing in the current chunk(s)
    explicit LaneRelationGroundTruthBuilder(const std::unordered_set<mantle_api::UniqueId>& existing_lane_ids);

    /// @brief Adds the successor/predecessor id information to the ground truth lane object:
    ///  All combinations of successor and predecessor lane ids are added as lane-pairings.
    ///  A lane-pair always contains the successor and predecessor. If one of these IDs does not exist in the current
    ///  chunk, a InvalidId will be set. If the lane has neither a successor or a predecessor, the lane
    ///  pairing is not added. This means the only valid lane pairing combinations are:
    ///   1) valid_successor_id + valid_predecessor_id
    ///   2) valid_successor_id + InvalidId
    ///   3) InvalidId + valid_predecessor_id
    void AddLanePairings(const map::Lane& gtgen_lane, osi3::Lane& gt_lane) const;

    /// @brief Adds the left and right adjacent lane ids to ground truth lane object:
    ///        - A lane can have no neighboured lanes, in that case the lists stay empty
    ///        - If a lane has a neighbour which is not in the current chunk it is not added.
    void AddAdjacentLaneIds(const map::Lane& gtgen_lane, osi3::Lane& gt_lane) const;

  private:
    void AddAllLanePairingCombinations(osi3::Lane& gt_lane, const map::Lane& gtgen_lane) const;

    void AddLanePairing(osi3::Lane& gt_lane,
                        mantle_api::UniqueId successor_id,
                        mantle_api::UniqueId predecessor_id) const;

    mantle_api::UniqueId GetExistingId(mantle_api::UniqueId id) const;
    bool IdExists(mantle_api::UniqueId id) const;

    std::unordered_set<mantle_api::UniqueId> existing_lane_ids_;
};

}  // namespace gtgen::core::environment::proto_groundtruth

#endif  // GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_LANERELATIONGROUNDTRUTHBUILDER_H
