/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_TRAFFICLIGHTPROTOCONVERTER_H
#define GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_TRAFFICLIGHTPROTOCONVERTER_H

#include "Core/Environment/Map/GtGenMap/Internal/traffic_light.h"
#include "osi_groundtruth.pb.h"

#include <MantleAPI/Traffic/i_entity.h>

namespace gtgen::core::environment::proto_groundtruth
{
/// @deprecated - remove when legacy code is deleted, and refactor gtgen map to only use i_entity is done
void FillProtoGroundTruthTrafficLight(const environment::map::TrafficLight* gtgen_traffic_light,
                                      osi3::GroundTruth& groundtruth);

void AddTrafficLightEntityToGroundTruth(const mantle_api::IEntity* entity, osi3::GroundTruth& ground_truth);

}  // namespace gtgen::core::environment::proto_groundtruth

#endif  // GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_TRAFFICLIGHTPROTOCONVERTER_H
