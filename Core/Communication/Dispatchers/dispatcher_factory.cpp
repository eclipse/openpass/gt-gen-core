/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Communication/Dispatchers/dispatcher_factory.h"

namespace gtgen::core::communication
{

DispatcherFactory::DispatcherFactory(std::function<std::unique_ptr<IDispatcher>()> create_function,
                                     std::string dispatcher_purpose)
    : create_function_{std::move(create_function)}, purpose_{std::move(dispatcher_purpose)}
{
}

std::unique_ptr<IDispatcher> DispatcherFactory::Create() const
{
    return create_function_();
}

}  // namespace gtgen::core::communication
