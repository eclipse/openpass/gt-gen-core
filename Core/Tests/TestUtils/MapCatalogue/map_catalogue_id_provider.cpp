/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue_id_provider.h"

#include <MantleAPI/Common/i_identifiable.h>

namespace gtgen::core::test_utils
{

mantle_api::UniqueId MapCatalogueIdProvider::GetNewId()
{
    return id_++;
}

void MapCatalogueIdProvider::Reset()
{
    id_ = 0;
}

}  // namespace gtgen::core::test_utils
