/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_PROFILING_PROFILING_H
#define GTGEN_CORE_SERVICE_PROFILING_PROFILING_H

#ifdef GTGEN_PROFILING_ENABLED

#include "Tracy.hpp"

#define GTGEN_PROFILE_NEW_FRAME FrameMark
#define GTGEN_PROFILE_NEW_FRAME_NAMED(name) FrameMarkNamed(name)

#define GTGEN_PROFILE_SCOPE ZoneScoped
#define GTGEN_PROFILE_SCOPE_NAMED(name) ZoneScopedN(name)

#define GTGEN_PROFILE_MESSAGE(txt, size) TracyMessage(txt, size)

#else

#define GTGEN_PROFILE_NEW_FRAME
#define GTGEN_PROFILE_NEW_FRAME_NAMED(name)

#define GTGEN_PROFILE_SCOPE
#define GTGEN_PROFILE_SCOPE_NAMED(name)

#define GTGEN_PROFILE_MESSAGE(txt, size)

#endif

#endif  // GTGEN_CORE_SERVICE_PROFILING_PROFILING_H
