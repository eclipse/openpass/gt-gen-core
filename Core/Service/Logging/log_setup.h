/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_LOGGING_LOGSETUP_H
#define GTGEN_CORE_SERVICE_LOGGING_LOGSETUP_H

#include <string>

namespace gtgen::core::service::logging
{
enum class FileLoggingOption
{
    kNoFileLogging,
    kAlwaysLogToNewFile,
    kOverwritePreviousLogFile
};

class LogSetup
{
  public:
    static LogSetup& Instance()
    {
        static LogSetup log_setup;
        return log_setup;
    }

    LogSetup(const LogSetup&) = delete;
    LogSetup(const LogSetup&&) = delete;
    LogSetup& operator=(const LogSetup&) = delete;
    LogSetup& operator=(const LogSetup&&) = delete;

    ~LogSetup() = default;

    /// @brief Setups the spdlog framework: Creates the default logger with a console sink
    /// @param log_to_stderr When running acceptance tests this flag should be set to true, so that the logging shows up
    void AddConsoleLogger(const std::string& log_level);

    /// @brief Setups the spdlog framework: Creates a file sink
    void AddFileLogger(const std::string& log_file_path,
                       FileLoggingOption file_logging_options,
                       const std::string& file_log_level);

    /// @brief Logging needs to be destroyed, otherwise we may end in trouble when running multiple tests
    void CleanupLogging();

  private:
    LogSetup() = default;

    void AddConsoleLoggerInternal(const std::string& log_level);

    std::string GetLogFileName(FileLoggingOption file_logging_options);

    bool console_logger_added_{false};
};

}  // namespace gtgen::core::service::logging

#endif  // GTGEN_CORE_SERVICE_LOGGING_LOGSETUP_H
