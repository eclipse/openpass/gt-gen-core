/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Service/Version/version.h"

#include <iostream>
#include <sstream>

namespace gtgen::core
{

std::string GetGtGenCoreVersion()
{
    return fmt::format("{}", gtgen_core_version);
}

Version GetOsiVersion()
{
    std::istringstream s(std::string(OSI_VERSION));
    std::string version_number;

    Version version;

    std::getline(s, version_number, '.');
    version.major = std::stoi(version_number);

    std::getline(s, version_number, '.');
    version.minor = std::stoi(version_number);

    std::getline(s, version_number, '.');
    version.patch = std::stoi(version_number);

    return version;
}

}  // namespace gtgen::core
