/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/TraceWriter/trace_writer.h"

#include "Core/Service/FileSystem/file_system_utils.h"

#include <boost/endian/conversion.hpp>
#include <gtest/gtest.h>
#include <osi_sensorview.pb.h>

#include <filesystem>
#include <fstream>
#include <iterator>
#include <memory>
#include <string>

namespace gtgen::core::service
{

namespace
{

const auto TEST_DATA_DIR_PATH = fs::path{fs::absolute(__FILE__)}.parent_path();

struct InputOutputParameters
{
    fs::path expected_trace_file;
    fs::path actual_trace_file;
};

class TraceWriterUnitTestFixture : public ::testing::TestWithParam<InputOutputParameters>
{
  protected:
    void SetUp() override
    {
        // We are going to load the vector sensor_view_messages_ with
        // messages contained in sensor_view.bin file which then would
        // be used to create the trace
        auto sensor_view_messages_path = TEST_DATA_DIR_PATH / "sensor_view.bin";
        std::ifstream input_file(sensor_view_messages_path, std::ios::binary);
        if (!input_file)
        {
            throw std::runtime_error("No sensor view file found: " + sensor_view_messages_path.string());
        }

        sensor_view_messages_.clear();
        std::uint32_t sensor_view_message_size = 0;
        while (input_file.read(reinterpret_cast<char*>(&sensor_view_message_size), sizeof(sensor_view_message_size)))
        {
            boost::endian::little_to_native_inplace(sensor_view_message_size);
            std::string serialized_sensor_view(sensor_view_message_size, '\0');
            if (!input_file.read(&serialized_sensor_view[0], sensor_view_message_size))
            {
                throw std::runtime_error("Error in file encoding");
            }

            osi3::SensorView sensor_view_message;
            if (!sensor_view_message.ParseFromString(serialized_sensor_view))
            {
                throw std::runtime_error("Failed to parse sensor view message");
            }

            sensor_view_messages_.push_back(sensor_view_message);
        }

        input_file.close();
    }

    void WriteSensorViewMessages()
    {
        for (const auto& sensor_view_message : sensor_view_messages_)
        {
            unit_->WriteToTraceFile(sensor_view_message);
        }
    }

    std::unique_ptr<TraceWriter> unit_;
    std::vector<osi3::SensorView> sensor_view_messages_;
};

TEST_P(TraceWriterUnitTestFixture, GivenSensorViewMessagesFileWhenCreatingTraceThenCheckTraceEqualExpected)
{
    const auto& parameters = GetParam();
    ASSERT_NO_THROW(unit_ = std::make_unique<TraceWriter>(parameters.actual_trace_file));

    WriteSensorViewMessages();

    EXPECT_TRUE(file_system::AreFilesEqual(parameters.expected_trace_file, unit_->GetTraceFile()));
}

INSTANTIATE_TEST_SUITE_P(
    TraceWriterTestsOsiTxtExtensions,
    TraceWriterUnitTestFixture,
    ::testing::ValuesIn({
        InputOutputParameters{TEST_DATA_DIR_PATH / "output_trace.txt", TEST_DATA_DIR_PATH / "generated_trace.txt"},
        InputOutputParameters{TEST_DATA_DIR_PATH / "output_trace.osi", TEST_DATA_DIR_PATH / "generated_trace.osi"},
        InputOutputParameters{TEST_DATA_DIR_PATH / "output_trace.txth", TEST_DATA_DIR_PATH / "generated_trace.txth"},
    }));

TEST_F(TraceWriterUnitTestFixture, GivenInvalidTraceExtensionWhenInstantiatingTraceWriterThenExpectException)
{
    std::string invalid_file_name{"/path/to/invalid/file/extension/file.ext"};
    ASSERT_THROW(unit_ = std::make_unique<TraceWriter>(invalid_file_name), std::runtime_error);
}

}  // namespace
}  // namespace gtgen::core::service
