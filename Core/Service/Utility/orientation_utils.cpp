/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Service/Utility/orientation_utils.h"

namespace gtgen::core::service::utility
{

using units::literals::operator""_rad;

units::angle::radian_t ClampOrientation(const units::angle::radian_t value)
{
    constexpr auto m_pi_rad{units::angle::radian_t{M_PI}};
    constexpr auto m_2_pi_rad{2 * m_pi_rad};

    auto clamped_value{value};

    if (value > m_pi_rad)
    {
        clamped_value -= m_2_pi_rad;
    }
    else if (value < -m_pi_rad)
    {
        clamped_value += m_2_pi_rad;
    }

    return clamped_value;
}

mantle_api::Orientation3<units::angle::radian_t> CalculateClampedDeltaOrientation(
    const mantle_api::Orientation3<units::angle::radian_t>& orientation_to,
    const mantle_api::Orientation3<units::angle::radian_t>& orientation_from)
{
    auto delta_orientation = orientation_to - orientation_from;

    delta_orientation.yaw = ClampOrientation(delta_orientation.yaw);
    delta_orientation.roll = ClampOrientation(delta_orientation.roll);
    delta_orientation.pitch = ClampOrientation(delta_orientation.pitch);

    return delta_orientation;
}

}  // namespace gtgen::core::service::utility
