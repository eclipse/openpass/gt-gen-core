/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_UTILITY_DATETIME_DATE_H
#define GTGEN_CORE_SERVICE_UTILITY_DATETIME_DATE_H

#include <fmt/format.h>

namespace gtgen::core::service::utility
{
class Date final
{
  public:
    static Date Today();

    int year{0};
    int month{0};
    int day{0};

  private:
    Date() = default;
    void SetToday();
};

}  // namespace gtgen::core::service::utility

template <>
struct fmt::formatter<gtgen::core::service::utility::Date>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const gtgen::core::service::utility::Date& d,
                FormatContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return format_to(ctx.out(), "{}-{:0>2}-{:0>2}", d.year, d.month, d.day);
    }
};

#endif  // GTGEN_CORE_SERVICE_UTILITY_DATETIME_DATE_H
