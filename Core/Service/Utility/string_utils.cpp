/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Service/Utility/string_utils.h"

#include "Core/Service/Exception/exception.h"

#include <algorithm>
#include <cmath>
#include <locale>
#include <regex>

namespace gtgen::core::service::utility
{
void Trim(std::string& s, const std::string& matcher)
{
    s.erase(s.find_last_not_of(matcher) + 1);
    s.erase(0, s.find_first_not_of(matcher));
}

std::vector<std::string> Split(const std::string& s, const char delimiter, bool ignore_empty_elements)
{
    std::vector<std::string> elements{};
    elements.reserve(static_cast<std::size_t>(std::count(s.begin(), s.end(), delimiter)));

    std::size_t current_position{0};
    std::size_t previous_position{0};

    while ((current_position = s.find(delimiter, current_position)) != std::string::npos)
    {
        elements.emplace_back(s.substr(previous_position, current_position - previous_position));
        previous_position = ++current_position;
    }

    if (previous_position != current_position)
    {
        elements.emplace_back(s.substr(previous_position, current_position - previous_position));
    }

    if (ignore_empty_elements)
    {
        elements.erase(
            std::remove_if(
                elements.begin(), elements.end(), [](const std::string& element) { return element.empty(); }),
            elements.end());
    }

    return elements;
}

bool EndsWith(std::string s, std::string ending, bool ignore_case)
{
    const std::size_t s_length{s.length()};
    const std::size_t ending_length{ending.length()};

    if (s_length < ending_length)
    {
        return false;
    }

    if (ignore_case)
    {
        std::transform(s.begin(), s.end(), s.begin(), [](unsigned char c) { return std::tolower(c); });
        std::transform(ending.begin(), ending.end(), ending.begin(), [](unsigned char c) { return std::tolower(c); });
    }

    return s.compare(s_length - ending_length, ending_length, ending) == 0;
}

bool IsDouble(const std::string& input)
{
    char* end = nullptr;  // NOLINT(cppcoreguidelines-pro-type-vararg)
    strtod(input.c_str(), &end);
    return end != input.c_str() && *end == '\0' && errno != ERANGE;
}

std::string ToLower(const std::string& input)
{
    std::string lower_string(input);
    std::transform(lower_string.begin(), lower_string.end(), lower_string.begin(), [](unsigned char c) {
        return std::tolower(c);
    });
    return lower_string;
}

void RemoveAllWhitespaces(std::string& string_to_modify)
{
    string_to_modify.erase(std::remove_if(string_to_modify.begin(),
                                          string_to_modify.end(),
                                          [](char& c) { return std::isspace<char>(c, std::locale::classic()); }),
                           string_to_modify.end());
}

std::vector<std::string> StringListToVector(std::string s)
{
    // Remove possible whitespaces from overall string
    service::utility::Trim(s, " ");
    if (s.empty())
    {
        return {};
    }
    if (s.front() != '{' || s.back() != '}')
    {
        throw ServiceException{"Malformed value. Expected string to start and end with a curly brace {}", s};
    }
    service::utility::Trim(s, "{ }");
    auto splited = service::utility::Split(s);
    for (auto& sp : splited)
    {
        service::utility::Trim(sp, " ");
    }
    return splited;
}

std::vector<std::int64_t> StringToVectorInt(const std::string& lane_ids)
{
    std::vector<std::int64_t> lane_ids_dec;

    if (lane_ids == "")
    {
        return lane_ids_dec;
    }

    std::regex word_pat("([^,].*?(?=,|$))");
    std::regex valid_string_pat{"(\\s*-?\\d+\\s*,\\s*)*-?\\d+\\s*$"};

    std::smatch m;

    if (!std::regex_match(lane_ids, m, valid_string_pat))
    {
        throw(std::invalid_argument("Values are invalid"));
    }

    auto it_be = std::sregex_iterator(lane_ids.begin(), lane_ids.end(), word_pat);

    try
    {
        std::for_each(it_be, std::sregex_iterator(), [&](auto it) { lane_ids_dec.push_back(std::stoll(it.str())); });
    }
    catch (const std::out_of_range&)
    {
        throw std::out_of_range("This number is out of range");
    }

    return lane_ids_dec;
}

}  // namespace gtgen::core::service::utility
