/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/GlmWrapper/glm_mantle_conversion.h"

#include "Core/Tests/TestUtils/expect_extensions.h"

#include <gtest/gtest.h>

namespace gtgen::core::service::glmwrapper
{
using units::literals::operator""_m;

TEST(ToMantleTest, GivenGlmVector_WhenConvertingToMantle_ThenAllValuesAreTheSame)
{
    glm::dvec3 glm_vec{3.0, 4.0, 5.0};
    mantle_api::Vec3<units::length::meter_t> vec = ToMantleVec3Length(glm_vec);

    EXPECT_EQ(vec.x(), glm_vec.x);
    EXPECT_EQ(vec.y(), glm_vec.y);
    EXPECT_EQ(vec.z(), glm_vec.z);
}

TEST(ToGlmTest, GivenMantleVector_WhenConvertingToGlm_ThenAllValuesAreTheSame)
{
    mantle_api::Vec3<units::length::meter_t> vec{3.0_m, 4.0_m, 5.0_m};
    glm::dvec3 glm_vec = ToGlmVec3(vec);

    EXPECT_EQ(glm_vec.x, vec.x());
    EXPECT_EQ(glm_vec.y, vec.y());
    EXPECT_EQ(glm_vec.z, vec.z());
}

TEST(ToGlmTest, GivenMantleVectorOfVec3_WhenConvertingToGlm_ThenAllValuesAreTheSame)
{
    std::vector<mantle_api::Vec3<units::length::meter_t>> vector{{3.0_m, 4.0_m, 5.0_m}, {1_m, 1_m, 2_m}};
    std::vector<glm::dvec3> glm_vector = ToVectorGlmVec3(vector);

    EXPECT_EQ(glm_vector.at(0).x, vector.at(0).x());
    EXPECT_EQ(glm_vector.at(0).y, vector.at(0).y());
    EXPECT_EQ(glm_vector.at(0).z, vector.at(0).z());
    EXPECT_EQ(glm_vector.at(1).x, vector.at(1).x());
    EXPECT_EQ(glm_vector.at(1).y, vector.at(1).y());
    EXPECT_EQ(glm_vector.at(1).z, vector.at(1).z());
}

}  // namespace gtgen::core::service::glmwrapper
