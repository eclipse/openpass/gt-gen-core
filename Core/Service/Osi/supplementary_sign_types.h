/*******************************************************************************
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_OSI_SUPPLEMENTARYSIGNTYPES_H
#define GTGEN_CORE_SERVICE_OSI_SUPPLEMENTARYSIGNTYPES_H

namespace osi
{

enum class OsiSupplementarySignType
{
    kUnknown = 0,
    kOther = 1,
    kNoSign = 2,
    kValidForDistance = 3,
    kValidInDistance = 4,
    kTimeRange = 5,
    kWeight = 6,
    kRain = 7,
    kFog = 8,
    kSnow = 9,
    kSnowRain = 10,
    kLeftArrow = 11,
    kRightArrow = 12,
    kLeftBendArrow = 13,
    kRightBendArrow = 14,
    kTruck = 15,
    kTractorsMayBePassed = 16,
    kHazardous = 17,
    kTrailer = 18,
    kNight = 19,
    kZone = 20,
    kStop_4Way = 21,
    kMotorcycle = 22,
    kMotorcycleAllowed = 23,
    kCar = 24,
    kStopIn = 25,
    kTime = 26,
    kPriorityRoadBottomLeftFourWay = 27,
    kPriorityRoadTopLeftFourWay = 28,
    kPriorityRoadBottomRightFourWay = 29,
    kArow = 30,
    kPriorityRoadTopRightFourWay = 31,
    kPriorityRoadBottomLeftThreeWayStraight = 32,
    kPriorityRoadBottomLeftThreeWaySideways = 33,
    kPriorityRoadTopLeftThreeWayStraight = 34,
    kPriorityRoadBottomRightThreeWayStraight = 35,
    kPriorityRoadBottomRightThreeWaySideway = 36,
    kPriorityRoadTopRightThreeWayStraight = 37,
    kNoWaitingSideStripes = 38,
    kSpace = 39,
    kAccident = 40,
    kText = 41,
    kParkingConstraint = 42,
    kParkingDiscTimeRestriction = 43,
    kWet = 44,
    kExcept = 45,
    kConstrainedTo = 46,
    kServices = 47,
    kRollingHighwayInformation = 48
};

enum class OsiSupplementarySignActor
{
    kUnknown = 0,
    kOther = 1,
    kNoActor = 2,
    kAgriculturalVehicles = 3,
    kBicycles = 4,
    kBuses = 5,
    kCampers = 6,
    kCaravans = 7,
    kCars = 8,
    kCarsWithCaravans = 9,
    kCarsWithTrailers = 10,
    kCattle = 11,
    kChildren = 12,
    kConstructionVehicles = 13,
    kDeliveryVehicles = 14,
    kDisabledPersons = 15,
    kEbikes = 16,
    kElectricVehicles = 17,
    kEmergencyVehicles = 18,
    kFerryUsers = 19,
    kForestryVehicles = 20,
    kHazardousGoodsVehicles = 21,
    kHorseCarriages = 22,
    kHorseRiders = 23,
    kInlineSkaters = 24,
    kMedicalVehicles = 25,
    kMilitaryVehicles = 26,
    kMopeds = 27,
    kMotorcycles = 28,
    kMotorizedMultitrackVehicles = 29,
    kOperationalAndUtilityVehicles = 30,
    kPedestrians = 31,
    kPublicTransportVehicles = 32,
    kRailroadTraffic = 33,
    kResidents = 34,
    kSlurryTransport = 35,
    kTaxis = 36,
    kTractors = 37,
    kTrailers = 38,
    kTrams = 39,
    kTrucks = 40,
    kTrucksWithSemitrailers = 41,
    kTrucksWithTrailers = 42,
    kVehiclesWithGreenBadges = 43,
    kVehiclesWithRedBadges = 44,
    kVehiclesWithYellowBadges = 45,
    kWaterPollutantVehicles = 46,
    kWinterSportspeople = 47
};

}  // namespace osi

#endif  // GTGEN_CORE_SERVICE_OSI_SUPPLEMENTARYSIGNTYPES_H
