/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2025, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/UserSettings/Internal/user_settings_writer.h"

#include "Core/Service/Exception/exception.h"
#include "Core/Service/Logging/logging.h"

namespace gtgen::core::service::user_settings
{
void UserSettingsWriter::SetUserSettings(std::string ini_file_path,
                                         mINI::INIStructure* ini_structure,
                                         UserSettings* user_settings)
{
    ini_file_path_ = std::move(ini_file_path);
    ini_structure_ = ini_structure;
    user_settings_ = user_settings;
}

void UserSettingsWriter::WriteSettingsStructToFile()
{
    ASSERT(ini_structure_ != nullptr && user_settings_ != nullptr &&
           "UserSettingsWriter has uninitialized properties! Contact GTGen support and report the problem.")

    Debug("Writing ini file to: {}", ini_file_path_);

    SetFileLogging();
    SetGroundTruth();
    SetHostVehicle();
    SetMap();
    SetMapChunking();
    SetUserDirectories();
    SetClock();
    SetFoxglove();
    SetTraceRecording();
    SetSimulationResults();

    WriteIniStructToFile(*ini_structure_);
}

void UserSettingsWriter::WriteIniStructToFile(mINI::INIStructure& ini_structure)
{
    mINI::INIFile file{ini_file_path_};
    if (!file.write(ini_structure, true))
    {
        GTGEN_LOG_AND_THROW_WITH_FILE_DETAILS(
            ServiceException("Writing user settings failed: mINI could not write file: {}", ini_file_path_))
    }

    Info("Wrote user settings to: {}", ini_file_path_);
}

void UserSettingsWriter::SetFileLogging()
{
    Set(user_settings_->file_logging.log_level, "FileLogging", "LogLevel");
}

void UserSettingsWriter::SetGroundTruth()
{
    Set(user_settings_->ground_truth.lane_marking_distance_in_m, "GroundTruth", "LaneMarkingDistance");
    Set(user_settings_->ground_truth.lane_marking_downsampling_epsilon, "GroundTruth", "SimplifyLaneMarkingsEpsilon");
    Set(user_settings_->ground_truth.lane_marking_downsampling, "GroundTruth", "SimplifyLaneMarkings");
    Set(user_settings_->ground_truth.allow_invalid_lane_locations, "GroundTruth", "AllowInvalidLaneLocations");
}

void UserSettingsWriter::SetHostVehicle()
{
    Set(user_settings_->host_vehicle.host_control_mode, "HostVehicle", "Movement");
    Set(user_settings_->host_vehicle.blocking_communication, "HostVehicle", "BlockingCommunication");
    Set(user_settings_->host_vehicle.time_scale, "HostVehicle", "TimeScale");
    Set(user_settings_->host_vehicle.recovery_mode, "HostVehicle", "RecoveryMode");
}

void UserSettingsWriter::SetMap()
{
    Set(user_settings_->map.include_obstacles, "Map", "IncludeObstacles");
}

void UserSettingsWriter::SetMapChunking()
{
    Set(user_settings_->map_chunking.chunk_grid_size, "MapChunking", "ChunkGridSize");
    Set(user_settings_->map_chunking.cells_per_direction, "MapChunking", "CellsPerDirection");
}

void UserSettingsWriter::SetUserDirectories()
{
    Set(user_settings_->user_directories.scenarios, "UserDirectories", "Scenarios");
    Set(user_settings_->user_directories.maps, "UserDirectories", "Maps");
    Set(user_settings_->user_directories.plugins, "UserDirectories", "Plugins");
}

void UserSettingsWriter::SetClock()
{
    Set(user_settings_->clock.use_system_clock, "Clock", "UseSystemClock");
}

void UserSettingsWriter::SetFoxglove()
{
    Set(user_settings_->foxglove.websocket_server, "Foxglove", "WebsocketServer");
}

void UserSettingsWriter::SetTraceRecording()
{
    Set(user_settings_->trace_recording.enabled, "TraceRecording", "Enabled");
    Set(user_settings_->trace_recording.trace_file_path.string(), "TraceRecording", "Path");
}

void UserSettingsWriter::SetSimulationResults()
{
    Set(user_settings_->simulation_results.log_cyclics, "SimulationResults", "LogCyclics");
    Set(user_settings_->simulation_results.output_directory_path.string(), "SimulationResults", "OutputDirectoryPath");
}

}  // namespace gtgen::core::service::user_settings
