/*******************************************************************************
 * Copyright (c) 2019-2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_USERSETTINGS_INTERNAL_USERSETTINGSREADER_H
#define GTGEN_CORE_SERVICE_USERSETTINGS_INTERNAL_USERSETTINGSREADER_H

#include "Core/Service/UserSettings/user_settings.h"
#include "Core/Service/Utility/string_utils.h"

#define MINI_CASE_SENSITIVE
#include <mINI/ini.h>

namespace gtgen::core::service::user_settings
{
class UserSettingsReader
{
  public:
    /// @brief Reads the ini file into an internal structure and fills the user_settings from it
    void ReadFromFile(const std::string& ini_file_path, UserSettings& user_settings);

    mINI::INIStructure ReadIniStructureFromFile(const std::string& ini_file_path);

  private:
    void ParseUserSettingsFromIniStructure(const mINI::INIStructure& ini_structure, UserSettings& user_settings);

    /// @brief Gets a setting from the ini data structure
    template <typename T>
    void Get(T& value, const std::string& group, const std::string& entry) const
    {
        std::string string_value = ini_structure_->get(group).get(entry);
        if (string_value.empty())
        {
            return;
        }

        if constexpr (std::is_same_v<T, bool>)
        {
            value = static_cast<bool>(service::utility::ToLower(string_value) == "true");
        }
        else if constexpr (std::is_integral_v<T>)
        {
            value = static_cast<u_int16_t>(std::stoul(string_value));
        }
        else if constexpr (std::is_floating_point_v<T>)
        {
            value = std::stod(string_value);
        }
        else if constexpr (std::is_same_v<T, HostControlMode>)
        {
            value = ToEnum<HostControlMode>(string_value);
        }
        else if constexpr (std::is_same_v<T, QualityLevel>)
        {
            value = ToEnum<QualityLevel>(string_value);
        }
        else if constexpr (std::is_same_v<T, ColorScheme>)
        {
            value = ToEnum<ColorScheme>(string_value);
        }
        else if constexpr (std::is_same_v<T, UserSettingsList>)
        {
            value = UserSettingsList::Create(string_value);
        }
        else if constexpr (std::is_same_v<T, UserSettingsPathList>)
        {
            value = UserSettingsPathList::Create(string_value);
        }
        else  // else it is just a string
        {
            value = string_value;
        }
    }

    /// @section Helper functions to extract the ini data structure to the service::user_settings data structures
    void GetFileLogging();
    void GetGroundTruth();
    void GetHostVehicle();
    void GetMap();
    void GetMapChunking();
    void GetUserDirectories();
    void GetClock();
    void GetFoxglove();
    void GetTraceRecording();
    void GetSimulationResults();
    void GetCollisionDetection();

    const mINI::INIStructure* ini_structure_{nullptr};
    UserSettings* user_settings_{nullptr};
};

}  // namespace gtgen::core::service::user_settings

#endif  // GTGEN_CORE_SERVICE_USERSETTINGS_INTERNAL_USERSETTINGSREADER_H
