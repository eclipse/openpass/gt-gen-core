# *******************************************************************************
# Copyright (c) 2025, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
# Copyright (C) 2024, ANSYS, Inc.
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# *******************************************************************************

#!/bin/bash

set -ex

MYDIR="$(dirname "$(readlink -f $0)")"
BASEDIR=$(realpath "${MYDIR}/../../../..")
CACHEDIR=$(realpath "/home/jenkins/cache/gtgen_core")

# This override the cache folder of bazel
export TEST_TMPDIR="${CACHEDIR}"
export BAZELISK_HOME="${CACHEDIR}"

# Navigate to repo folder
cd "${MYDIR}/../../.." || exit 1

echo "Build project using debug config ..."
bazel build --config=core //Core/... \
    --local_cpu_resources=16 \
    --disk_cache="${CACHEDIR}" \
    --experimental_disk_cache_gc_max_size=130G \
    --noshow_progress

echo "Run all unit tests using debug config ..."
bazel test --config=core //Core/... \
    --test_output=all \
    --local_cpu_resources=16 \
    --disk_cache="${CACHEDIR}" \
    --experimental_disk_cache_gc_max_size=130G \
    --noshow_progress
