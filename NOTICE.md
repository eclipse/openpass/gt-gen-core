# Notices for Eclipse openpass

This content is produced and maintained by the Eclipse openpass project.

 * Project home: https://projects.eclipse.org/projects/automotive.openpass

## Copyright

All content is the property of the respective authors or their employers.
For more information regarding authorship of content, please consult the
listed source code repository logs.

## Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v2.0 which is available at
https://www.eclipse.org/legal/epl-2.0/.

SPDX-License-Identifier: EPL-2.0

## Source Code

The project maintains the following source code repositories:

 * https://gitlab.eclipse.org/eclipse/openpass/gt-gen-core

## Third-party Content
This project leverages the following third party content.

### asio
- License: Boost Software License - Version 1.0
- Homepage: http://think-async.com/Asio
- Repository: https://github.com/chriskohlhoff/asio/
- Version: 1-24-0


### bazel
- License: Apache-2.0
- Repository: https://github.com/bazelbuild/bazel
- Version: 6.2.1

### Bazel Rules
- License: Apache-2.0
- Repository: https://github.com/bazelbuild/rules_pkg
- Version: patch version #TODO check version

### Boost library (Boost)
- License: Boost Software License 1.0
- Repository: https://github.com/boostorg/boost/releases/tag/boost-1.71.1
- Version: 1.71.1

### fmt (libfmt)
- License: BSD 2-Clause "Simplified" License
- Homepage: http://fmtlib.net/latest/index.html
- Repository: https://github.com/fmtlib/fmt
- Version: 8.0.1

### Foxglove WebSocket Protocol
- License: MIT license
- Repository: https://github.com/foxglove/ws-protocol
- Version: 1.0.0

### glm
- License: The Happy Bunny License and MIT License
- Version: 0.9.9.7

### googletest (Google Test)
- License: BSD 3-Clause "New" or "Revised" License
- Repository: https://github.com/google/googletest
- Version: 1.10.0


### MantleAPI
- License: Eclipse Public License 2.0
- Repository: https://gitlab.eclipse.org/eclipse/openpass/mantle-api
- Commit: see https://gitlab.eclipse.org/eclipse/openpass/mantle-api/-/tags/v11.0.0

### MapSDK
- License: EPL 2.0
- Repository: https://gitlab.eclipse.org/eclipse/openpass/map-sdk
- Commit: see https://gitlab.eclipse.org/eclipse/openpass/map-sdk/-/tags/v0.4.0

### mINI (Ini file reader and writer)
- License: MIT License
- Repository: https://github.com/pulzed/mINI
- Version: 0.9.14

### nlohmann (JSON for Modern C++)
- License: MIT License
- Homepage: https://nlohmann.github.io/json/
- Repository: https://github.com/nlohmann/json
- Version: 3.9.1#

### Open Simulation Interface
- License: Mozilla Public License Version 2.0
- Homepage: https://github.com/OpenSimulationInterface
- Repository: https://github.com/OpenSimulationInterface/open-simulation-interface
- Version: 3.7.0

### OpenSSL
- License: Apache-2.0 license
- Homepage: https://www.openssl.org/
- Repository: https://github.com/openssl/openssl
- Version: 3.0.13

### OSI Traffic Participant Interface
- License: EPL 2.0
- Repository: https://gitlab.eclipse.org/eclipse/openpass/osi-traffic-participant
- Version: v2.0.0

### Google protobuf
- License: BSD 3-Clause "New" or "Revised" License
- Repository: https://github.com/protocolbuffers/protobuf
- Version: 3.19.1

### spdlog
- License: MIT License
- Repository: https://github.com/gabime/spdlog
- Changes: removed include/spdlog/fmt/bundled
- Version: 1.11.0

### tracy
- License: BSD 3-Clause
- Repository: https://bitbucket.org/wolfpld/tracy
- Version: 0.5
- Misc: Patch version can be downloaded from https://bitbucket.org/afrasson/tracy/get/update-port.zip

### Units
- License: MIT License
- Repository: https://github.com/nholthaus/units
- Version: 2.3.4

### WebSocket++
- License: BSD 3-Clause License
- Homepage: www.zaphoyd.com/websocketpp
- Repository: https://github.com/zaphoyd/websocketpp
- Version: 0.8.2


### zlib
- Required for protobuf (versions >= 3.17.0)
- License: zlib
- Repository: https://github.com/madler/zlib
- Version: v1.2.12



## Cryptography

Content may contain encryption software. The country in which you are currently
may have restrictions on the import, possession, and use, and/or re-export to
another country, of encryption software. BEFORE using any encryption software,
please check the country's laws, regulations and policies concerning the import,
possession, or use, and re-export of encryption software, to see if this is
permitted.
