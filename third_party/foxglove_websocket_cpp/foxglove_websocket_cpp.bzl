load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_VERSION = "v1.2.0"

def foxglove_websocket_cpp():
    maybe(
        http_archive,
        name = "foxglove_websocket_cpp",
        sha256 = "0471d3932500ed6acd87a99cd76e048366c82f2527c1631afcee4f8ab71c4ab7",
        build_file = Label("//:third_party/foxglove_websocket_cpp/foxglove_websocket_cpp.BUILD"),
        url = "https://github.com/foxglove/ws-protocol/archive/refs/tags/releases/cpp/{version}.tar.gz".format(version = _VERSION),
        strip_prefix = "ws-protocol-releases-cpp-{version}".format(version = _VERSION),
    )
